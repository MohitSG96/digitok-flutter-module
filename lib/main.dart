import 'dart:async';
import 'dart:convert';

import 'package:digitok/pages/banner_screen.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
//  FirebaseCrashlytics.instance.enableInDevMode = true;
  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
//  WidgetsFlutterBinding.ensureInitialized();
  runZoned(() {
    runApp(MyApp());
  }, onError: FirebaseCrashlytics.instance.recordError);
//  AssetImage("assets/intro_tok.webp");
}

void onCrash(Object exception, StackTrace stackTrace) {
  // Prints the exception and the stack trace locally
  print(exception);
  print(stackTrace);
  // Send the strack trace to Crashlytics
  FirebaseCrashlytics.instance.recordError(exception, stackTrace);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Theme.of(context).textTheme.apply(
//          fontFamily: "Titillium",
          bodyColor: Colors.white,
          displayColor: Colors.white,
        );
    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/grd.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Digitok',
        theme: ThemeData(
//          fontFamily: "Titillium",
          backgroundColor: Colors.transparent,
          primarySwatch: Colors.red,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: SplashScreen(),
//        home: BetaQuiz(),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  var webSocket;
  String authToken;
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    getLogin();
    controller = AnimationController(
      duration: Duration(milliseconds: 4500),
      vsync: this,
    );
    animation = Tween(begin: 0.0, end: 1.0).animate(controller);
    animation.addListener(() {
      setState(() {});
    });
    animation.addStatusListener((status) {});
    controller.repeat();
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object["msg"].toLowerCase().contains("not authenticate")) {
        globals.showToast("Please Login Again");
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => BannerScreen()));
      } else if (object["msg"].contains("User is authenticate")) {
        globals.authToken = authToken;
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => GameList()));
      } else {
        globals.showToast("Please Login Again");
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => BannerScreen()));
      }
    } else {
      globals.showToast("Please Login Again");
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => BannerScreen()));
    }
  }

  getLogin() async {
    authToken = await UserData().getAuthToken();
//    String welcome = await UserData().getWelcome();
    Future.delayed(Duration(seconds: 1), () {
      if (authToken != null && authToken.trim().isNotEmpty) {
        Map<String, dynamic> action = {
          "action": "getUser",
          "data": {"stage": globals.STAGE, "authToken": authToken}
        };

        webSocket.sink.add(jsonEncode(action).toString());
      } else
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => BannerScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble_white.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/digitok_logo.webp"),
            SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Color(0xFFAAAAAA)),
                  right: BorderSide(color: Color(0xFFAAAAAA)),
                  left: BorderSide(color: Color(0xFFAAAAAA)),
                  bottom: BorderSide(color: Color(0xFF808080), width: 2.5),
                ),
              ),
              height: 30,
              width: MediaQuery.of(context).size.width * 0.65,
              child: LinearProgressIndicator(
                value: animation.value,
                backgroundColor: Colors.white,
                valueColor: AlwaysStoppedAnimation(Colors.red),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
