import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/pages/game_screen.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class History_Screen extends StatefulWidget {
  final Game game;

  const History_Screen({Key key, this.game}) : super(key: key);

  @override
  _History_ScreenState createState() => _History_ScreenState();
}

class _History_ScreenState extends State<History_Screen>
    with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  var websocket;
  String _authToken;
  bool _isLoading = true;

  int totalGamesPlayed, totalPoints, Heart, BankIt, PlayOn;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    getuserdetails();

    websocket = WebSocketChannel.connect(globals.uri);
    websocket.stream.listen(output);

    WidgetsBinding.instance.addObserver(this);
  }

  void getuserdetails() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> condition = {
      "UserId": globals.userId,
    };

    Map<String, dynamic> data = {
      "condition": condition,
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "userHistory",
      "data": data,
    };

    websocket.sink.add(jsonEncode(action).toString());
  }

  output(message) {
    Map<String, dynamic> obj = jsonDecode(message);
    if (obj.containsKey("msg")) {
      if (obj["msg"]
          .toString()
          .contains("Usergamesummarylist successfully found!")) {
        if (obj.containsKey("data")) {
          var data = obj["data"];

          totalGamesPlayed = data["totalGamesPlayed"];
          totalPoints = data["totalPoints"];
          var totalLifeUsed = data["totalLifeUsed"];
          Heart = totalLifeUsed["Heart"];
          BankIt = totalLifeUsed["BankIt"];
          PlayOn = totalLifeUsed["PlayOn"];

          setState(() {
            _isLoading = false;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              centerTitle: true,
              leading: Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0),
                child: GestureDetector(
                  onTap: () {
                    if (globals.Screen_name == "GameList") {
                      return Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => GameList()));
                    } else if (globals.Screen_name == "GameScreen") {
                      return Navigator.of(context)
                          .pushReplacement(MaterialPageRoute(
                              builder: (context) => GameScreen(
                                    game: widget.game,
                                  )));
                    }
                  },
                  child: Image.asset(
                    "assets/icons/back_arrow.webp",
                    fit: BoxFit.cover, // this is the solution for border
                    width: 70.0,
                    height: 70.0,
                  ),
                ),
              ),
              backgroundColor: Colors.transparent,
            ),
            body: Center(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: AutoSizeText(
                      "Game History",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 32.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  _isLoading
                      ? Center(child: globals.lottie)
                      : Padding(
                          padding: EdgeInsets.only(
                              top: 20.0, right: 10.0, left: 10.0, bottom: 0.0),
                          child: Container(
                              width: width,
                              height: height * 0.65,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/history_background.jpg"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 15,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 5.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/icons/gameplayedicon.png",
                                            width: 45,
                                            height: 45,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: AutoSizeText(
                                                "Total Games Played",
                                                style: TextStyle(
                                                    fontSize: 18.0,
                                                    color:
                                                        HexColor("#3e1b5f"))),
                                          ),
                                          AutoSizeText(
                                              totalGamesPlayed.toString(),
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: HexColor("#3e1b5f"))),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Container(
                                            height: 2.0,
                                            width: width,
                                            color: HexColor("#E3DDDD"))),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 5.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/icons/totalpointsicon.png",
                                            width: 45,
                                            height: 45,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: AutoSizeText(
                                                "Total Points Won",
                                                style: TextStyle(
                                                    fontSize: 18.0,
                                                    color:
                                                        HexColor("#3e1b5f"))),
                                          ),
                                          AutoSizeText(totalPoints.toString(),
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: HexColor("#3e1b5f"))),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Container(
                                            height: 1.0,
                                            width: width,
                                            color: HexColor("#E3DDDD"))),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 5.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/icons/lifeline.png",
                                            width: 45,
                                            height: 45,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: AutoSizeText("Lifeline",
                                                style: TextStyle(
                                                    fontSize: 18.0,
                                                    color:
                                                        HexColor("#3e1b5f"))),
                                          ),
                                          AutoSizeText(Heart.toString(),
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: HexColor("#3e1b5f"))),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Container(
                                            height: 1.0,
                                            width: width,
                                            color: HexColor("#E3DDDD"))),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 5.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/icons/bankedit_icon.png",
                                            width: 45,
                                            height: 45,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: AutoSizeText("Banked it",
                                                style: TextStyle(
                                                    fontSize: 18.0,
                                                    color:
                                                        HexColor("#3e1b5f"))),
                                          ),
                                          AutoSizeText(BankIt.toString(),
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: HexColor("#3e1b5f"))),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Container(
                                            height: 1.0,
                                            width: width,
                                            color: HexColor("#E3DDDD"))),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 10,
                                          left: 10.0,
                                          right: 10.0,
                                          bottom: 5.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/icons/playon_icon.png",
                                            width: 45,
                                            height: 45,
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: AutoSizeText("PlayOn",
                                                style: TextStyle(
                                                    fontSize: 18.0,
                                                    color:
                                                        HexColor("#3e1b5f"))),
                                          ),
                                          AutoSizeText(PlayOn.toString(),
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: HexColor("#3e1b5f"))),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        child: Container(
                                            height: 1.0,
                                            width: width,
                                            color: HexColor("#E3DDDD"))),
                                  ],
                                ),
                              )),
                        )
                ],
              ),
            )));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

//endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    if (globals.Screen_name == "GameList") {
      return Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
    } else if (globals.Screen_name == "GameScreen") {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => GameScreen(game: widget.game)));
    } else {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => GameList(),
      ));
    }
  }
}
