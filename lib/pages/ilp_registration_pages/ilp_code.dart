import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/ilp_registration_pages/student_profile.dart';
import 'package:digitok/ui_widgets/iql_edittext.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ILPCodeScreen extends StatefulWidget {
  @override
  _ILPCodeScreenState createState() => _ILPCodeScreenState();
}

class _ILPCodeScreenState extends State<ILPCodeScreen> with WidgetsBindingObserver {
  String iqlCode;
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView(
        children: [
          Container(
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top +
                5,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
//                SizedBox(height: 20),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(left: 20, bottom: 10),
                      width: 60,
                      child: Image.asset(
                        "assets/btn/back_button.webp",
                        color: Colors.black,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Image.asset(
                  "assets/iql_logo.webp",
                  height: 220,
                  width: 350,
                  fit: BoxFit.fitWidth,
                ),
                Container(
                  width: 250,
                  child: AutoSizeText(
                    "Enter the IQL Code you received on ILP",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  width: 220,
                  child: IQLEditText(
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      textCapitalization: TextCapitalization.sentences,
                      style: TextStyle(
                        color: Color(0xFF002060),
                        fontSize: 20,
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Enter Code Here",
                        hintStyle: TextStyle(color: Colors.grey.shade300),
                        contentPadding: EdgeInsets.zero,
                      ),
                      onChanged: (val) {
                        iqlCode = val;
                      },
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    _playSound();
                    sendIQLCode();
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/btn/btn-enter.webp",
                      fit: BoxFit.fill,
                      height: 40,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  output(message) {
    var object = Map<String, dynamic>.from(jsonDecode(message));
    if (object.containsKey("data") && object["data"] != null) {
      if (object['data'] is List<dynamic> && object['data'].length == 0) {
        if (Platform.isIOS) {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return CupertinoAlertDialog(
                title: Text("Invalid ILP Code "),
                content: Text("Please enter valid ILP Code to proceed."),
                actions: [
                  CupertinoDialogAction(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(diaCtx).pop(),
                  )
                ],
              );
            },
          );
        } else {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return AlertDialog(
                title: Text("Invalid ILP Code !"),
                content: Text("Please enter valid ILP Code to proceed."),
                actions: [
                  FlatButton(
                    onPressed: () => Navigator.of(diaCtx).pop(),
                    child: Text("OK"),
                  ),
                ],
              );
            },
          );
        }
      } else if (object['data'] is Map) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => StudentProfile(user: object['data']),
        ));
      } else {
        if (Platform.isIOS) {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return CupertinoAlertDialog(
                title: Text("Invalid ILP Code "),
                content: Text("Please enter valid ILP Code to proceed."),
                actions: [
                  CupertinoDialogAction(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(diaCtx).pop(),
                  )
                ],
              );
            },
          );
        } else {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return AlertDialog(
                title: Text("Invalid ILP Code !"),
                content: Text("Please enter valid ILP Code to proceed."),
                actions: [
                  FlatButton(
                    onPressed: () => Navigator.of(diaCtx).pop(),
                    child: Text("OK"),
                  ),
                ],
              );
            },
          );
        }
      }
    } else {
      if (Platform.isIOS) {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return CupertinoAlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                CupertinoDialogAction(
                  child: Text("OK"),
                  onPressed: () => Navigator.of(diaCtx).pop(),
                )
              ],
            );
          },
        );
      } else {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return AlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.of(diaCtx).pop(),
                  child: Text("OK"),
                ),
              ],
            );
          },
        );
      }
    }
  }

  sendIQLCode() {
    if (iqlCode != null) {
      var webSocket = WebSocketChannel.connect(globals.uri);
      webSocket.stream.listen(output);
      Map<String, dynamic> action = {
        "action": "ILPUser",
        "stage": globals.STAGE,
        "data": {"iqlId": iqlCode}
      };
      print(jsonEncode(action).toString());
      webSocket.sink.add(jsonEncode(action).toString());
    } else {
      if (Platform.isIOS) {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return CupertinoAlertDialog(
              title: Text("ILP Code Empty!"),
              content: Text("Please enter ILP Code to proceed."),
              actions: [
                CupertinoDialogAction(
                  child: Text("OK"),
                  onPressed: () => Navigator.of(diaCtx).pop(),
                )
              ],
            );
          },
        );
      } else {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return AlertDialog(
              title: Text("ILP Code Empty!"),
              content: Text("Please enter ILP Code to proceed."),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.of(diaCtx).pop(),
                  child: Text("OK"),
                ),
              ],
            );
          },
        );
      }
    }
  }
}
