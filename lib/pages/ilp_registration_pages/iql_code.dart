import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/ilp_registration_pages//ilp_code.dart';
import 'package:digitok/pages/ilp_registration_pages/candidate_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:soundpool/soundpool.dart';

class IQLRequestScreen extends StatefulWidget {
  @override
  _IQLRequestScreenState createState() => _IQLRequestScreenState();
}

class _IQLRequestScreenState extends State<IQLRequestScreen> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width - 50;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Align(
            alignment: Alignment.bottomLeft,
            child: GestureDetector(
              child: Container(
                margin: EdgeInsets.only(left: 20, bottom: 10),
                width: 60,
                child: Image.asset(
                  "assets/btn/back_button.webp",
                  color: Colors.black,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Image.asset(
            "assets/iql_logo.webp",
            height: 200,
            width: 300,
          ),
//          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 10),
            child: AutoSizeText(
              "Do you have your unique IQL Code?",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.white,
              ),
              maxLines: 2,
            ),
          ),
          SizedBox(height: 10),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30),
//            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    _playSound();
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ILPCodeScreen(),
                    ));
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/btn/btn_yes.webp",
                      fit: BoxFit.fill,
                      height: 40,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    _playSound();
                    Navigator.of(context).push(MaterialPageRoute(
//                      builder: (context) => UserTypeScreen(),
                      builder: (context) => IQLInfoScreen(),
                    ));
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/btn/btn_no.webp",
                      fit: BoxFit.fill,
                      height: 40,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
