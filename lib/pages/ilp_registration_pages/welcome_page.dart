import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/ilp_registration_pages/user_type.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class IQLWelcome extends StatefulWidget {

  @override
  _IQLWelcomeState createState() => _IQLWelcomeState();
}

class _IQLWelcomeState extends State<IQLWelcome> with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        padding: EdgeInsets.fromLTRB(
            15, MediaQuery.of(context).padding.top + 10, 15, 5),
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              child: AutoSizeText(
                "Welcome to",
                textAlign: TextAlign.center,
                maxLines: 1,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.orangeAccent,
                    fontSize: 30,
                    shadows: [
                      Shadow(
                        color: Colors.grey.shade700,
                        offset: Offset(0, 2.5),
                        blurRadius: 3.0,
                      ),
                    ]),
              ),
            ),
            Container(
              child: Image.asset(
                "assets/iql_logo.webp",
                height: 200,
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: AutoSizeText(
                "Indianna Quiz League",
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 25,
                    shadows: [
                      Shadow(
                        color: Colors.grey.shade700,
                        offset: Offset(0, 2.5),
                        blurRadius: 3.0,
                      ),
                    ]),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: ShaderMask(
                shaderCallback: (bounds) {
                  return RadialGradient(
                    colors: <Color>[Colors.yellow, Colors.orange],
                    radius: 12.0,
                    center: Alignment.center,
                  ).createShader(
                    Rect.fromLTWH(50.0, 5.0, 5, 5.0),
                  );
                },
                child: AutoSizeText(
                  "Presented by",
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      fontStyle: FontStyle.italic,
                      color: Colors.yellow,
                      shadows: [
                        Shadow(
                          color: Colors.grey.shade700,
                          offset: Offset(0, 3.0),
                          blurRadius: 3.0,
                        ),
                      ]),
                ),
              ),
            ),
            Container(
              width: 250,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
              alignment: Alignment.center,
              child: Image.asset(
                "assets/indianna_logo.webp",
              ),
            ),
            InkWell(
              onTap: () {
                _playSound();
                Navigator.of(context).push(
                  MaterialPageRoute(
//                    builder: (context) => IQLRequestScreen(),
                    builder: (context) => UserTypeScreen(),
                  ),
                );
              },
              child: Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                width: 200,
                height: 100,
                child: Image.asset("assets/btn/continue.webp"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
