import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/attachment_model_sheet.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class OpenCandidate extends StatefulWidget {
  @override
  _OpenCandidateState createState() => _OpenCandidateState();
}

class _OpenCandidateState extends State<OpenCandidate> with WidgetsBindingObserver {
  GlobalKey _userKey, _pinCodeKey, _emailKey, _phoneKey;
  bool _isEditable = true, _isLoading = true;
  TextStyle label = new TextStyle(color: Color(0xFFFFD966));
  String authToken;
  var _name = TextEditingController();
  var _pinCode = TextEditingController();
  var _email = TextEditingController();
  var _phone = TextEditingController();
  var progressDialog;
  String profilePic;

  Map<String, dynamic> _user = {
    "active": true,
    "deleted": false,
    "_id": "",
    "name": "",
    "iqlId": "",
    "grade": "",
    "mobile": "",
    "email": "",
    "state": "",
    "city": "",
    "school": "",
    "pincode": "",
    "gender": "",
    "dob": "",
    "zone": "",
    "quiz_code": "",
  };
  User userData = new User();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }
  profileView() {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            GestureDetector(
              onTap: _isEditable
                  ? () {
                      var sheet =
                          AddAttachmentModalSheet(MediaQuery.of(context).size);
                      showModalBottomSheet(
                        context: context,
                        builder: (context) => sheet,
                        isScrollControlled: true,
                      ).then((value) {
                        if (globals.imageURL != null &&
                            globals.imageURL.isNotEmpty) {
                          setState(() {
                            profilePic = globals.imageURL;
                          });
                        }
                      });
                    }
                  : () {},
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(70.0),
                    child: FadeInImage(
                      placeholder: AssetImage("assets/icons/avtar.webp"),
                      image: profilePic != null && profilePic.isNotEmpty
                          ? NetworkImage(profilePic)
                          : AssetImage("assets/icons/avtar.webp"),
                      height: 100,
                      width: 100,
                    ),
                  ),
                  Positioned.fill(
                    bottom: 7,
                    right: 5,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: _isEditable
                          ? Container(
                              alignment: Alignment.center,
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.black,
                              ),
                              child: Image.asset(
                                "assets/icons/camera_icon.webp",
                                color: Colors.white,
                                width: 20,
                                height: 20,
                              ),
                            )
                          : Center(),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [],
            ),
          ],
        ),
        name(),
        pinCode(),
        email(),
        phone(),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              color: Colors.white,
              onPressed: () {
                _playSound();
                setState(() {
                  if (_isEditable) {
                    setState(() {
                      _isEditable = false;
                    });
                  } else {
                    setState(() {
                      _isEditable = true;
                    });
                  }
                });
              },
              child: Text(_isEditable ? "Save Profile" : "Edit Profile"),
            ),
            FlatButton(
              color: Colors.white,
              onPressed: () {
                _playSound();
                if (_isEditable) {
                  setState(() {
                    _isEditable = false;
                  });
                } else {
                  save();
                }
              },
              child: Text(_isEditable ? "Cancel" : "Save"),
            ),
          ],
        ),
      ],
    );
  }

  name() {
    var screenWidth = MediaQuery.of(context).size.width;
    _name.text = _user["name"];
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: (screenWidth - 40) * 0.35,
          child: Text(
            "Name",
            style: label,
          ),
        ),
        Container(
          width: (screenWidth - 40) * 0.54,
          child: TextField(
            key: _userKey,
            keyboardType: TextInputType.text,
            controller: _name,
            enabled: _isEditable,
            style: TextStyle(color: Colors.white),
            onChanged: (val) {
              _user["name"] = val.trim();
            },
          ),
        ),
      ],
    );
  }

  pinCode() {
    _pinCode.text = _user["pincode"];
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Pin Code",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _pinCodeKey,
              keyboardType: TextInputType.text,
              controller: _pinCode,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                _user["pincode"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  email() {
    var screenWidth = MediaQuery.of(context).size.width;
    _email.text = _user["email"];
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Mail ID",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _emailKey,
              keyboardType: TextInputType.emailAddress,
              controller: _email,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                _user["email"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  phone() {
    var screenWidth = MediaQuery.of(context).size.width;
    _phone.text = _user["mobile"];
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Phone",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _phoneKey,
              keyboardType: TextInputType.phone,
              maxLength: 10,
              controller: _phone,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                _user["mobile"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  save() async {
    Widget progressAlert;
    if (Platform.isIOS) {
      progressAlert = CupertinoAlertDialog(
        title: Text("Saving Profile..."),
        content: CupertinoActivityIndicator(),
      );
    } else {
      progressAlert = AlertDialog(
        title: Text("Saving Profile..."),
        content: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      );
    }
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        progressDialog = context;
        return progressAlert;
      },
    );
//    String profilePic = globals.imageURL;
    User userData = User.fromJson(_user);
    userData.profilePicture = profilePic;
    Map<String, dynamic> fields = {};
    if (userData.name != null) {
      fields.addAll({"name": userData.name});
    }
    if (userData.phone != null) {
      if (userData.phone.length == 10) {
        fields.addAll({"phone": "${userData.countryCode}${userData.phone}"});
      } else {
        globals.showToast(" Please Enter 10 digit Mobile Number ");
        return;
      }
    }
    if (userData.school != null) {
      fields.addAll({"school": userData.school});
    }
    if (userData.city != null) {
      fields.addAll({"city": userData.city});
    }
    if (userData.email != null) {
      if (!validateEmail(userData.email)) {
        fields.addAll({"email": userData.email});
      } else {
        globals.showToast(" Please enter valid Email ID ");
        return;
      }
    }
    if (userData.profilePicture != null && userData.profilePicture.isNotEmpty) {
      fields.addAll({"profilePicture": userData.profilePicture});
    }
    fields.addAll({'isFlowCompleted': true});

    if (fields.length > 0) {
      Map<String, dynamic> action = {
        "action": "userUpdate",
        "data": {
          "id": this.userData.id,
          "authToken": authToken,
          "stage": globals.STAGE,
          "fields": fields,
        },
      };

      var webSocket1 = WebSocketChannel.connect(globals.uri);
      webSocket1.stream.listen(output);
      print(jsonEncode(action).toString());
      webSocket1.sink.add(jsonEncode(action).toString());
    }
  }

  output(message) async {
    var object = jsonDecode(message);
    if (object.containsKey("data") && object["data"] != null) {
      if (object['msg'].toString().contains("User logged in successfully")) {
        setState(() {
          _user = object['data'];
          authToken = _user['authToken'];
        });
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] != null) {
        User userRes = User.fromJson(object['user']);
        await UserData().saveUser(userRes, authToken);
        globals.showToast(" Profile Saved Successfully ");
        await UserData().setWelcome();
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => GameList(),
          ),
          (r) => false,
        );
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] == null) {
        globals.showToast(" User Not Updated !");
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        setState(() {
          _isEditable = false;
        });
      } else {
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        if (Platform.isIOS) {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return CupertinoAlertDialog(
                title: Text("Could Not Login"),
                content: Text("${object['msg']}"),
                actions: [
                  CupertinoDialogAction(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(diaCtx).pop(),
                  )
                ],
              );
            },
          );
        } else {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return AlertDialog(
                title: Text("Could Not Login"),
                content: Text("${object['msg']}"),
                actions: [
                  FlatButton(
                    onPressed: () => Navigator.of(diaCtx).pop(),
                    child: Text("OK"),
                  ),
                ],
              );
            },
          );
        }
      }
    } else {
      if (progressDialog != null) {
        Navigator.of(progressDialog).pop();
      }
      if (Platform.isIOS) {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return CupertinoAlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                CupertinoDialogAction(
                  child: Text("OK"),
                  onPressed: () => Navigator.of(diaCtx).pop(),
                )
              ],
            );
          },
        );
      } else {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return AlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.of(diaCtx).pop(),
                  child: Text("OK"),
                ),
              ],
            );
          },
        );
      }
    }
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value);
  }

  getUser() async {
    authToken = await UserData().getAuthToken();
    userData = await UserData().getUser();
    _user["name"] = userData.name;
    _user["email"] = userData.email;
    _user["mobile"] = userData.phone;
    profilePic = userData.profilePicture;
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) getUser();
    return DecoratedBox(
      decoration: BoxDecoration(
//        image: DecorationImage(
//            image: AssetImage("assets/profile_back.webp"), fit: BoxFit.fill),
        color: Color(0xFF4B3588),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: (!_isLoading)
              ? ListView(
                  shrinkWrap: true,
                  children: [
                    Container(
                      alignment: Alignment.topCenter,
                      width: MediaQuery.of(context).size.width - 10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: GestureDetector(
                              child: Container(
                                margin: EdgeInsets.only(left: 20, bottom: 10),
                                width: 60,
                                child: Image.asset(
                                  "assets/btn/back_button.webp",
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          SizedBox(
                            width: 150,
                            height: 100,
                            child: Image.asset("assets/iql_logo.webp"),
                          ),
                          SizedBox(width: 10),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                      child: AutoSizeText(
                        "STUDENT PROFILE",
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: 28,
                          color: Color(0xffffc000),
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 15),
                      child: profileView(),
                    ),
                  ],
                )
              : Center(
                  child: globals.lottie,
                ),
        ),
      ),
    );
  }
}
