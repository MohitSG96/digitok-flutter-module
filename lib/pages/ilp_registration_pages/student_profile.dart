import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/attachment_model_sheet.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class StudentProfile extends StatefulWidget {
  final user;

  const StudentProfile({Key key, this.user}) : super(key: key);

  @override
  _StudentProfileState createState() => _StudentProfileState();
}

class _StudentProfileState extends State<StudentProfile> with WidgetsBindingObserver {
  GlobalKey _userKey,
      _userNameKey,
      _schoolKey,
      _cityKey,
      _emailKey,
      _phoneKey,
      _dobKey;
  bool _isEditable = false;
  TextStyle label = new TextStyle(
    color: Color(0xFFFFD966),
  );
  var _name = TextEditingController();
  var _gradeName = TextEditingController();
  var _school = TextEditingController();
  var _city = TextEditingController();
  var _email = TextEditingController();
  var _phone = TextEditingController();
  var _dob = TextEditingController();
  var progressDialog;

  Map<String, dynamic> previousData = {
    "active": true,
    "deleted": false,
    "_id": "",
    "name": "",
    "iqlId": "",
    "grade": "",
    "mobile": "",
    "email": "",
    "state": "",
    "city": "",
    "school": "",
    "pincode": "",
    "gender": "",
    "dob": "",
    "zone": "",
    "quiz_code": "",
    "created": "",
    "modified": "",
    "__v": 0
  };

  Map<String, dynamic> _updatedUser = {
    "active": true,
    "deleted": false,
    "_id": "",
    "name": "",
    "iqlId": "",
    "grade": "",
    "mobile": "",
    "email": "",
    "state": "",
    "city": "",
    "school": "",
    "pincode": "",
    "gender": "",
    "dob": "",
    "zone": "",
    "quiz_code": "",
    "created": "",
    "modified": "",
    "__v": 0
  };
  User _user;
  String profilePic, authToken;
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
    Screen.keepOn(true);
    if (widget.user != null) {
      _updatedUser = widget.user;
      cpUserData(_updatedUser);
      uploadProfile();
    }
  }

  cpUserData(Map<String, dynamic> user) {
    user.forEach((key, value) {
      previousData[key] = value;
    });
  }

  uploadProfile() {
    var webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    Map<String, dynamic> action = {
      "action": "ILPUserAuthentication",
      "stage": globals.STAGE,
      "data": _updatedUser
    };
    print(_updatedUser.toString());
    webSocket.sink.add(jsonEncode(action).toString());
  }

  save() async {
    Widget progressAlert;
    if (Platform.isIOS) {
      progressAlert = CupertinoAlertDialog(
        title: Text("Saving Profile..."),
        content: CupertinoActivityIndicator(),
      );
    } else {
      progressAlert = AlertDialog(
        title: Text("Saving Profile..."),
        content: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      );
    }
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        progressDialog = context;
        return progressAlert;
      },
    );
//    String profilePic = globals.imageURL;
    _user = User.fromJson(_updatedUser);
    _user.profilePicture = profilePic;
    Map<String, dynamic> fields = {};
    if (_user.name != null) {
      fields.addAll({"name": _user.name});
    }
    if (_user.phone != null) {
      if (_user.phone.length == 10) {
        fields.addAll({"phone": _user.phone});
      } else {
        globals.showToast(" Please Enter 10 digit Mobile Number ");
        return;
      }
    }
    if (_user.school != null) {
      fields.addAll({"school": _user.school});
    }
    if (_user.city != null) {
      fields.addAll({"city": _user.city});
    }
    if (_user.email != null) {
      if (!validateEmail(_user.email)) {
        fields.addAll({"email": _user.email});
      } else {
        globals.showToast(" Please enter valid Email ID ");
        return;
      }
    }
    if (_user.profilePicture != null && _user.profilePicture.isNotEmpty) {
      fields.addAll({"profilePicture": _user.profilePicture});
    }
    fields.addAll({'isFlowCompleted': true});
    if (fields.length > 0) {
      Map<String, dynamic> action = {
        "action": "userUpdate",
        "data": {
          "id": _user.id,
          "authToken": authToken,
          "stage": globals.STAGE,
          "fields": fields,
        },
      };

      var webSocket1 = WebSocketChannel.connect(globals.uri);
      webSocket1.stream.listen(output);
//      print(jsonEncode(action).toString());
      webSocket1.sink.add(jsonEncode(action).toString());
    }
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value);
  }

  output(message) async {
    var object = jsonDecode(message);
    if (object.containsKey("data") && object["data"] != null) {
      if (object['msg'].toString().contains("User logged in successfully")) {
        setState(() {
          _updatedUser = object['data'];
          authToken = _updatedUser['authToken'];
        });
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] != null) {
        User userRes = User.fromJson(object['user']);
        await UserData().saveUser(userRes, authToken);
        globals.showToast(" Profile Saved Successfully ");
        await UserData().setWelcome();
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => GameList(),
          ),
              (r) => false,
        );
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] == null) {
        globals.showToast(" User Not Updated !");
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        setState(() {
          _isEditable = false;
        });
      } else {
        if (progressDialog != null) {
          Navigator.of(progressDialog).pop();
        }
        if (Platform.isIOS) {
          showDialog(
            context: context,
            builder: (diaCtx) {
              return CupertinoAlertDialog(
                title: Text("Could Not Login"),
                content: Text("${object['msg']}"),
                actions: [
                  CupertinoDialogAction(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(diaCtx).pop(),
                  )
                ],
              );
            },
          );
        } else {
          if (progressDialog != null) {
            Navigator.of(progressDialog).pop();
          }
          showDialog(
            context: context,
            builder: (diaCtx) {
              return AlertDialog(
                title: Text("Could Not Login"),
                content: Text("${object['msg']}"),
                actions: [
                  FlatButton(
                    onPressed: () => Navigator.of(diaCtx).pop(),
                    child: Text("OK"),
                  ),
                ],
              );
            },
          );
        }
      }
    } else {
      if (progressDialog != null) {
        Navigator.of(progressDialog).pop();
      }
      if (Platform.isIOS) {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return CupertinoAlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                CupertinoDialogAction(
                  child: Text("OK"),
                  onPressed: () => Navigator.of(diaCtx).pop(),
                )
              ],
            );
          },
        );
      } else {
        showDialog(
          context: context,
          builder: (diaCtx) {
            return AlertDialog(
              title: Text("Something Went Wrong"),
              content: Text("${object['message']}"),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.of(diaCtx).pop(),
                  child: Text("OK"),
                ),
              ],
            );
          },
        );
      }
    }
  }

  profileView() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(20, 2, 20, 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(7),
                decoration: BoxDecoration(
                  color: Color(0xFF4472C4),
                  border: Border.all(color: Color(0xFF2F528F), width: 1.5),
                  borderRadius: BorderRadius.circular(7),
                ),
                child: AutoSizeText(
                  "CODE",
                  style: TextStyle(fontSize: 22),
                  maxLines: 1,
                ),
              ),
              Container(
                padding: EdgeInsets.all(7),
                decoration: BoxDecoration(
                  color: Color(0xFF4472C4),
                  border: Border.all(color: Color(0xFF2F528F), width: 1.5),
                  borderRadius: BorderRadius.circular(7),
                ),
                child: AutoSizeText(
                  "MH1024",
                  style: TextStyle(fontSize: 22),
                  maxLines: 1,
                ),
              ),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            GestureDetector(
              onTap: _isEditable
                  ? () {
                var sheet =
                AddAttachmentModalSheet(MediaQuery
                    .of(context)
                    .size);
                showModalBottomSheet(
                  context: context,
                  builder: (context) => sheet,
                  isScrollControlled: true,
                ).then((value) {
                  if (globals.imageURL != null &&
                      globals.imageURL.isNotEmpty) {
                    setState(() {
                      profilePic = globals.imageURL;
                    });
                  }
                });
              }
                  : () {},
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(70.0),
                    child: FadeInImage(
                      placeholder: AssetImage("assets/icons/avtar.webp"),
                      image: profilePic != null && profilePic.isNotEmpty
                          ? NetworkImage(profilePic)
                          : AssetImage("assets/icons/avtar.webp"),
                      height: 100,
                      width: 100,
                    ),
                  ),
                  Positioned.fill(
                    bottom: 7,
                    right: 5,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: _isEditable
                          ? Container(
                        alignment: Alignment.center,
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                        child: Image.asset(
                          "assets/icons/camera_icon.webp",
                          color: Colors.white,
                          width: 20,
                          height: 20,
                        ),
                      )
                          : Center(),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [],
            ),
          ],
        ),
        name(),
        className(),
        school(),
        city(),
        email(),
        phone(),
        dob(),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  if (_isEditable) {
                    cpUserData(_updatedUser);
                    setState(() {
                      _isEditable = false;
                    });
                  } else {
                    setState(() {
                      _isEditable = true;
                    });
                  }
                });
              },
              child: Text(_isEditable ? "Save Profile" : "Edit Profile"),
            ),
            FlatButton(
              color: Colors.white,
              onPressed: () {
                if (_isEditable) {
                  previousData.forEach((key, value) {
                    _updatedUser[key] = value;
                  });
                  setState(() {
                    _isEditable = false;
                  });
                } else {
                  save();
                }
              },
              child: Text(_isEditable ? "Cancel" : "Save"),
            ),
          ],
        ),
      ],
    );
  }

  name() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    _name.text = _updatedUser["name"];
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: (screenWidth - 40) * 0.35,
          child: Text(
            "Name",
            style: label,
          ),
        ),
        Container(
          width: (screenWidth - 40) * 0.54,
          child: TextField(
            key: _userKey,
            keyboardType: TextInputType.text,
            controller: _name,
            enabled: _isEditable,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(hintText: "Enter Your Name"),
            onChanged: (val) {
              _updatedUser["name"] = val.trim();
            },
          ),
        ),
      ],
    );
  }

  className() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    _gradeName.text = _updatedUser["grade"];
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Class",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _userNameKey,
              keyboardType: TextInputType.text,
              controller: _gradeName,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(hintText: "Enter Your Class"),
              onChanged: (val) {
                _updatedUser["grade"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  school() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    _school.text = _updatedUser["school"];
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "School",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _schoolKey,
              keyboardType: TextInputType.text,
              controller: _school,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(hintText: "Enter Your School Name"),
              onChanged: (val) {
                _updatedUser["school"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  city() {
    _city.text = _updatedUser["city"];
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "City",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _cityKey,
              keyboardType: TextInputType.text,
              controller: _city,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(hintText: "Enter City"),
              onChanged: (val) {
                _updatedUser["city"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  email() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    _email.text = _updatedUser["email"];
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Mail ID",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _emailKey,
              keyboardType: TextInputType.emailAddress,
              controller: _email,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(hintText: "Enter Your Mail ID"),
              onChanged: (val) {
                _updatedUser["email"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  phone() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    _phone.text = _updatedUser["mobile"];
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Phone",
              style: label,
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _phoneKey,
              keyboardType: TextInputType.phone,
              maxLength: 10,
              controller: _phone,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(hintText: "Enter Phone Number"),
              onChanged: (val) {
                _updatedUser["mobile"] = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  dob() {
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    String dob = "";
    DateTime formattedDate;
    if (_updatedUser.containsKey("dob") &&
        _updatedUser["dob"] != null &&
        _updatedUser["dob"]
            .trim()
            .isNotEmpty) {
      dob = _updatedUser["dob"];
      formattedDate = DateFormat("yyyy-MM-dd").parse(dob);
    }
    return StatefulBuilder(
      builder: (context, setState) {
        _dob.text = dob;
        return Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: (screenWidth - 40) * 0.35,
                child: Text(
                  "Date of Birth",
                  style: label,
                ),
              ),
              Container(
                width: (screenWidth - 40) * 0.54,
                child: TextField(
                  key: _dobKey,
                  keyboardType: TextInputType.datetime,
                  controller: _dob,
                  enabled: _isEditable,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(hintText: "Select Date of Birth"),
                  onTap: () async {
                    DateTime date = DateTime(1900);
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (Platform.isIOS) {
                      showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return Container(
                            child: CupertinoDatePicker(
                              mode: CupertinoDatePickerMode.date,
                              onDateTimeChanged: (value) {
                                setState(() {
                                  dob = DateFormat("yyyy-MM-dd").format(value);
                                  _updatedUser["dob"] = dob;
                                });
                              },
                              maximumYear: DateTime
                                  .now()
                                  .year,
                              initialDateTime: formattedDate ?? DateTime.now(),
                            ),
                          );
                        },
                      );
                    } else {
//                  DateTime today = DateTime(DateTime.now().year - 5);
                      date = await showDatePicker(
                        context: context,
                        initialDate: formattedDate ?? DateTime.now(),
                        firstDate: DateTime(1950),
                        lastDate: DateTime.now(),
                      );
                      if (date != null) {
                        setState(() {
                          dob = DateFormat("yyyy-MM-dd").format(date);
                          _updatedUser["dob"] = dob;
                        });
                      }
                    }
                  },
                  onChanged: (val) {
                    _updatedUser["dob"] = val;
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
//        image: DecorationImage(
//            image: AssetImage("assets/profile_back.webp"), fit: BoxFit.fill),
        color: Color(0xFF4B3588),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: ListView(
            shrinkWrap: true,
            children: [
              SizedBox(
                width: 150,
                height: 100,
                child: Image.asset("assets/iql_logo.webp"),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                child: AutoSizeText(
                  "STUDENT PROFILE",
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 28,
                    color: Color(0xffffc000),
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(10, 5, 10, 15),
                child: profileView(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
