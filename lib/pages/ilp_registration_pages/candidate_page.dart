import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class IQLInfoScreen extends StatefulWidget {
  @override
  _IQLInfoScreenState createState() => _IQLInfoScreenState();
}

class _IQLInfoScreenState extends State<IQLInfoScreen> with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top + 5),
          Align(
            alignment: Alignment.bottomLeft,
            child: GestureDetector(
              child: Container(
                margin: EdgeInsets.only(left: 20, bottom: 10),
                width: 60,
                child: Image.asset(
                  "assets/btn/back_button.webp",
                  color: Colors.black,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Image.asset(
            "assets/iql_logo.webp",
            height: 220,
            width: 350,
            fit: BoxFit.fitWidth,
          ),
          SizedBox(height: 15),
          Container(
            child: AutoSizeText(
              "You need a unique IQL code to participate in IQL." +
                  "\nTo generate the code, please download ILP and register" +
                  " yourself there. Once you have it come back here",
              textAlign: TextAlign.center,
              maxLines: 8,
              style: TextStyle(
                color: Color(0xFF002060),
                fontSize: 25,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          SizedBox(height: 15),
          InkWell(
            onTap: () {
              _playSound();
              globals.showToast("Please Download ILP app from App Store.");
            },
            child: Container(
              child: Image.asset(
                "assets/btn/btn_download.webp",
                fit: BoxFit.fill,
                height: 40,
              ),
            ),
          ),
          SizedBox(height: 15),
          InkWell(
            onTap: () {
              _playSound();
              Navigator.of(context).pop();
            },
            child: Container(
              child: Image.asset(
                "assets/btn/btn_back.webp",
                fit: BoxFit.fill,
                height: 40,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
