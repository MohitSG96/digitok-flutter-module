import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:digitok/models/game_feature.dart';
import 'package:digitok/models/question.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/request_question.dart';
import 'package:digitok/utils/db_helper.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class CountDownScreen extends StatefulWidget {
//  final GameFeatures feature;
  final questionList;

  const CountDownScreen({Key key, @required this.questionList})
      : super(key: key);

  @override
  _CountDownScreenState createState() => _CountDownScreenState();
}

class _CountDownScreenState extends State<CountDownScreen>
    with WidgetsBindingObserver {
  int seconds = 30;
  Duration count;
  Timer _timer;
  GameFeatures _gameFeature;
  var webSocket;
  var webSocket1;
  var _authToken;
  int totalUsers = 0;
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    seconds = globals.countdown;
    count = Duration(seconds: seconds);
    initPlayer();
    PlayAudio();
    runTimer();
//    _gameFeature = widget.feature;

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);

    /*webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);*/

    CheckUserOnline();

    fetchQuestions();

    WidgetsBinding.instance.addObserver(this);
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };

    print(jsonEncode(action).toString());
//    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        print(object['data']);
        if (object['data'].length > 0) {
          var data = object['data'][0];
          totalUsers = data['totalUsers'];
        }
        globals.totalUsersOnline = totalUsers;
      }
    }
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);

    if (object.containsKey("msg")) {
      if (object['msg'].toString().contains("Question list found")) {
        var data = object['data'];
        saveQuestions(data);
      } else {
        globals.showToast("No Questions Found");
        _timer.cancel();
        Navigator.of(context).pop();
      }
    } else {
      globals.showToast("Something went wrong\nPlease come again in sometime");
      Navigator.of(context).pop();
      _timer.cancel();
    }
  }

  saveQuestions(questions) async {
    DBHelper helper = DBHelper();
    await helper.drop();
    await helper.create();
    globals.totalQuestions = questions.length;
    print(globals.totalQuestions);
    print(questions);
    globals.questionNumber = 1;
    for (var question in questions) {
//      print("Saving Question:${question['id']}");
      Question english = Question.fromMap(question);
//      HindiQuestion hindi = HindiQuestion.fromMap(question);
      helper.saveEnglish(english);
//      helper.saveHindi(hindi);

      if ((english.media != null && english.media.isNotEmpty) &&
          globals.image_media.contains(
              english.media.substring(english.media.lastIndexOf(".")))) {
        cacheImages(english.media);
      }

      if ((english.preMedia != null && english.preMedia.isNotEmpty) &&
          globals.image_media.contains(
              english.preMedia.substring(english.preMedia.lastIndexOf(".")))) {
        cacheImages(english.preMedia);
      }

      if ((english.questionMedia != null && english.questionMedia.isNotEmpty) &&
          globals.image_media.contains(english.questionMedia
              .substring(english.questionMedia.lastIndexOf(".")))) {
        cacheImages(english.questionMedia);
      }
    }
  }

  cacheImages(url) async {
    CachedNetworkImage(
      imageUrl: url,
    );
  }

  fetchQuestions() async {
    globals.questionNumber = 1;
    _authToken = await UserData().getAuthToken();
    var questionList = widget.questionList;
    Map<String, dynamic> action = {
      "action": "questionsList",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
        "condition": {"id": questionList}
      }
    };
    var data = jsonEncode(action).toString();
    webSocket.sink.add(data);
  }

  runTimer() {
    int time = seconds;
    _timer = new Timer.periodic(Duration(seconds: 1), (Timer timer) {
      if (timer.tick <= time) {
        setState(() {
          seconds--;
          count = Duration(seconds: seconds);
        });
      } else {
//        Navigator.of(context).pushReplacement(MaterialPageRoute(
//          builder: (context) => QuestionScreen(),
//          settings: RouteSettings(name: '/question'),
//        ));
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => RequestQuestion()));
      }
    });
  }

  countDown() {
    var remainingTime;
    var gameStartText = "Your Game Starts in ";
    var gameBeginText = "The game will begin in ";
    remainingTime = "${(count.inSeconds % 60)}";
    if (globals.language.trim().contains("hindi")) {
      gameStartText = "खेल शुरू होता है..";
      gameBeginText = "प्रश्न 1 तक";
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        seconds > 10
            ? Text(
                gameStartText,
                style: TextStyle(color: Colors.white, fontSize: 15),
              )
            : Text(
                gameBeginText,
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.center,
          width: 250,
          height: 250,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/Timer_Base.webp"),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            child: Stack(
              alignment: Alignment.center,
              children: [
                SizedBox(
                  width: 191,
                  height: 191,
                  child: CircularProgressIndicator(
                    strokeWidth: 15,
                    backgroundColor: Colors.grey,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                    value: (seconds / globals.countdown).toDouble(),
                  ),
                ),
                Container(
                  width: 191,
                  height: 191,
                  margin: EdgeInsets.all(36.5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color(0xFF4D69FF),
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black54.withOpacity(0.5),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0, 0))
                      ]),
                  child: Text(
                    remainingTime,
                    style: globals.blueGlowTextStyle(130.0),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          body: Stack(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 25, 10, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: Image.asset(
                          "assets/iql_logo.webp",
                          width: 200,
                          fit: BoxFit.fitWidth,
                        ),
                        onTap: () {
                          _exitDialog();
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
//                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: 70,
                child: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  leading: Material(
                    child: IconButton(
                      iconSize: 20,
                      icon: Image.asset("assets/icons/back_arrow.webp"),
                      onPressed: () {
                        _exitDialog();
                      },
                      tooltip: "Back",
                    ),
                    color: Colors.transparent,
                  ),
                ),
              ),
              Center(
                child: countDown(),
              ),
            ],
          ),
        ));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    if (seconds <= 10) {
      audioCache.play("countdown_10.mp3");
      advancedPlayer.seek(Duration(seconds: 10 - seconds));
    } else if (seconds <= 30) {
      audioCache.play("countdown_30.mp3");
      advancedPlayer.seek(Duration(seconds: 30 - seconds));
    } else if (seconds <= 60) {
      audioCache.play("countdown_60.mp3");
      advancedPlayer.seek(Duration(seconds: 60 - seconds));
    } else {
      audioCache.play("countdown_60.mp3");
    }
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

//endregion

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<bool> _exitDialog() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Do you want to exit the App ?"),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.pop(context, false),
                    child: Text("No")),
                FlatButton(
                    onPressed: () {
                      if (_timer != null && _timer.isActive) _timer.cancel();
                      stopAudio();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => GameList(),
                        ),
                      );
                      /*_stopSound();
                  stopAudio();*/
                    },
                    child: Text("Yes"))
              ],
            ));
  }

  Future<bool> _onBackPressed() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    return Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
  }
}
