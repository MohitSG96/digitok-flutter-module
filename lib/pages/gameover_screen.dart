import 'dart:convert';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/giftsvoucher_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class GameOverScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _GameOverState();
}

class _GameOverState extends State<GameOverScreen> with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  var webSocket1;
  var _authToken;
  int totalUsers = 0;

  var rank = "";
  var points = "";

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);

    getuser();
    CheckUserOnline();

    WidgetsBinding.instance.addObserver(this);
  }

  void getuser() async {
    _authToken = await UserData().getAuthToken();
    Map<String, dynamic> condition = {
      "GameId": globals.gameId,
    };

    Map<String, dynamic> action = {
      "action": "userSummary",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
        "condition": condition
      }
    };
    print(jsonEncode(action).toString());
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        var data = object['data'][0];
        totalUsers = data['totalUsers'];

        globals.totalUsersOnline = totalUsers;
      } else if (object['msg']
          .toString()
          .contains("User game summary successfully found!")) {
        var data = object['data'];
        setState(() {
          rank = data['rank'].toString();
          points = data['points'].toString();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(
                                "assets/followers_icon.webp",
                                width: 60,
                              ),
                              Text(
                                NumberFormat.compact()
                                    .format(globals.totalUsersOnline),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            child: Image.asset(
                              "assets/round_logo.gif",
                              width: 80,
                              height: 80,
                            ),
                            onTap: () {
                              _exitDialog();
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Center(
                            child: Image.asset("assets/gameoverimg.png",
                                width: width, height: 250),
                          ),
                        ),
                        Container(
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Image.asset("assets/points_img.png",
                                  width: 35, height: 35),
                              SizedBox(width: 10),
                              AutoSizeText(
                                "Points : $points",
                                style: TextStyle(
                                    fontSize: 26.0, color: Colors.white),
                              ),
                            ],
                          ),
                          width: width,
                        ),
                        SizedBox(height: 10),
                        Container(
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset("assets/rank_img.png",
                                  width: 35, height: 35),
                              SizedBox(width: 10),
                              AutoSizeText(
                                "Rank :",
                                style: TextStyle(
                                    fontSize: 20.0, color: Colors.white),
                              ),
                              SizedBox(width: 10),
                              AutoSizeText(
                                "$rank",
                                style: TextStyle(
                                    fontSize: 20.0, color: Colors.white),
                              )
                            ],
                          ),
                          width: width,
                        ),
                      ],
                    ),
                  ),
//                  DigiButton(
//                    onPressed: () {
//                      Navigator.of(context).pushReplacement(MaterialPageRoute(
//                        builder: (context) => LeaderBoardScreen(),
//                      ));
//                    },
//                    child: Center(
//                      child: AutoSizeText(
//                        "LeaderBoard",
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontWeight: FontWeight.bold,
//                          fontSize: 20,
//                        ),
//                      ),
//                    ),
//                    image: AssetImage("assets/main_button_background.webp"),
//                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
//                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: 70,
                child: AppBar(
                  backgroundColor: Colors.transparent,
                  leading: Material(
                    child: IconButton(
                      iconSize: 20,
                      icon: Image.asset("assets/icons/back_arrow.webp"),
                      onPressed: () {
                        _onBackPressed();
                      },
                      tooltip: "Back",
                    ),
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
        ),
        onWillPop: _onBackPressed);
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.play('game_over_sound.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

//endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _exitDialog() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Do you want to exit the App ?"),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.pop(context, false),
                    child: Text("No")),
                FlatButton(
                    onPressed: () {
                      stopAudio();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => GameList(),
                        ),
                      );
                      /*_stopSound();
                  stopAudio();*/
                    },
                    child: Text("Yes"))
              ],
            ));
  }

  Future<bool> _onBackPressed() {
    stopAudio();
    return Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GiftsVoucherScreen(),
      ),
    );
  }
}
