import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/ui_widgets/digi_button.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class GiftsVoucherScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _GiftsVoucherState();
}

class _GiftsVoucherState extends State<GiftsVoucherScreen> {
  var webSocket1;
  var _authToken;
  int totalUsers = 0;

  @override
  void initState() {
    super.initState();

    webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);

    CheckUserOnline();

    Screen.keepOn(true);
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        var data = object['data'][0];
        totalUsers = data['totalUsers'];

        globals.totalUsersOnline = totalUsers;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 5), () {
      return Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => GameList(),
        ),
      );
    });
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          body: Column(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            "assets/followers_icon.webp",
                            width: 60,
                          ),
                          Text(
                            NumberFormat.compact()
                                .format(globals.totalUsersOnline),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Image.asset(
                        "assets/round_logo.gif",
                        width: 80,
                        height: 80,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  DigiButton(
                    height: 150.0,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 25),
                      child: AutoSizeText(
                        "25,000 pts",
                        style: TextStyle(
                          color: HexColor("#F31909"),
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    image: AssetImage("assets/points_ribbon.webp"),
                  ),
                  AutoSizeText(
                    "gets you",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(height: 10),
                  DigiButton(
                    height: 150.0,
                    width: 300.0,
                    child: Padding(
                      padding: EdgeInsets.only(left: 30),
                      child: AutoSizeText(
                        "5,000/-",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    image: AssetImage("assets/voucher.webp"),
                  ),
                ],
              ))
            ],
          ),
        ),
        onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GameList(),
      ),
    );
  }
}
