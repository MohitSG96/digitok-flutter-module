import 'dart:convert';
import 'dart:io';

import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/login_signup/signup_screen.dart';
import 'package:digitok/ui_widgets/custom_dialog.dart';
import 'package:digitok/ui_widgets/digi_button.dart';
import 'package:digitok/ui_widgets/digi_edittext.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:permission_handler/permission_handler.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'package:digitok/pages/login_signup/otp_screen.dart';

class Phone_SignUpScreen extends StatefulWidget {
  final GoogleSignInAccount user;

  const Phone_SignUpScreen({Key key, this.user}) : super(key: key);

  @override
  _Phone_SignUpScreenState createState() => _Phone_SignUpScreenState();
}

class _Phone_SignUpScreenState extends State<Phone_SignUpScreen>
    with WidgetsBindingObserver {
  var channel;
  TextEditingController _userNameController, _phoneController, _emailController;
  String available = "Availability";
  IconData _check = Icons.check;
  Color _color = Colors.green;
  bool _nameIsValid = false;
  User newUser;
  FocusNode phoneFocus = FocusNode(), emailFocus = FocusNode();

  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;
  File _photo;
  String _photoUrl;
  String _photoPath;
  GoogleSignInAccount googleUser;

  @override
  void initState() {
    super.initState();

    if (widget.user != null) googleUser = widget.user;
    Screen.keepOn(true);

    _soundpool = Soundpool();
    _soundId = _loadSound();

    initPlayer();
    PlayAudio();

    channel = WebSocketChannel.connect(
        Uri.parse("wss://kpue6mvso2.execute-api.us-east-2.amazonaws.com/ss"));
    channel.stream.listen(output);
    _userNameController = TextEditingController();
    _phoneController = TextEditingController();
    _emailController = TextEditingController();
    if (googleUser != null) {
      _userNameController.text = googleUser.displayName.split(" ")[0];
      _emailController.text = googleUser.email;
    }

    WidgetsBinding.instance.addObserver(this);
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value);
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      var msg = object['msg'].toString();
      if (msg.contains("User already registered")) {
        globals.showToast("Email/Phone Number Already Exist");
        return;
      } else if (msg.contains("User registered successfully")) {
        var parsed = object['data'];
        newUser = User.fromJson(parsed);
        String authToken = parsed['authToken'];
        Future.delayed(Duration(seconds: 2), () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => OTPScreen(
              user: newUser,
              authToken: authToken,
              login: false,
            ),
          ));
        });
      } else {
        if (msg.contains("Username is available")) {
          setState(() {
            _check = Icons.check;
            _color = Colors.green;
            available = "Available";
            _nameIsValid = true;
          });
        } else if (msg.contains("Username not available")) {
          setState(() {
            _check = Icons.clear;
            _color = Colors.red;
            available = "Not Available";
            _nameIsValid = false;
          });
        } else {
          _nameIsValid = false;
          globals.showToast("Error: $msg");
        }
      }
    } else if (object.containsKey("message")) {
      globals
          .showToast("Something went Wrong\nPlease try again after sometime!");
    }
  }

  checkUserName(name) {
    var pattern = r"^[a-zA-Z0-9_]*$";
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(name)) {
      setState(() {
        _check = Icons.autorenew;
        _color = Colors.amber;
        available = "Checking";
      });
      Map<String, dynamic> action = {
        "action": "checkUser",
        "data": {
          "stage": globals.STAGE,
          "username": name,
        }
      };
      try {
        channel.sink.add(jsonEncode(action).toString());
      } on Exception catch (e) {
        channel = WebSocketChannel.connect(globals.uri);
        channel.sink.add(jsonEncode(action).toString());
      }
    } else {
      globals.showToast("UserName cannot contain special Characters.");
    }
  }

  userSignUp(uName, uEmail, uPhone) {
    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "username": uName,
      "email": uEmail,
      "phone": "91$uPhone",
    };

    if (_photoUrl != null && _photoUrl.trim().isNotEmpty) {
      data.addAll({"profilePicture": _photoUrl});
    }

    Map<String, dynamic> action = {
      "action": "userSignup",
      "data": data,
    };
    print(jsonEncode(action).toString());
    channel.sink.add(jsonEncode(action).toString());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    var userName = TextFormField(
      controller: _userNameController,
      keyboardType: TextInputType.text,
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(phoneFocus),
      decoration: InputDecoration(
        counterText: "",
        hintText: "USER NAME",
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.green)),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
        border:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
      ),
      onChanged: (val) {
        if (val.length >= 3) checkUserName(val);
      },
    );
    var email = TextFormField(
      focusNode: emailFocus,
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        counterText: "",
        hintText: "E-MAIL ID",
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.green)),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
        border:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
      ),
    );
    var phoneNumber = TextFormField(
      focusNode: phoneFocus,
      controller: _phoneController,
      keyboardType: TextInputType.phone,
      maxLength: 10,
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(emailFocus),
      decoration: InputDecoration(
        counterText: "",
        hintText: "PHONE NUMBER",
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.green)),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
        border:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
      ),
    );

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: WillPopScope(
          onWillPop: _onBackPressed,
          child: Scaffold(
              backgroundColor: Colors.transparent,
              resizeToAvoidBottomPadding: true,
              appBar: AppBar(
                backgroundColor: Color(0x00000000),
                      elevation: 0,
                leading: IconButton(
                  icon: Image.asset("assets/icons/back_arrow.webp"),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => SignUpScreen()));
                  },
                  tooltip: "Back",
                ),
              ),
              body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(width * 0.01, height * 0.02,
                        width * 0.01, height * 0.02),
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.fromLTRB(width * 0.05, height * 0.05,
                        width * 0.05, height * 0.01),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        alignment: Alignment.center,
                        image: AssetImage("assets/base.webp"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: SizedBox(
                      child: Padding(
                        padding: EdgeInsets.only(left: 25, right: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "YOUR PROFILE",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xff3b5998),
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _onAddPhotoClicked(context);
                              },
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(70.0),
                                    child: _photoUrl != null &&
                                            _photoUrl.trim().isNotEmpty
                                        ? Image.network(
                                            _photoUrl,
                                            width: 100,
                                            height: 100,
                                          )
                                        : _photoPath != null &&
                                                _photoPath.trim().isNotEmpty
                                            ? _photoPath.contains("http://") ||
                                                    _photoPath
                                                        .contains("https://")
                                                ? Image.network(
                                                    _photoPath,
                                                    width: 100,
                                                    height: 100,
                                                  )
                                                : Image.file(
                                                    new File(_photoPath),
                                                    width: 100,
                                                    height: 100,
                                                  )
                                            : (googleUser != null)
                                                ? googleUser.photoUrl
                                                        .trim()
                                                        .isNotEmpty
                                                    ? Image.network(
                                                        googleUser.photoUrl,
                                                        width: 100,
                                                        height: 100,
                                                      )
                                                    : Image.asset(
                                                        "assets/icons/avtar.webp",
                                                        width: 100,
                                                        height: 100,
                                                      )
                                                : Image.asset(
                                                    "assets/icons/avtar.webp",
                                                    width: 100,
                                                    height: 100,
                                                  ),
                                  ),
                                  Positioned.fill(
                                    bottom: 7,
                                    right: 5,
                                    child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.black,
                                        ),
                                        child: Image.asset(
                                          "assets/icons/camera_icon.webp",
                                          color: Colors.white,
                                          width: 20,
                                          height: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 5),
                            DigiEditText(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 2),
                                    width: 200,
                                    child: userName,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 3),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(available),
                                SizedBox(width: 5),
                                Icon(
                                  _check,
                                  color: _color,
                                  size: 17,
                                ),
                                SizedBox(width: 20),
                              ],
                            ),
                            SizedBox(height: 7),
                            DigiEditText(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 2),
                                    width: 200,
                                    child: phoneNumber,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 12),
                            DigiEditText(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 2),
                                    width: 200,
                                    child: email,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 5),
                            DigiButton(
                              child: Text("Submit"),
                              onPressed: () async {
                                _playSound();
                                String uName = _userNameController.text;
                                String uPhone = _phoneController.text;
                                String uEmail = _emailController.text;

                                if (uName == null ||
                                    uName.isEmpty ||
                                    uName.length < 3) {
                                  globals.showToast(
                                      "Please Enter User Name with at least 3 Characters");
                                  return;
                                }
                                checkUserName(uName);
                                if (uPhone == null || uPhone.isEmpty) {
                                  globals
                                      .showToast("Please Enter Phone Number");
                                  return;
                                }
                                if (uPhone.length != 10) {
                                  globals.showToast(
                                      "Phone Number must contain 10 digits");
                                  return;
                                }
                                if (uEmail == null || uEmail.isEmpty) {
                                  globals.showToast("Please Enter Email ID");
                                  return;
                                }
                                if (validateEmail(uEmail)) {
                                  globals.showToast("Email ID is not Valid");
                                  return;
                                }
                                globals.showToast("Please Wait...");
                                bool connection = false;

                                try {
                                  final result = await InternetAddress.lookup(
                                      'google.com');
                                  if (result.isNotEmpty &&
                                      result[0].rawAddress.isNotEmpty) {
                                    connection = true;
                                  } else {
                                    connection = false;
                                  }
                                } on SocketException catch (_) {
                                  connection = false;
                                }
                                if (connection) {
                                  Future.delayed(Duration(seconds: 2), () {
                                    if (!_nameIsValid) {
                                      globals
                                          .showToast("User Name Already Taken");
                                      return;
                                    }
                                    userSignUp(uName, uEmail, uPhone);
                                  });
                                } else {
                                  showDialog(
                                    context: this.context,
                                    builder: (diaCxt) {
                                      return AlertDialog(
                                        actions: [
                                          CupertinoButton(
                                              child: Text("OK"),
                                              color: Colors.red,
                                              onPressed: () {
                                                Navigator.of(diaCxt).pop();
                                              })
                                        ],
                                        title: Text("Failed to connect"),
                                        content: Text(
                                            "No internet connection found."),
                                      );
                                    },
                                  );
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ))),
    );
  }

  _onAddPhotoClicked(context) async {
    Permission permission;

    if (Platform.isIOS) {
      permission = Permission.photos;
    } else {
      permission = Permission.storage;
    }

    PermissionStatus permissionStatus = await permission.status;

    print(permissionStatus);

    if (permissionStatus == PermissionStatus.restricted) {
      _showOpenAppSettingsDialog(context);

      permissionStatus = await permission.status;

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.permanentlyDenied) {
      _showOpenAppSettingsDialog(context);

      permissionStatus = await permission.status;

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.undetermined) {
      permissionStatus = await permission.request();

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.denied) {
      if (Platform.isIOS) {
        _showOpenAppSettingsDialog(context);
      } else {
        permissionStatus = await permission.request();
      }

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.granted) {
      ifPermissionGrant();
      print('Permission granted');
    }
  }

  _showOpenAppSettingsDialog(context) {
    return CustomDialog.show(
      context,
      'Permission needed',
      'Photos permission is needed to select photos',
      'Open settings',
      openAppSettings,
    );
  }

  ifPermissionGrant() async {
    print('Permission granted');
    File image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );

    if (image != null) {
      File croppedImage = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
        ],
        androidUiSettings: AndroidUiSettings(
          initAspectRatio: CropAspectRatioPreset.square,
          hideBottomControls: true,
        ),
        iosUiSettings: IOSUiSettings(
          aspectRatioPickerButtonHidden: true,
        ),
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 30,
      );
//      print(croppedImage.path);

      DateTime now = DateTime.now();
      String date = (now.day.toString()) +
          "/" +
          (now.month.toString()) +
          "/" +
          now.year.toString();

      String filename = date +
          "/" +
          (now.hour.toString()) +
          ":" +
          (now.minute.toString()) +
          "/" +
          path.basename(croppedImage.path);
      try {
        final result = await InternetAddress.lookup("google.com");
        String uploadedImageUrl;
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          uploadedImageUrl = await AmazonS3Cognito.upload(
              croppedImage.path,
              globals.BUCKET_ID,
              globals.POOL_ID,
              filename,
              AwsRegion.US_EAST_2,
              AwsRegion.US_EAST_2);
        } else {
          throw ("No Internet Connected");
        }
//        print(uploadedImageUrl);
        if (uploadedImageUrl != null &&
            (uploadedImageUrl.trim().isEmpty ||
                uploadedImageUrl.toLowerCase().contains("failed"))) {
          showDialog(
            context: this.context,
            builder: (diaCxt) {
              return AlertDialog(
                actions: [
                  CupertinoButton(
                      child: Text("OK"),
                      color: Colors.red,
                      onPressed: () {
                        Navigator.of(diaCxt).pop();
                      })
                ],
                title: Text("Image Upload Failed!"),
                content: Text("Image Not uploaded to server."),
              );
            },
          );
        } else {
          setState(() {
            _photoPath = uploadedImageUrl;
          });
        }
      } on Exception catch (e) {
        showDialog(
          context: this.context,
          builder: (diaCxt) {
            return AlertDialog(
              actions: [
                CupertinoButton(
                    child: Text("OK"),
                    color: Colors.red,
                    onPressed: () {
                      Navigator.of(diaCxt).pop();
                    })
              ],
              title: Text("Image Upload Failed!"),
              content: Text("No internet connection"),
            );
          },
        );
      }

      /*GalleryItem(
        id: Uuid().v1(),
        resource: image.path,
        isSvg: fileExtension.toLowerCase() == ".svg",
      );

      setState(() {
        _photo = image;
      });
      GenerateImageUrl generateUrl = GenerateImageUrl();
      await generateUrl.call(fileExtension);*/
    }
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => SignUpScreen()));
  }
}
