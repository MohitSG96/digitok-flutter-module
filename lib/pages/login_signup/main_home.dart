import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:digitok/pages/login_signup/login.dart';
import 'package:digitok/pages/login_signup/phone_signup.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';

class MainHomeScreen extends StatefulWidget {
  @override
  _MainHomeScreenState createState() => _MainHomeScreenState();
}

class _MainHomeScreenState extends State<MainHomeScreen>
    with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    _soundpool = Soundpool();
    _soundId = _loadSound();

    initPlayer();
    PlayAudio();

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                child: Image.asset(
                  "assets/round_logo.gif",
                  width: 250,
                  height: 250,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 0.0),
              child: GestureDetector(
                onTap: () {
                  _playSound();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                child: Image.asset(
                  'assets/login.webp',
                  fit: BoxFit.fill, // this is the solution for border
                  width: width,
                  height: 75.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: GestureDetector(
                onTap: () {
                  _playSound();
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => Phone_SignUpScreen()));
                },
                child: Image.asset(
                  'assets/signup.webp',
                  fit: BoxFit.fill,
                  width: width,
                  height: 75.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
  }
}
