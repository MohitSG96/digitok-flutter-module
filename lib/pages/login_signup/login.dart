import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/login_signup/main_home.dart';
import 'package:digitok/pages/login_signup/otp_screen.dart';
import 'package:digitok/pages/login_signup/signup_screen.dart';
import 'package:digitok/ui_widgets/digi_button.dart';
import 'package:digitok/ui_widgets/digi_edittext.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> with WidgetsBindingObserver {
  TextEditingController _phoneController;
  bool _isLoading = false;
  var webSocket;
  Color purple = Color(0xff7911d8);

  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();

    _phoneController = TextEditingController();
    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);

    WidgetsBinding.instance.addObserver(this);
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object.containsKey("data") && object['data'] != null) {
        var parsed = object['data'];
        User user = User.fromJson(parsed);
        String authToken = parsed['authToken'];
        Fluttertoast.showToast(
          msg: " Sending OTP ",
          textColor: Colors.white,
          backgroundColor: purple,
        );
        setState(() {
          _isLoading = false;
        });
        Future.delayed(Duration(seconds: 2), () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => OTPScreen(
              user: user,
              authToken: authToken,
              login: true,
            ),
          ));
        });
      } else {
        Fluttertoast.showToast(
          msg: object['msg'].toString(),
          textColor: Colors.white,
          backgroundColor: purple,
        );
        setState(() {
          _isLoading = false;
        });
      }
    } else if (object.containsKey("message")) {
      Fluttertoast.showToast(
        msg: "Error: " + object['message'].toString(),
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.deepPurpleAccent,
        textColor: Colors.white,
      );
      setState(() {
        _isLoading = false;
      });
    } else {
      Fluttertoast.showToast(
        msg: "Something Went Wrong",
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.deepPurpleAccent,
        textColor: Colors.white,
      );
      setState(() {
        _isLoading = false;
      });
    }
  }

  checkLogin(number) {
    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "username": "91" + number.toString(),
      "channel": "sms",
    };
    Map<String, dynamic> action = {
      "data": data,
      "action": "userSignin",
    };
    webSocket.sink.add(jsonEncode(action).toString());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    var mobile = TextField(
      autofocus: true,
      keyboardType: TextInputType.phone,
      maxLength: 10,
      controller: _phoneController,
      decoration: InputDecoration(
        counterText: "",
        hintText: "Mobile Number",
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.green)),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
        border:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
      ),
    );

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: WillPopScope(
          onWillPop: _onBackPressed,
          child: Scaffold(
            resizeToAvoidBottomPadding: false,
            backgroundColor: Colors.transparent,
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.fromLTRB(
                    width * 0.01, height * 0.26, width * 0.01, height * 0.05),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/signup_file.gif"),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: Padding(
                    padding: EdgeInsets.only(left: 40, right: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AutoSizeText(
                          "Enter 10 digit Mobile Number and tap GO to receive your Verification Code",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w900,
                              fontSize: 16.5,
                              color: Colors.grey.shade800),
                        ),
                        SizedBox(height: 10),
                        DigiEditText(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Text(
                                "+91",
                                style: TextStyle(fontSize: 18),
                              ),
                              SizedBox(width: 10),
                              Container(
                                padding: EdgeInsets.only(bottom: 2),
                                width: 150,
                                child: mobile,
                              ),
                            ],
                          ),
                        ),
                        DigiButton(
                          child: _isLoading
                              ? Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("GO"),
                                    SizedBox(width: 10),
                                    SizedBox(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2.0,
                                        valueColor: AlwaysStoppedAnimation(
                                            Colors.white),
                                      ),
                                    ),
                                  ],
                                )
                              : Text("GO"),
                          onPressed: () {
                            _playSound();
                            FocusScope.of(context).unfocus();
                            setState(() {
                              if (_phoneController.text.length != 10) {
                                Fluttertoast.showToast(
                                  msg: "Please Enter 10 digit Mobile number.",
                                  backgroundColor: purple,
                                  textColor: Colors.white,
                                  toastLength: Toast.LENGTH_LONG,
                                );
                              } else {
                                Fluttertoast.showToast(
                                  msg: "Please Wait...",
                                  backgroundColor: purple,
                                  textColor: Colors.white,
                                  toastLength: Toast.LENGTH_LONG,
                                );
                                /*Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => OTPScreen(
                            login: true,
                          ),
                        ));*/
                                checkLogin(_phoneController.text);
                                setState(() {
                                  _isLoading = true;
                                });
                              }
                            });
                          },
                        ),
                        DigiButton(
                          child: Text("Sign Up"),
                          onPressed: () {
                            _playSound();
                            FocusScope.of(context).unfocus();
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                              builder: (context) => SignUpScreen(),
                            ));
                          },
                        ),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                            text: "By Signing in, you agree to the ",
                            children: <TextSpan>[
                              TextSpan(
                                  text: "Terms of Use",
                                  style: TextStyle(
                                      decoration: TextDecoration.underline)),
                              TextSpan(text: " and "),
                              TextSpan(
                                  text: "Privacy Policy",
                                  style: TextStyle(
                                      decoration: TextDecoration.underline)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )),
    );
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
    webSocket.sink.close();
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => MainHomeScreen()));
  }
}
