import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/login_signup/main_home.dart';
import 'package:digitok/pages/login_signup/phone_signup.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:screen/screen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen>
    with WidgetsBindingObserver {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  GoogleSignInAccount _googleUser;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: WillPopScope(
          onWillPop: _onBackPressed,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: true,
            body: Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.fromLTRB(
                  width * 0.01, height * 0.26, width * 0.01, height * 0.05),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/signup_file.gif"),
                  fit: BoxFit.fitHeight,
                ),
              ),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Padding(
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Image.asset(
                          'assets/frame_fb.webp',
                          fit: BoxFit.fill,
                          width: width,
                          height: 70.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Image.asset(
                          'assets/frame_twitter.webp',
                          fit: BoxFit.fill,
                          width: width,
                          height: 70.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _googleFirebaseSignIn().then((result) {
                            if (result != null) {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                builder: (context) => Phone_SignUpScreen(
                                  user: _googleUser,
                                ),
                              ));
                            }
                          });
                        },
                        child: Image.asset(
                          'assets/frame_google.webp',
                          fit: BoxFit.fill,
                          width: width,
                          height: 70.0,
                        ),
                      ),
                      AutoSizeText(
                        "-  or  -",
                        style: TextStyle(
                          color: HexColor("#808080"),
                          fontSize: 22.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                              builder: (context) => Phone_SignUpScreen()));
                        },
                        child: Image.asset(
                          'assets/frame_ph_no.webp',
                          fit: BoxFit.fill,
                          width: width,
                          height: 70.0,
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                            text: "By Signing in, you agree to the ",
                            children: <TextSpan>[
                              TextSpan(
                                  text: "Terms of Use",
                                  style: TextStyle(
                                      decoration: TextDecoration.underline)),
                              TextSpan(text: " and "),
                              TextSpan(
                                  text: "Privacy Policy",
                                  style: TextStyle(
                                      decoration: TextDecoration.underline)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }

  _googleFirebaseSignIn() async {
    await Firebase.initializeApp();
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      _googleUser = googleSignInAccount;
      return '$googleSignInAccount';
    }

    return null;
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => MainHomeScreen()));
  }
}
