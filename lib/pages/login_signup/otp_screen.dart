import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/ilp_registration_pages/welcome_page.dart';
import 'package:digitok/pages/login_signup/login.dart';
import 'package:digitok/pages/login_signup/phone_signup.dart';
import 'package:digitok/ui_widgets/digi_edittext.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class OTPScreen extends StatefulWidget {
  final User user;
  final String authToken;
  final bool login;

  const OTPScreen({Key key, this.user, this.authToken, this.login})
      : super(key: key);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> with WidgetsBindingObserver {
  var channel;
  User user;
  String authToken, otpCode;
  Color purple = Color(0xff7911d8);

  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    channel = WebSocketChannel.connect(
        Uri.parse("wss://kpue6mvso2.execute-api.us-east-2.amazonaws.com/ss"));
    user = widget.user ?? null;
    authToken = widget.authToken;
    channel.stream.listen(output);
    if (user != null)
      requestOtp();
    else
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ));

    WidgetsBinding.instance.addObserver(this);
  }

  output(message) async {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      String msg = object['msg'].toString();
      if (msg.contains("OTP is Sent")) {
        //ignore
      } else if (msg.contains("OTP is not verified")) {
        Fluttertoast.showToast(
          msg: "Invalid OTP",
          textColor: Colors.white,
          backgroundColor: purple,
        );
      } else if (msg.contains("OTP is verified")) {
        Fluttertoast.showToast(
          msg: "OTP Verified",
          textColor: Colors.white,
          backgroundColor: purple,
        );
        await UserData().saveUser(user, authToken);
        globals.authToken = authToken;
        String welcome = await UserData().getWelcome();
        /*Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => IQLWelcome(),
        ));*/
        if(user.flowCompleted){
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => GameList(),
          ));
        } else {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => IQLWelcome(),
          ));
        }
      } else {
        Fluttertoast.showToast(
          msg: "Please try again after sometime",
          textColor: Colors.white,
          backgroundColor: purple,
        );
      }
    } else {
      Fluttertoast.showToast(
        msg: "Please try again after sometime",
        textColor: Colors.white,
        backgroundColor: purple,
      );
    }
  }

  requestOtp() {
    Map<String, dynamic> send = {
      "action": "sendOTP",
      "data": {
        "phone": "${user.countryCode}${user.phone}",
        "channel": "sms",
        "stage": globals.STAGE,
      }
    };
    channel.sink.add(jsonEncode(send).toString());
  }

  verifyOTP(value) {
    Map<String, dynamic> action = {
      "action": "verifyOTP",
      "data": {
        "phone": "${user.countryCode}${user.phone}",
        "code": value,
      }
    };
    channel.sink.add(jsonEncode(action).toString());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: WillPopScope(
          onWillPop: _onBackPressed,
          child: Scaffold(
              backgroundColor: Colors.transparent,
              resizeToAvoidBottomPadding: false,
              body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.fromLTRB(
                      width * 0.01, height * 0.26, width * 0.01, height * 0.05),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/signup_file.gif"),
                      fit: BoxFit.fitHeight
                    ),
                  ),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: Padding(
                      padding: EdgeInsets.only(left: 45, right: 45),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          DigiEditText(
                            height: 40,
                            child: Container(
                              height: 40,
                              alignment: Alignment.center,
                              child: AutoSizeText(
                                "Mobile Verification",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xff377238),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                          AutoSizeText(
                            "Enter the Verification Code" + "\nSent to ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 16.5,
                                color: Colors.grey.shade800),
                          ),
                          SizedBox(height: 5),
                          Container(
                            width: 300,
                            height: 82,
                            padding: EdgeInsets.fromLTRB(12, 17, 12, 0),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/btn2.webp"),
                                  alignment: Alignment.topCenter,
                                  fit: BoxFit.fill),
                            ),
                            child: PinCodeTextField(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              length: 4,
                              textInputType: TextInputType.number,
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.underline,
                                fieldHeight: 45,
                                fieldWidth: 30,
                                borderWidth: 1.0,
                                activeColor: Colors.black,
                                inactiveColor: Colors.black,
                                selectedColor: Color(0xff006400),
                              ),
                              backgroundColor: Colors.transparent,
                              onChanged: (val) {},
                              onCompleted: user != null ? verifyOTP : null,
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                          SizedBox(height: 5),
                          InkWell(
                            child: Container(
                              width: 250,
                              height: 80,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/btn3.webp"),
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            onTap: requestOtp,
                          ),
                          SizedBox(height: 5),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey.shade800),
                              text: "By Signing in, you agree to the ",
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Terms of Use",
                                    style: TextStyle(
                                        decoration: TextDecoration.underline)),
                                TextSpan(text: " and "),
                                TextSpan(
                                    text: "Privacy Policy",
                                    style: TextStyle(
                                        decoration: TextDecoration.underline)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ))),
    );
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => Phone_SignUpScreen()));
  }
}
