import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/models/game_feature.dart';
import 'package:digitok/models/question.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/request_question.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/db_helper.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class GameScreen extends StatefulWidget {
  final Game game;

  const GameScreen({Key key, @required this.game}) : super(key: key);

  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> with WidgetsBindingObserver {
  bool _isLoading = true, _loadPractice = false;
  var _language = "";
  var webSocket;
  Game game;
  String _authToken;
  GameFeatures _gameFeature;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();

    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();

    Screen.keepOn(true);

    globals.extraLife = 0;
    globals.doubleX = false;
    globals.isBouncer = true;
    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    game = widget.game;

    getStack();

//    AssetImage("assets/option_answer.webp");

    globals.practice = false;
    globals.last = false;

    Sendlog();

    globals.gameBackGround = "";
    globals.questionNumber = 1;
    globals.bankItQuestion = -1;

    WidgetsBinding.instance.addObserver(this);
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);

    if (object.containsKey("msg")) {
      if (object['msg'].toString().contains("Stack list found")) {
        if (object['data'] != null) {
          var data = object['data'];
          if (data.length > 0) {
            var data = object['data'][0];
            _gameFeature = GameFeatures.fromJson(data);
            print(data);
            globals.gameFeatures = _gameFeature;
            /*globals.extraLife = _gameFeature.game.heartCount;
            globals.doubleX = _gameFeature.game.is2x;*/
            globals.bankItQuestion = _gameFeature.bankOn;
            globals.animtype = _gameFeature.game.animationType;
            globals.doubleX = false;
            globals.extraLife = -2;

            setState(() {
              _isLoading = false;
            });
            cacheRewardImage();
            var practiceList = object['qstack'] ?? [];
            globals.description = data['isDescription'] ?? true;
            if (data['Game'].containsKey('countdownTimer'))
              globals.countdown = data['Game']['countdownTimer'] ?? 30;
            else
              globals.countdown = 30;
            savePracticeQuestions(practiceList);
          } else {
            globals.showToast(
                "Game is not playable at the moment\n Please try again after sometime");
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => GameList(),
            ));
          }
        }
      } else if (object['msg'].toString().contains("gamelog")) {
      } else {
        globals.showToast(
            "Game not Found or taking long time to load\n Please try again");
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => GameList(),
        ));
      }
    } else {
      globals.showToast("Something went wrong\nPlease come again in sometime");
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => GameList(),
      ));
    }
  }

  cacheRewardImage() async {
    for (String url in _gameFeature.game.rewardImages) {
      CachedNetworkImage(imageUrl: url);
    }
  }

  savePracticeQuestions(practiceList) async {
    DBHelper helper = DBHelper();
    await helper.drop();
    await helper.create();
    for (var question in practiceList) {
      Question practiceQuestion = Question.fromMap(question);
      await helper.savePractice(practiceQuestion);
    }
    setState(() {
      _loadPractice = true;
    });
  }

  void getStack() async {
    _authToken = await UserData().getAuthToken();
    User _user = await UserData().getUser();
    Map<String, dynamic> action = {
      "action": "stacksList",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
        "condition": {"id": game.stackId}
      }
    };
    print(jsonEncode(action));
    webSocket.sink.add(jsonEncode(action).toString());
    globals.authToken = _authToken;
    globals.userId = _user.id;
    globals.userName = _user.username;
    globals.profilePicture = _user.profilePicture ?? "";
  }

  playGame() {
    String gamedate = "Comming Soon";
    String gametime = "Comming Soon";
    bool game = false;

    game = true;

    if (_gameFeature.game.nextGame != null &&
        _gameFeature.game.nextGame.isNotEmpty) {
      var date = DateTime.parse(_gameFeature.game.nextGame);
      var time = DateTime.parse(_gameFeature.game.nextGame);

      DateFormat format = DateFormat('EEEE, \n d MMMM yyyy');
      DateFormat timeformat = DateFormat('hh:mm aaa');

      gamedate = "${format.format(date)}";
      gametime = "${timeformat.format(date)}";
    }

    if (_gameFeature.season.backgroundUrl != null &&
        _gameFeature.season.backgroundUrl.isNotEmpty) {
      CachedNetworkImage(
        imageUrl: _gameFeature.season.backgroundUrl,
      );
    }

    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(right: 5),
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.07,
            decoration: BoxDecoration(color: HexColor("#1E90FF")),
            child: Text(
              gamedate,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(2.0),
            width: MediaQuery.of(context).size.width * 0.25,
            height: MediaQuery.of(context).size.height * 0.04,
            decoration: BoxDecoration(color: Colors.deepPurple),
            child: Text(
              gametime,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.257,
                height: MediaQuery.of(context).size.height * 0.06,
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.deepPurple,
                        style: BorderStyle.solid,
                        width: 1.0,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            "REMIND ME",
                            style: TextStyle(
                              color: Colors.deepPurple,
                              fontFamily: 'Montserrat',
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 3,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.257,
                height: MediaQuery.of(context).size.height * 0.06,
                child: GestureDetector(
                  onTap: () {
                    _playSound();
                    if (game) {
                      if (_language.isNotEmpty) {
                        globals.language = _language.toLowerCase();
                        /*Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) =>
                                CountDownScreen(feature: _gameFeature)));*/
                      } else
                        globals.showToast("Please Select One Language");
                    } else
                      globals.showToast("This Game will Come Soon");
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.red,
                        style: BorderStyle.solid,
                        width: 1.0,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: AutoSizeText(
                            "PLAY NOW",
                            style: TextStyle(
                              color: Colors.red,
                              fontFamily: 'Montserrat',
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  languages() {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    bool isHindi = _gameFeature.isHindi;
    if (!isHindi) {
      _language = "english";
    }
    return StatefulBuilder(
      builder: (context, childLanguageState) {
        var englishButton = FlatButton(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
          color: _language.isNotEmpty && _language.toLowerCase() == "english"
              ? Colors.green
              : Colors.amberAccent,
          textColor:
              _language.isNotEmpty && _language.toLowerCase() == "english"
                  ? Colors.white
                  : HexColor("#683594"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)),
          ),
          onPressed: () {
            _playSound();
            childLanguageState(() {
              _language = "english";
            });
          },
          child: AutoSizeText(
            "English",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w700,
              fontFamily: "Times New Roman",
            ),
          ),
        );
        var hindiButton = FlatButton(
          padding: EdgeInsets.only(left: 35, right: 35, top: 5),
          color: _language.isNotEmpty && _language.toLowerCase() == "hindi"
              ? Colors.green
              : isHindi
                  ? Colors.amberAccent
                  : Colors.grey,
          textColor: _language.isNotEmpty && _language.toLowerCase() == "hindi"
              ? Colors.white
              : HexColor("#683594"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)),
          ),
          onPressed: () {
            _playSound();
            if (isHindi) {
              childLanguageState(() {
                _language = "hindi";
              });
            } else {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: AutoSizeText("Unavailable"),
                        content:
                            AutoSizeText("This Quiz is not available in Hindi. "
                                "Please choose a different language."),
                        actions: [
                          FlatButton(
                              onPressed: () => Navigator.pop(context, false),
                              child: Text("OK")),
                        ],
                      ));
            }
          },
          child: AutoSizeText(
            "हिंदी",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w700,
              fontFamily: "Times New Roman",
            ),
          ),
        );
        return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            englishButton,
            SizedBox(width: 10),
            hindiButton,
          ],
        );
      },
    );
  }

  gameDetails() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        AutoSizeText(
          _gameFeature.game.name.toUpperCase(),
          style: TextStyle(
              fontWeight: FontWeight.w700,
              color: Colors.white,
              fontSize: 45,
              fontFamily: "Times New Roman"),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
        languages(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
        Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/rules_bg.png"),
                fit: BoxFit.fill,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 15.0),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: HexColor("#fff653"),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset("assets/nextgame.png",
                                width: MediaQuery.of(context).size.width * 0.33,
                                height:
                                    MediaQuery.of(context).size.height * 0.043),
                            Image.asset("assets/clock_img.jpg",
                                width: MediaQuery.of(context).size.width * 0.30,
                                height:
                                    MediaQuery.of(context).size.height * 0.15),
                            SizedBox(height: 5),
                          ],
                        ),
                        SizedBox(height: 5, width: 3),
                        playGame(),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.fromLTRB(5, 5, 5, 15),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/practice_img.jpg"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        SizedBox(width: 5),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(height: 10),
                            Row(
                              children: [
                                AutoSizeText(
                                  "PRACTICE",
                                  style: TextStyle(
                                    color: HexColor("#66058F"),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Times New Roman",
                                  ),
                                ),
                                SizedBox(width: 5),
                                AutoSizeText(
                                  "Game",
                                  style: TextStyle(
                                    color: HexColor("#6A5ACD"),
                                    fontSize: 24,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Times New Roman",
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                FlatButton(
                                  onPressed: _loadPractice
                                      ? () {
                                          _playSound();
                                          globals.practice = true;
                                          globals.extraLife = 0;
                                          Navigator.of(context).pushReplacement(
                                              MaterialPageRoute(
                                            builder: (context) =>
                                                RequestQuestion(),
                                          ));
                                        }
                                      : null,
                                  disabledColor: Colors.grey.shade400,
                                  disabledTextColor: Colors.black,
                                  color: HexColor("#1E90FF"),
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50)),
                                  child: AutoSizeText(
                                    "PLAY NOW",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            )),
        SizedBox(height: 15),
        FlatButton(
          padding: EdgeInsets.only(left: 30, right: 30, top: 5, bottom: 5),
          color: Colors.amberAccent,
          textColor: HexColor("#683594"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)),
          ),
          onPressed: () {
            _playSound();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) =>
                    GameRulesScreen(game: widget.game, feature: _gameFeature)));
          },
          child: AutoSizeText(
            "RULES",
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.w700,
              fontFamily: "Times New Roman",
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/navigation_back.webp"),
          fit: BoxFit.cover,
        )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListView(
              shrinkWrap: true,
              children: <Widget>[
                NavItem(
                  image: Icon(Icons.home),
                  text: Text(
                    "Home",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                          builder: (context) => GameList(),
                        ),
                            (route) => false);
                  },
                ),
                NavItem(
                  image: Image.asset(
                    "assets/icons/pin.png",
                    height: 25,
                    width: 25,
                    color: navTextColor,
                  ),
                  text: Text(
                    "Notice Board",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {},
                ),
                NavItem(
                  icon: Icons.account_circle,
                  text: Text(
                    "Profile",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {
                    globals.Screen_name = "GameList";
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => ProfileScreen(),
                    ));
                  },
                ),
                NavItem(
                  image: Image.asset(
                    "assets/icons/check_list.png",
                    color: navTextColor,
                    height: 25,
                    width: 25,
                  ),
                  text: Text(
                    "Rules and Format",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {
                    globals.Screen_name = "GameList";
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => SettingsScreen()));
                  },
                ),
                NavItem(
                  icon: Icons.settings,
                  text: Text(
                    "Settings",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: navTextColor,
                    ),
                  ),
                  onTap: () {
                    globals.Screen_name = "GameList";
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => SettingsScreen()));
                  },
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: ExpansionTile(
                    tilePadding: EdgeInsets.zero,
                    title: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Color(0xFF2F3F7e),
                            width: 0.8,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      padding: EdgeInsets.only(bottom: 5),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.check,
                            color: Color(0xFF2F3F7e),
                          ),
                          SizedBox(width: 15),
                          Text(
                            "Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    expandedAlignment: Alignment.centerLeft,
                    childrenPadding: EdgeInsets.only(left: 40),
                    children: <Widget>[
                      NavItem(
                        text: Text(
                          "Your Scorecard",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: navTextColor,
                          ),
                        ),
                      ),
                      NavItem(
                        text: Text(
                          "Your School’s Scorecard",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: navTextColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                NavItem(
                  icon: Icons.info,
                  text: Text(
                    "About",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {
                    globals.Screen_name = "GameList";
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ReportAbuseScreen()));
                  },
                ),
                NavItem(
                  icon: Icons.share,
                  text: Text(
                    "Share App",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {},
                ),
                NavItem(
                  icon: Icons.power_settings_new,
                  text: Text(
                    "Logout",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: navTextColor),
                  ),
                  onTap: () {
                    UserData().logout(context);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
    globals.questionNumber = 1;

    return WillPopScope(
        onWillPop: _onBackPressed,
        child: DecoratedBox(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: _isLoading
                  ? AssetImage("assets/bg_bubble.webp")
                  : _gameFeature.season.backgroundUrl != null
                      ? NetworkImage(_gameFeature.season.backgroundUrl)
                      : AssetImage("assets/bg_bubble.webp"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: true,
            body: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Stack(
                    children: <Widget>[
                      AppBar(
                        backgroundColor: Color(0x00000000),
                        elevation: 0,
                        leading: IconButton(
                          icon: Image.asset("assets/icons/back_arrow.webp"),
                          onPressed: () {
                            _onBackPressed();
                          },
                          tooltip: "Back",
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: _isLoading ? globals.lottie : gameDetails(),
                )
              ],
            ),
            drawer: drawer,
          ),
        ));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  //region click sound
  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
  }

  void Sendlog() async {
    User _user = await UserData().getUser();
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
      "GameId": globals.gameId,
      "UserId": _user.id,
    };

    Map<String, dynamic> action = {
      "action": "createGameLog",
      "data": data,
    };

    webSocket.sink.add(jsonEncode(action).toString());
  }
}
