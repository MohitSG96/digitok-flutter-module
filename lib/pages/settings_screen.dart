import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/pages/TutorialScreen.dart';
import 'package:digitok/pages/about_us_screen.dart';
import 'package:digitok/pages/game_screen.dart';
import 'package:digitok/pages/privacy_policy_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:screen/screen.dart';

import 'games_list.dart';

class SettingsScreen extends StatefulWidget {
  final Game game;

  const SettingsScreen({Key key, this.game}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SettingsState();
}

class SettingsState extends State<SettingsScreen> with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  bool AppSoundstatus = false;
  bool GameMusicstatus = false;
  bool Notificationstatus = false;
  bool Chatstatus = false;
  UserData gameData = new UserData();

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();

    WidgetsBinding.instance.addObserver(this);
  }

  saveSettings() async {
    await gameData.saveGameSetting(
        AppSoundstatus, GameMusicstatus, Notificationstatus);
    stopAudio();
    setState(() {});
  }

  getSettings(data) {
    List<bool> settings = data;
    AppSoundstatus = settings[0];
    if(AppSoundstatus){
      PlayAudio();
    }
    GameMusicstatus = settings[1];
    Notificationstatus = settings[2];
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
        width: width,
        decoration: BoxDecoration(
          color: HexColor("#cbcbcb"),
        ),
        child: Padding(
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(3.0),
                child: Row(
                  children: [
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "assets/icons/soundsettings.png",
                      width: 40,
                      height: 40,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: AutoSizeText("App Sound",
                          style: TextStyle(
                              fontSize: 18.0, color: HexColor("#3e1b5f"))),
                    ),
                    FlutterSwitch(
                      width: 70.0,
                      height: 35.0,
                      toggleSize: 30.0,
                      value: AppSoundstatus,
                      borderRadius: 30.0,
                      padding: 8.0,
                      onToggle: (val) {
                        AppSoundstatus = val;
                        saveSettings();
                      },
                    )
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),
              Padding(
                  padding: EdgeInsets.all(3.0),
                  child: Row(
                    children: [
                      SizedBox(width: 5),
                      Image.asset(
                        "assets/icons/music_setting.png",
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: AutoSizeText("Game Music",
                            style: TextStyle(
                                fontSize: 18.0, color: HexColor("#3e1b5f"))),
                      ),
                      FlutterSwitch(
                        width: 70.0,
                        height: 35.0,
                        toggleSize: 30.0,
                        value: GameMusicstatus,
                        borderRadius: 30.0,
                        padding: 8.0,
                        onToggle: (val) {
                          GameMusicstatus = val;
                          saveSettings();
                        },
                      )
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),
              Padding(
                  padding: EdgeInsets.all(3.0),
                  child: Row(
                    children: [
                      SizedBox(width: 5),
                      Image.asset(
                        "assets/icons/notification_setting.png",
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: AutoSizeText("Push Notification",
                            style: TextStyle(
                                fontSize: 18.0, color: HexColor("#3e1b5f"))),
                      ),
                      FlutterSwitch(
                        width: 70.0,
                        height: 35.0,
                        toggleSize: 30.0,
                        value: Notificationstatus,
                        borderRadius: 30.0,
                        padding: 8.0,
                        onToggle: (val) {
                          Notificationstatus = val;
                          saveSettings();
                        },
                      )
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),
              /*********---------- Hiding Chat Enabled -----------*********/
              /*Padding(
                padding: EdgeInsets.all(3.0),
                child: Row(
                  children: [
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "assets/icons/chat_settings.webp",
                      width: 40,
                      height: 40,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: AutoSizeText("Enable Chat",
                          style: TextStyle(
                              fontSize: 18.0, color: HexColor("#3e1b5f"))),
                    ),
                    FlutterSwitch(
                      width: 70.0,
                      height: 35.0,
                      toggleSize: 30.0,
                      value: Chatstatus,
                      borderRadius: 30.0,
                      padding: 8.0,
                      onToggle: (val) {
                        setState(() {
                          Chatstatus = val;
                        });
                      },
                    )
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),*/
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => TutorialScreen()));
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "assets/icons/tutorial_setting.png",
                      width: 40,
                      height: 40,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: AutoSizeText("Tutorial",
                          style: TextStyle(
                              fontSize: 18.0, color: HexColor("#3e1b5f"))),
                    )
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),
              SizedBox(
                height: 3.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => PrivacyPolicyScreen()));
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "assets/icons/privacy_setting.png",
                      width: 40,
                      height: 40,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: AutoSizeText("Privacy",
                          style: TextStyle(
                              fontSize: 18.0, color: HexColor("#3e1b5f"))),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 3.0,
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                      height: 1.0, width: width, color: Colors.white)),
              SizedBox(
                height: 3.0,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => AboutUs_Screen()));
                  },
                  child: Row(
                    children: [
                      SizedBox(
                        width: 5.0,
                      ),
                      Image.asset(
                        "assets/icons/aboutus_setting.png",
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: AutoSizeText("About Us",
                            style: TextStyle(
                                fontSize: 18.0, color: HexColor("#3e1b5f"))),
                      )
                    ],
                  )),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            centerTitle: true,
            leading: Padding(
              padding: EdgeInsets.only(top: 10.0, left: 10.0),
              child: GestureDetector(
                onTap: () {
                  if (globals.Screen_name == "GameList") {
                    return Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => GameList()));
                  } else if (globals.Screen_name == "GameScreen") {
                    return Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(
                            builder: (context) => GameScreen(
                                  game: widget.game,
                                )));
                  }
                },
                child: Image.asset(
                  "assets/icons/back_arrow.webp",
                  fit: BoxFit.cover, // this is the solution for border
                  width: 70.0,
                  height: 70.0,
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
          body: Center(
              child: Padding(
            padding: EdgeInsets.only(
                top: 10.0, left: 20.0, right: 20.0, bottom: 10.0),
            child: ListView(
              children: [
                Image.asset(
                  "assets/round_logo.gif",
                  width: 100,
                  height: 100,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AutoSizeText("Settings",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    SizedBox(width: 5),
                    Image.asset(
                      "assets/icons/setting_icon.png",
                      width: 40,
                      height: 40,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                FutureBuilder(
                  future: gameData.getGameSetting(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.none ||
                        snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: globals.lottie,
                      );
                    } else if (snapshot.connectionState ==
                        ConnectionState.done) {
                      if (snapshot.hasData) {
                        return getSettings(snapshot.data);
                      } else {
                        return Center(
                          child: Text("Something went wrong!!!"),
                        );
                      }
                    } else {
                      return Center();
                    }
                  },
                ),
              ],
            ),
          )),
        ));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    if (globals.Screen_name == "GameList") {
      return Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
    } else if (globals.Screen_name == "GameScreen") {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => GameScreen(
                game: widget.game,
              )));
    }
  }
}
