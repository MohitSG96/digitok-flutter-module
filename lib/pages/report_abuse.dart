import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'game_screen.dart';

class ReportAbuseScreen extends StatefulWidget {
  final Game game;

  const ReportAbuseScreen({Key key, this.game}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ReportAbuseState();
}

class ReportAbuseState extends State<ReportAbuseScreen>
    with WidgetsBindingObserver {
  var webSocket;
  String _authToken;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);

    WidgetsBinding.instance.addObserver(this);
  }

  void SocketSendMessge(String text) async {
    _authToken = await UserData().getAuthToken();
    Map<String, dynamic> data = {
      "message": text,
      "status": "pending",
      "authToken": _authToken,
      "stage": globals.STAGE,
    };
    Map<String, dynamic> action = {
      "data": data,
      "action": "support",
    };
    webSocket.sink.add(jsonEncode(action).toString());
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("message")) {
      globals.showToast("Your Feedback is Successfully Submitted !");
    }

    globals.showToast(object.toString());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    bool _validate = false;
    final _text = TextEditingController();

    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              centerTitle: true,
              leading: Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0),
                child: GestureDetector(
                  onTap: () {
                    if (globals.Screen_name == "GameList") {
                      return Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => GameList()));
                    } else if (globals.Screen_name == "GameScreen") {
                      return Navigator.of(context)
                          .pushReplacement(MaterialPageRoute(
                              builder: (context) => GameScreen(
                                    game: widget.game,
                                  )));
                    }
                  },
                  child: Image.asset(
                    "assets/icons/back_arrow.webp",
                    fit: BoxFit.cover, // this is the solution for border
                    width: 70.0,
                    height: 70.0,
                  ),
                ),
              ),
              backgroundColor: Colors.transparent,
              title: AutoSizeText(
                "Report Abuse",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white,
                          primaryColorDark: Colors.white,
                        ),
                        child: TextField(
                          maxLines: 7,
                          controller: _text,
                          style: new TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              errorText:
                                  _validate ? 'Value Can\'t Be Empty' : null,
                              hintText: 'Write your Message here...',
                              hintStyle: new TextStyle(
                                  fontSize: 16.0, color: Colors.white70),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                borderSide:
                                    BorderSide(width: 2, color: Colors.white),
                              ),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 10.0))),
                          onChanged: (String str) {
                            print(str);
                          },
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        height: 50.0,
                        width: 200.0,
                        child: GestureDetector(
                          onTap: () {
                            FocusScope.of(context).unfocus();
                            setState(() {
                              if (_text.text.isEmpty) {
                                globals
                                    .showToast("Message body can't be Empty");
                              } else {
                                globals.showToast("Please Wait...");
                                SocketSendMessge(_text.text);
                              }
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Color(0xFFF05A22),
                                style: BorderStyle.solid,
                                width: 3.0,
                              ),
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: AutoSizeText(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Montserrat',
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ))));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if(Platform.isIOS){
      advancedPlayer.monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  Future<bool> _onBackPressed() {
    if (globals.Screen_name == "GameList") {
      return Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
    } else if (globals.Screen_name == "GameScreen") {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => GameScreen(
                game: widget.game,
              )));
    }
  }
}
