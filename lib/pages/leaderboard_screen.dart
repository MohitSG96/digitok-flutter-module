import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/leaderboard_list_screen.dart';
import 'package:digitok/pages/login_signup/login.dart';
import 'package:digitok/ui_widgets/digi_button.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class LeaderBoardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LeaderBoardState();
}

class _LeaderBoardState extends State<LeaderBoardScreen>
    with WidgetsBindingObserver {
  bool _isLoading = true;
  int userRank = -1;
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  var webSocket1;
  var _authToken;
  int totalUsers = 0;

  List<dynamic> leaderBoardList;

  @override
  void initState() {
    super.initState();
    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);

    CheckUserOnline();

    getWinnerList();

    WidgetsBinding.instance.addObserver(this);
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        print(object['data']);
        if (object['data'].length > 0) {
          var data = object['data'][0];
          totalUsers = data['totalUsers'];
        }
        globals.totalUsersOnline = totalUsers;
      }
    }
  }

  output(message) async {
    Map<String, dynamic> obj = jsonDecode(message);
    if (obj.containsKey("msg")) {
      if (obj['msg']
          .toString()
          .contains("Usergamesummarylist successfully found")) {
        if (obj.containsKey('data')) {
          leaderBoardList = obj['data'];
          leaderBoardList.sort((a, b) => a['rank'].compareTo(b['rank']));
          userRank = leaderBoardList
              .indexWhere((element) => element['username'] == globals.userName);
          print("User Rank is at index :$userRank");
          setState(() {
            _isLoading = false;
          });
        } else {
          globals.showToast("No Users Found");
        }
      } else if (obj['msg'].toString().contains("User not authenticated")) {
        globals.showToast("User Session Expired. Please Login Again");
        Future.delayed(Duration(seconds: 2), () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
              (route) => false);
        });
      } else {
        globals.showToast("Something Went wrong.\nError: ${obj['msg']}");
      }
    } else {
      globals.showToast("Something Went Wrong");
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(5, 25, 5, 0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(
                                "assets/followers_icon.webp",
                                width: 60,
                              ),
                              AutoSizeText(
                                NumberFormat.compact()
                                    .format(globals.totalUsersOnline),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Image.asset(
                            "assets/WinnerScreen/winners_img.webp",
                            width: 100,
                            height: 100,
                          ),
                          GestureDetector(
                            child: Image.asset(
                              "assets/round_logo.gif",
                              width: 80,
                              height: 80,
                            ),
                            onTap: () {
                              _onBackPressed();
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  _isLoading
                      ? Center(child: globals.lottie)
                      : Column(
                          children: [
                            Stack(
                              alignment: Alignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          child: FadeInImage(
                                            width: 60,
                                            height: 60,
                                            placeholder: AssetImage(
                                                "assets/icons/avtar.webp"),
                                            image: NetworkImage(leaderBoardList[
                                                        1]['profilePicture'] !=
                                                    "no image"
                                                ? leaderBoardList[1]
                                                    ['profilePicture']
                                                : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        AutoSizeText(
                                          "${leaderBoardList[1]['username']}",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        DigiButton(
                                          width: 84.0,
                                          height: 124.0,
                                          child: Container(
                                            alignment: Alignment.topCenter,
                                            child: AutoSizeText(
                                              "${leaderBoardList[1]['points']}",
                                              textAlign: TextAlign.center,
                                              minFontSize: 16,
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          image: AssetImage(
                                              "assets/WinnerScreen/podium_2.webp"),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          child: FadeInImage(
                                            width: 60,
                                            height: 60,
                                            placeholder: AssetImage(
                                                "assets/icons/avtar.webp"),
                                            image: NetworkImage(leaderBoardList[
                                                        0]['profilePicture'] !=
                                                    "no image"
                                                ? leaderBoardList[0]
                                                    ['profilePicture']
                                                : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                          ),
                                        ),
                                        SizedBox(
                                          child: AutoSizeText(
                                            "${leaderBoardList[0]['username']}",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        DigiButton(
                                          width: 85.0,
                                          height: 150.0,
                                          child: Container(
                                            alignment: Alignment.topCenter,
                                            child: AutoSizeText(
                                              "${leaderBoardList[0]['points']}",
                                              textAlign: TextAlign.center,
                                              minFontSize: 16,
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          image: AssetImage(
                                              "assets/WinnerScreen/podium_1.webp"),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          child: FadeInImage(
                                            width: 60,
                                            height: 60,
                                            placeholder: AssetImage(
                                                "assets/icons/avtar.webp"),
                                            image: NetworkImage(leaderBoardList[
                                                        2]['profilePicture'] !=
                                                    "no image"
                                                ? leaderBoardList[2]
                                                    ['profilePicture']
                                                : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        AutoSizeText(
                                          "${leaderBoardList[2]['username']}",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        DigiButton(
                                          width: 86.0,
                                          height: 97.0,
                                          child: Container(
                                            alignment: Alignment.topCenter,
                                            child: AutoSizeText(
                                              "${leaderBoardList[2]['points']}",
                                              textAlign: TextAlign.center,
                                              minFontSize: 16,
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          image: AssetImage(
                                              "assets/WinnerScreen/podium_3.webp"),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Container(
                                  height: 150,
                                  width: MediaQuery.of(context).size.width - 50,
                                  child: Lottie.asset(
                                    "assets/loader/fireworks.json",
                                    alignment: Alignment.center,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Column(
                              children: [
                                Row(
                                  children: [
                                    DigiButton(
                                      width: 40.0,
                                      height: 50.0,
                                      child: AutoSizeText(
                                        "${leaderBoardList[3]['rank']}",
                                        textAlign: TextAlign.center,
                                        minFontSize: 16,
                                        style: TextStyle(
                                          fontSize: 28,
                                          color: HexColor("#561225"),
                                        ),
                                      ),
                                      image: AssetImage(
                                          "assets/number_background.webp"),
                                    ),
                                    SizedBox(width: 5),
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(30.0),
                                      child: FadeInImage(
                                        width: 50,
                                        height: 50,
                                        placeholder: AssetImage(
                                            "assets/icons/avtar.webp"),
                                        image: NetworkImage(leaderBoardList[3]
                                                    ['profilePicture'] !=
                                                "no image"
                                            ? leaderBoardList[3]
                                                ['profilePicture']
                                            : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          "${leaderBoardList[3]['username']}",
                                          style: TextStyle(
                                            color: HexColor("#2196F3"),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                        ),
                                        AutoSizeText(
                                          "${leaderBoardList[3]['points']}",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: [
                                    DigiButton(
                                      width: 40.0,
                                      height: 50.0,
                                      child: AutoSizeText(
                                        "${leaderBoardList[4]['rank']}",
                                        textAlign: TextAlign.center,
                                        minFontSize: 16,
                                        style: TextStyle(
                                          fontSize: 28,
                                          color: HexColor("#561225"),
                                        ),
                                      ),
                                      image: AssetImage(
                                          "assets/number_background.webp"),
                                    ),
                                    SizedBox(width: 5),
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(30.0),
                                      child: FadeInImage(
                                        width: 50,
                                        height: 50,
                                        placeholder: AssetImage(
                                            "assets/icons/avtar.webp"),
                                        image: NetworkImage(leaderBoardList[4]
                                                    ['profilePicture'] !=
                                                "no image"
                                            ? leaderBoardList[4]
                                                ['profilePicture']
                                            : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          "${leaderBoardList[4]['username']}",
                                          style: TextStyle(
                                            color: HexColor("#2196F3"),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                        ),
                                        AutoSizeText(
                                          "${leaderBoardList[4]['points']}",
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: [
                                    DigiButton(
                                      width: 40.0,
                                      height: 50.0,
                                      child: AutoSizeText(
                                        "${leaderBoardList[5]['rank']}",
                                        textAlign: TextAlign.center,
                                        minFontSize: 16,
                                        style: TextStyle(
                                          fontSize: 28,
                                          color: HexColor("#561225"),
                                        ),
                                      ),
                                      image: AssetImage(
                                          "assets/number_background.webp"),
                                    ),
                                    SizedBox(width: 5),
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(30.0),
                                      child: FadeInImage(
                                        width: 50,
                                        height: 50,
                                        placeholder: AssetImage(
                                            "assets/icons/avtar.webp"),
                                        image: NetworkImage(leaderBoardList[5]
                                                    ['profilePicture'] !=
                                                "no image"
                                            ? leaderBoardList[5]
                                                ['profilePicture']
                                            : "https://scontent.fslv1-2.fna.fbcdn.net/v/t1.0-9/52029280_2199773943684300_796946403136897024_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=tSsF5s0B9uUAX-5FIXF&_nc_ht=scontent.fslv1-2.fna&oh=feeb57e18a92fddfdf4a8f0f82ec0dbb&oe=5F636216"),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          "${leaderBoardList[5]['username']}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: HexColor("#2196F3"),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                        ),
                                        AutoSizeText(
                                          "${leaderBoardList[5]['points']}",
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
//                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: 70,
                child: AppBar(
                  backgroundColor: Colors.transparent,
                  leading: Material(
                    child: IconButton(
                      iconSize: 20,
                      icon: Image.asset("assets/icons/back_arrow.webp"),
                      onPressed: () {
                        _onBackPressed();
                      },
                      tooltip: "Back",
                    ),
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          bottomNavigationBar: _isLoading
              ? null
              : GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => LeaderBoardUsers(
                        index: userRank,
                        userList: leaderBoardList,
                      ),
                    ));
                  }, // handle your image tap here
                  child: Image.asset(
                    'assets/view_leaderboard.webp',
                    fit: BoxFit.cover, // this is the solution for border
                    width: width,
                    height: 50.0,
                  ),
                ),
        ),
        onWillPop: _onBackPressed);
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.play('game_over_and_leaderboard.wav');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

//endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }

  getWinnerList() async {
    var webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    Map<String, dynamic> action = {
      "action": "usergamesummariesList",
      "data": {
        "stage": globals.STAGE,
        "authToken": globals.authToken,
        "condition": {"GameId": globals.gameId},
      }
    };
    print(jsonEncode(action).toString());
    webSocket.sink.add(jsonEncode(action).toString());
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GameList(),
      ),
    );
  }
}
