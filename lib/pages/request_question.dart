import 'package:digitok/models/question.dart';
import 'package:digitok/pages/question_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';

class RequestQuestion extends StatefulWidget {
  final Question question;

  const RequestQuestion({Key key, this.question}) : super(key: key);

  @override
  _RequestQuestionState createState() => _RequestQuestionState();
}

class _RequestQuestionState extends State<RequestQuestion> {
  var imageArray = new List(21);
  var question;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _SoundStreamId;

  @override
  void initState() {
    super.initState();
    _soundpool = Soundpool();
    _soundId = _loadSound();

    Screen.keepOn(true);

    //region images
    imageArray[1] = "assets/RequestQuestionImages/Q1.webp";
    imageArray[2] = "assets/RequestQuestionImages/Q2.webp";
    imageArray[3] = "assets/RequestQuestionImages/Q3.webp";
    imageArray[4] = "assets/RequestQuestionImages/Q4.webp";
    imageArray[5] = "assets/RequestQuestionImages/Q5.webp";
    imageArray[6] = "assets/RequestQuestionImages/Q6.webp";
    imageArray[7] = "assets/RequestQuestionImages/Q7.webp";
    imageArray[8] = "assets/RequestQuestionImages/Q8.webp";
    imageArray[9] = "assets/RequestQuestionImages/Q9.webp";
    imageArray[10] = "assets/RequestQuestionImages/Q10.webp";
    imageArray[11] = "assets/RequestQuestionImages/Q11.webp";
    imageArray[12] = "assets/RequestQuestionImages/Q12.webp";
    imageArray[13] = "assets/RequestQuestionImages/Q13.webp";
    imageArray[14] = "assets/RequestQuestionImages/Q14.webp";
    imageArray[15] = "assets/RequestQuestionImages/Q15.webp";
    imageArray[16] = "assets/RequestQuestionImages/Q16.webp";
    imageArray[17] = "assets/RequestQuestionImages/Q17.webp";
    imageArray[18] = "assets/RequestQuestionImages/Q18.webp";
    imageArray[19] = "assets/RequestQuestionImages/Q19.webp";
    imageArray[20] = "assets/RequestQuestionImages/Q20.webp";
    //endregion
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height * 0.75;
    _playSound();
    Future.delayed(Duration(seconds: 2), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => QuestionScreen(),
      ));
    });
    return WillPopScope(
        onWillPop: () => Future.value(false),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: true,
            body: Center(
              child: Container(
                height: height,
                width: width,
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(color: Colors.transparent),
                child: Image.asset(
                  imageArray[globals.questionNumber],
                  fit: BoxFit.fitWidth,
                ),
              ),
            )));
  }

  //region sound
  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/question_seprator_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _SoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_SoundStreamId != null) {
      await _soundpool.stop(_SoundStreamId);
      _soundpool.release();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    _stopSound();
  }
}
