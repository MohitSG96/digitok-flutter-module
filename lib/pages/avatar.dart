import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:digitok/models/avatar_model.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class AvatarScreen extends StatefulWidget {
  const AvatarScreen({Key key}) : super(key: key);

  @override
  _AvatarScreenState createState() => _AvatarScreenState();
}

class _AvatarScreenState extends State<AvatarScreen>
    with WidgetsBindingObserver {
  bool _isLoading = true;
  var webSocket;
  String _authToken, imageUrl;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;
  List<Avatar> avatarList = [];

  @override
  void initState() {
    super.initState();

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    getAvatars();

    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();

    Screen.keepOn(true);

    WidgetsBinding.instance.addObserver(this);
  }

  output(message) {
    print(message);
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg'].toString().contains("Avatar List Found")) {
        if (object['data'] != null) {
          var data = object['data'];
          for (var d in data) {
            avatarList.add(Avatar.fromJson(d));
          }
          setState(() {
            _isLoading = false;
          });
        }
      }
    }
  }

  userAvatarList() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            alignment: Alignment.center,
            child: Text(
              "Select One Avatar",
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(7),
              color: Color(0x94CBCBCB),
              alignment: Alignment.center,
              child: GridView.builder(
                itemCount: avatarList.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.3,
                ),
                itemBuilder: (context, index) {
                  Avatar avatar = avatarList[index];
                  bool _isSelected = false;
                  if (avatar.avatarImage == imageUrl) {
                    _isSelected = true;
                  }
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        imageUrl = avatar.avatarImage;
                      });
                    },
                    child: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          alignment: Alignment.center,
                          child: Image.network(
                            avatar.avatarImage,
                            width: 200,
                            height: 200,
                          ),
                        ),
                        _isSelected
                            ? Container(
                                alignment: Alignment(0.9, -0.95),
                                child: Image.asset(
                                  "assets/icons/tick.webp",
                                  width: 30,
                                  height: 30,
                                ),
                              )
                            : Center(),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  selectedAvatar() {}

  void getAvatars() async {
    _authToken = await UserData().getAuthToken();
    if (_authToken == null || _authToken.trim().isEmpty) {
      _authToken = globals.authToken;
    }
    Map<String, dynamic> action = {
      "action": "avatarsList",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
      }
    };
    print(jsonEncode(action));
    webSocket.sink.add(jsonEncode(action).toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: true,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Stack(
              children: <Widget>[
                AppBar(
                  backgroundColor: Color(0x00000000),
                  elevation: 0,
                  leading: IconButton(
                    icon: Image.asset("assets/icons/back_arrow.webp"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    tooltip: "Back",
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: _isLoading ? globals.lottie : userAvatarList(),
          )
        ],
      ),
      bottomNavigationBar: _isLoading
          ? null
          : Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FlatButton(
                    onPressed: () {
                      if (imageUrl != null) {
                        globals.imageURL = imageUrl;
                      }
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Save",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    color: Color(0xFF269105),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: Color(0xFFD00A0A),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    child: Text(
                      "Cancel",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  //region click sound
  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
  }
}
