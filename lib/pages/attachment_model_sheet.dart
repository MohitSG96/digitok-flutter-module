import 'dart:io';

import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:digitok/pages/avatar.dart';
import 'package:digitok/ui_widgets/custom_dialog.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:permission_handler/permission_handler.dart';

class AddAttachmentModalSheet extends StatefulWidget {
  final Size screenSize;

  AddAttachmentModalSheet(this.screenSize);

  @override
  _AddAttachmentModalSheetState createState() =>
      _AddAttachmentModalSheetState();
}

class _AddAttachmentModalSheetState extends State<AddAttachmentModalSheet> {
  _buildHeading(String text) {
    return Text(text, style: TextStyle(fontSize: 26, color: Colors.black));
  }

  _buildCloseButton(context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Icon(Icons.close, color: Colors.grey.shade500),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildHeading('Upload'),
                _buildCloseButton(context)
              ],
            ),
            SizedBox(height: 16),
            /*_buildOption(Icons.camera_alt, 'Camera',
                    () => _onPickFromCameraClicked(context)),*/
            _buildOption(Icons.photo_library, 'Photo library',
                () => _onAddPhotoClicked(context)),
            _buildOption(Icons.person, "Select Avatar", () {
              Navigator.of(context)
                  .push(MaterialPageRoute(
                    builder: (context) => AvatarScreen(),
                  ))
                  .then((value) => Navigator.of(context).pop());
            })
          ],
        ),
      ),
    );
  }

  _buildOption(IconData optionIcon, String optionName, Function onItemClicked) {
    return InkWell(
      onTap: onItemClicked,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 12),
        child: Row(
          children: <Widget>[
            Icon(optionIcon),
            SizedBox(width: 8),
            Text(
              optionName,
              style: TextStyle(color: Colors.black, fontSize: 18),
            )
          ],
        ),
      ),
    );
  }

  _onAddPhotoClicked(context) async {
    Permission permission;

    if (Platform.isIOS) {
      permission = Permission.photos;
    } else {
      permission = Permission.storage;
    }

    PermissionStatus permissionStatus = await permission.status;

    print(permissionStatus);

    if (permissionStatus == PermissionStatus.restricted) {
      _showOpenAppSettingsDialog(context);

      permissionStatus = await permission.status;

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.permanentlyDenied) {
      _showOpenAppSettingsDialog(context);

      permissionStatus = await permission.status;

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.undetermined) {
      permissionStatus = await permission.request();

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.denied) {
      if (Platform.isIOS) {
        _showOpenAppSettingsDialog(context);
      } else {
        permissionStatus = await permission.request();
      }

      if (permissionStatus != PermissionStatus.granted) {
        //Only continue if permission granted
        return;
      }
    }

    if (permissionStatus == PermissionStatus.granted) {
      ifPermissionGrant();
//      print('Permission granted');
    }
  }

  _showOpenAppSettingsDialog(context) {
    return CustomDialog.show(
      context,
      'Permission needed',
      'Photos permission is needed to select photos',
      'Open settings',
      openAppSettings,
    );
  }

/*  _onAddPhotoClicked(context) async {
    GetImagePermission getPermission = GetImagePermission.gallery();
    await getPermission.getPermission(context);

    if (getPermission.granted) {
      widget.source = Source.GALLERY;
      Navigator.pop(context);
    }
  }

  _onPickFromCameraClicked(context) async {
    GetImagePermission getPermission = GetImagePermission.camera();
    await getPermission.getPermission(context);

    if (getPermission.granted) {
      widget.source = Source.CAMERA;
      Navigator.pop(context);
    }
  }*/

  ifPermissionGrant() async {
    print('Permission granted');
    File image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );

    if (image != null) {
      File croppedImage = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
        ],
        androidUiSettings: AndroidUiSettings(
          initAspectRatio: CropAspectRatioPreset.square,
          hideBottomControls: true,
        ),
        iosUiSettings: IOSUiSettings(
          aspectRatioPickerButtonHidden: true,
        ),
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 30,
      );
//      print(croppedImage.path);

      DateTime now = DateTime.now();
      String date = (now.day.toString()) +
          "/" +
          (now.month.toString()) +
          "/" +
          now.year.toString();

      String filename = date +
          "/" +
          (now.hour.toString()) +
          ":" +
          (now.minute.toString()) +
          "/" +
          path.basename(croppedImage.path);
      try {
        final result = await InternetAddress.lookup("google.com");
        String uploadedImageUrl;
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          uploadedImageUrl = await AmazonS3Cognito.upload(
              croppedImage.path,
              globals.BUCKET_ID,
              globals.POOL_ID,
              filename,
              AwsRegion.US_EAST_2,
              AwsRegion.US_EAST_2);
        } else {
          throw ("No Internet Connected");
        }
//        print(uploadedImageUrl);
        if (uploadedImageUrl != null &&
            (uploadedImageUrl.trim().isEmpty ||
                uploadedImageUrl.toLowerCase().contains("failed"))) {
          showDialog(
            context: this.context,
            builder: (diaCxt) {
              return AlertDialog(
                actions: [
                  CupertinoButton(
                      child: Text("OK"),
                      color: Colors.red,
                      onPressed: () {
                        Navigator.of(diaCxt).pop();
                      })
                ],
                title: Text("Image Upload Failed!"),
                content: Text("Image Not uploaded to server."),
              );
            },
          );
        } else {
          globals.imageURL = uploadedImageUrl;
          Navigator.of(context).pop();
        }
      } on Exception catch (e) {
        showDialog(
          context: this.context,
          builder: (diaCxt) {
            return AlertDialog(
              actions: [
                CupertinoButton(
                    child: Text("OK"),
                    color: Colors.red,
                    onPressed: () {
                      Navigator.of(diaCxt).pop();
                    })
              ],
              title: Text("Image Upload Failed!"),
              content: Text("No internet connection"),
            );
          },
        );
      }

      /*GalleryItem(
        id: Uuid().v1(),
        resource: image.path,
        isSvg: fileExtension.toLowerCase() == ".svg",
      );

      setState(() {
        _photo = image;
      });
      GenerateImageUrl generateUrl = GenerateImageUrl();
      await generateUrl.call(fileExtension);*/
    }
  }
}
