import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/games_sub_pages/alpha/daily_quiz.dart';
import 'package:digitok/pages/games_sub_pages/beta/beta.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class GameList extends StatefulWidget {
  @override
  _GameListState createState() => _GameListState();
}

class _GameListState extends State<GameList> with WidgetsBindingObserver {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController _scrollController = ScrollController();
  var _isLoading = true;
  var webSocket;
  List<Game> _gameLists;
  String authToken;
  int _current = 0;
  List<String> bannerUrls = [];

  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);

    WidgetsBinding.instance.addObserver(this);

    fetchGameList();
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object.containsKey("data") && object['data'] != null) {
        var data = object['data'];
        _gameLists = [];
        for (Map<String, dynamic> game in data) {
          _gameLists.add(Game.fromJson(game));
        }
        if (object.containsKey("banners") &&
            object['banners'] != null &&
            object['banners'].containsKey('bannerList')) {
//          var banners = object['banners'];
          for (var banner in object['banners']['bannerList']) {
            bannerUrls.add(banner.toString());
          }
        }
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  game(logoUrl) {
    var width = MediaQuery.of(context).size.width;
    return Align(
      alignment: Alignment.center,
      child: Container(
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(5),
              child: FadeInImage(
                width: width * 0.77,
                placeholder: AssetImage("assets/ic_launcher_square.webp"),
                image: logoUrl != null && logoUrl.toString().isNotEmpty
                    ? NetworkImage(logoUrl)
                    : AssetImage("assets/ic_launcher_square.webp"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getGames() {
//    var height = MediaQuery.of(context).size.height;
    Widget child;
    Widget parent;
    if (!_isLoading) {
      child = ListView.builder(
        itemCount: _gameLists.length,
        padding: EdgeInsets.zero,
//        controller: _scrollController,
        itemBuilder: (context, index) {
          Game _game = _gameLists[index];
//          _game.stackId = 50;
          return GestureDetector(
            onTap: () {
              globals.gameId = _game.id;
              print("Game ID is: ${_game.id}");
              _playSound();
              if (_game.isAppointment ||
                  _game.gameName.toLowerCase().contains("alpha")) {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => DailyQuiz(game: _game),
                  ),
                );
              } else {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => BetaQuiz(game: _game),
                  ),
                );
              }
            },
            child: game(_game.imageSrc),
          );
        },
      );
      parent = child;
    }
    return _isLoading
        ? globals.lottie
        : _gameLists.length > 0
            ? Column(
                children: <Widget>[
//                  SizedBox(height: 15),
                  bannerUrls.length > 0
                      ? Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: CarouselSlider(
                                    items: bannerUrls.map((e) {
                                      return Container(
                                        alignment: Alignment.center,
                                        child: FadeInImage(
                                          imageErrorBuilder:
                                              (context, error, stackTrace) =>
                                                  Container(
                                            margin: EdgeInsets.symmetric(
                                              horizontal: 15,
                                            ),
                                            width: double.maxFinite,
                                            color: Colors.blueAccent,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Icon(Icons.error_outline),
                                                SizedBox(height: 5),
                                                Text("Image Error"),
                                              ],
                                            ),
                                          ),
                                          placeholder: AssetImage(
                                              'assets/iql_logo.webp'),
                                          image: NetworkImage(e),
                                        ),
                                      );
                                    }).toList(),
                                    options: CarouselOptions(
                                      height: 150,
//                                  enlargeCenterPage: true,
                                      aspectRatio: 16 / 9,
                                      autoPlay: true,
                                      onPageChanged: (index, reason) {
                                        setState(() {
                                          _current = index;
                                        });
                                      },
                                    )),
                              ),
                              StatefulBuilder(
                                builder: (context, setState) => Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    for (var i = 0; i < bannerUrls.length; i++)
                                      i
                                  ].map((e) {
                                    int index = e;
                                    return Container(
                                      width: 8.0,
                                      height: 8.0,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 2.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: _current == index
                                            ? Color.fromRGBO(0, 0, 0, 0.9)
                                            : Color.fromRGBO(0, 0, 0, 0.4),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        )
                      : Center(),
                  Expanded(
                    child: parent,
                  ),
                ],
              )
            : Container(
                color: Colors.white,
                padding: EdgeInsets.all(10),
                child: Text(
                  "NO GAMES FOUND!",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              );
  }

  fetchGameList() async {
    authToken = await UserData().getAuthToken();

    var action = {
      "action": "gamesList",
      "data": {
        "authToken": authToken,
        "stage": globals.STAGE,
        "condition": {"iqlEnabled": true}
      }
    };

    webSocket.sink.add(jsonEncode(action).toString());
    print(jsonEncode(action).toString());
    User _user = await UserData().getUser();
    globals.profilePicture = _user.profilePicture ?? "";
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Do you want to exit the App ?"),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.pop(context, false),
                    child: Text("No")),
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context, true);
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                      _stopSound();
                      stopAudio();
                    },
                    child: Text("Yes"))
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/navigation_back.webp"),
          fit: BoxFit.cover,
        )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DrawerHeader(
//              padding: EdgeInsets.zero,
//              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/bg_bubble.webp"),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/iql_logo.webp",
                    width: double.maxFinite,
                    height: 100,
                  ),
                  Text(
                    "IQL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  NavItem(
                    image: Icon(Icons.home),
                    text: Text(
                      "Home",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => GameList(),
                          ),
                              (route) => false);
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/pin.png",
                      height: 25,
                      width: 25,
                      color: navTextColor,
                    ),
                    text: Text(
                      "Notice Board",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.showToast("No Notice at moment");
                      if (_scaffoldKey.currentState.isDrawerOpen)
                        _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  NavItem(
                    icon: Icons.account_circle,
                    text: Text(
                      "Profile",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ));
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/check_list.png",
                      color: navTextColor,
                      height: 25,
                      width: 25,
                    ),
                    text: Text(
                      "Rules and Format",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => GameRulesScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.settings,
                    text: Text(
                      "Settings",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: navTextColor,
                      ),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xFF2F3F7e),
                              width: 0.8,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Color(0xFF2F3F7e),
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Scorecard",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: navTextColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      expandedAlignment: Alignment.centerLeft,
                      childrenPadding: EdgeInsets.only(left: 40),
                      children: <Widget>[
                        NavItem(
                          text: Text(
                            "Your Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals
                                .showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        NavItem(
                          text: Text(
                            "Your School’s Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals
                                .showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    ),
                  ),
                  NavItem(
                    icon: Icons.info,
                    text: Text(
                      "About",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ReportAbuseScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.share,
                    text: Text(
                      "Share App",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {},
                  ),
                  NavItem(
                    icon: Icons.power_settings_new,
                    text: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      UserData().logout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    var height = MediaQuery.of(context).size.height;

    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          body: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Stack(
                  children: <Widget>[
                    AppBar(
                      backgroundColor: Color(0x00000000),
                      elevation: 0,
                    ),
                  ],
                ),
              ),
              Container(
                height: height - MediaQuery.of(context).padding.top - 60,
//                width: 250,
                alignment: Alignment.center,
                child: getGames(),
              )
            ],
          ),
          drawer: drawer,
        ));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  //endregion

  @override
  void dispose() {
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
