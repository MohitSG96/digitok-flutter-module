import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:digitok/models/question.dart';
import 'package:digitok/pages/gameover_screen.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/request_question.dart';
import 'package:digitok/ui_widgets/digi_button.dart';
import 'package:digitok/utils/db_helper.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:screen/screen.dart';
import 'package:soundpool/soundpool.dart';
import 'package:video_player/video_player.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class QuestionScreen extends StatefulWidget {
  @override
  _QuestionScreenState createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  DBHelper helper;
  bool doubleXSelected = false,
      isPracticeGame = false,
      optionTimeSet = false,
      showReplayBtn = false,
      replayedMedia = false,
      replayMedia = false,
      loadOptions = false,
      preMediaLoad = true,
      isSpectator = false,
      gameExited = false,
      showAnswer = false,
      isBouncer = false,
      timeOver = false,
      loadToast = true,
      isReload = false,
      doubleX = false,
      finish = false;
  DateTime timeSelection, endSelection;
  int optionTime = 0, mediaTime = 0, questionTime = 0;
  int questionId;
  double positivePoints, negativePoints;
  double points;
  double progressValue;
  Timer _timer;
  String selection = "", answer = "", selectionB = "", msg = "";
  var toastState;
  AnimationController animation;
  VideoPlayerController _controller;
  var imageCtxParent;

  Soundpool _soundpool;
  Future<int> _soundId, _questionSound, _optionSound, _rightSound, _wrongSound;
  int _ClickSoundStreamId;

  var webSocket1, bouncerSocket;
  var _authToken;
  int totalUsers = 0;

  bool questionPlayed = false, optionPlayed = false;
  AudioPlayer advancedPlayer, questionAudio;
  AudioCache audioCache, questionAudioCache;
  var optionArray;

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);

    _soundpool = Soundpool();

    _soundId = _loadSound();
    _questionSound = _loadQuestionAppearSound();
    _optionSound = _loadOptionSound();
    _rightSound = _loadRightSound();
    _wrongSound = _loadWrongSound();
    initPlayer();

    webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);

    CheckUserOnline();

    helper = DBHelper();
    animation = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    animation.repeat();
    if (globals.extraLife == -1) isSpectator = true;
    doubleX = globals.doubleX;
    globals.getBouncer = true;
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        var data = object['data'][0];
        totalUsers = data['totalUsers'];
      }
    }
  }

  Future<Question> getQuestion() async {
    int questionNumber = globals.questionNumber - 1;
    Question question;
    if (globals.practice) {
      List<Question> questions = await helper.getPractice();
      if (globals.questionNumber == questions.length) {
        globals.last = true;
      }
      question = questions[questionNumber];
      return question;
    } else if (globals.language == "hindi") {
      List<Question> questions = await helper.getHindi();
      if (globals.questionNumber == globals.totalQuestions) {
        globals.last = true;
      }
      question = questions[questionNumber];
      questionId = question.id;
      positivePoints = question.positivePoints;
      negativePoints = question.negativePoints;
      var numOptions = 0;
      if (question.optionA.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionB.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionC.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionD.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (numOptions < 3) {
        doubleX = false;
      }
      if (globals.extraLife > 0 && doubleX) {
        msg = "आपके पास जीवन और 2x विकल्प उपलब्ध है";
      } else if (globals.extraLife > 0) {
        msg = "आपके पास जीवन उपलब्ध है";
      } else if (globals.doubleX) {
        msg = "आपके पास 2x विकल्प उपलब्ध ह";
      }
      return question;
    } else {
      List<Question> questions = await helper.getEnglish();

      if (globals.questionNumber == questions.length) {
        globals.last = true;
        globals.doubleX = false;
        if (globals.extraLife >= 0) {
          globals.extraLife = 0;
        }
      }
      question = questions[questionNumber];
      questionId = question.id;
      positivePoints = question.positivePoints;
      negativePoints = question.negativePoints;
      var numOptions = 0;
      if (question.optionA.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionB.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionC.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (question.optionD.trim().isNotEmpty) {
        numOptions += 1;
      }
      if (numOptions < 3) {
        doubleX = false;
      }
      if (globals.extraLife > 0 && doubleX) {
        msg = "Life and 2x option is Available";
      } else if (globals.extraLife > 0) {
        msg = "Life is Available";
      } else if (globals.doubleX) {
        msg = "2x Option is Available";
      }
      return question;
    }
  }

  question(Question question) {
    answer = question.answer.trim();
    if (!isReload) {
      if (question.preMedia != null && question.preMedia.isNotEmpty) {
        if (globals.question_media['image'].contains(
            question.preMedia.substring(question.preMedia.lastIndexOf(".")))) {
          preMediaLoad = false;
        } else {
          preMediaLoad = true;
        }
      } else if (question.questionMedia != null &&
          question.questionMedia.isNotEmpty) {
        if (globals.question_media['image'].contains(question.questionMedia
            .substring(question.questionMedia.lastIndexOf(".")))) {
          preMediaLoad = false;
        } else {
          preMediaLoad = true;
        }
      } else {
        preMediaLoad = false;
      }
    }
    return Column(
      children: [
        //region Header for Question Page Like DoubleX, Heart and Timer Progress
        globals.practice ? Center() : headerTimer(question),
        //endregion

        question.preMedia != null && question.preMedia.isNotEmpty
            ? preMediaLoad
                ? Center()
                : // region Question Number and Question Text
                questionHeader(question) // Portion Where Question is Displayed
            //endregion
            : // region Question Number and Question Text
            questionHeader(question),
        // Portion Where Question is Displayed
        //endregion

        //region Question or Pre Question Media
        question.preMedia != null && question.preMedia.isNotEmpty
            ? getQuestionMedia(question.preMedia)
            : question.questionMedia != null &&
                    question.questionMedia.isNotEmpty
                ? getQuestionMedia(question.questionMedia)
                : Center(),
        //endregion

        //region Question's Options and state
        preMediaLoad ? Center() : options(question),
        //endregion
      ],
    );
  }

  getQuestionMedia(String preMedia) {
    var _isReload = isReload;
    return StatefulBuilder(
      builder: (context, preMediaState) {
        if (globals.question_media['image']
            .contains(preMedia.substring(preMedia.lastIndexOf(".")))) {
          return GestureDetector(
            child: Container(
              height: 200,
              child: CachedNetworkImage(imageUrl: preMedia),
            ),
            onTap: () {
              imageDialog(preMedia);
            },
          );
        } else if (globals.question_media['audio']
            .contains(preMedia.substring(preMedia.lastIndexOf(".")))) {
          _controller = VideoPlayerController.network(preMedia);

          return FutureBuilder(
            future: playQuestionAudio(preMedia),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                int result = snapshot.data;
                Future.delayed(Duration(milliseconds: 2000), () async {
                  int duration = await questionAudio.getDuration();
                  if (questionTime > 4) {
                    duration = questionTime;
                  }
                  if (!(gameExited || finish)) {
                    Future.delayed(
                        Duration(milliseconds: duration - 2000),
                        (gameExited || finish) || replayMedia
                            ? () {}
                            : () {
                                if (!_isReload) {
                                  isReload = true;
                                  if (!finish) {
                                    setState(() {
                                      loadToast = true;
                                      showReplayBtn = true;
                                      preMediaLoad = false;
                                    });
                                    questionAudio.pause();
                                  }
                                }
                              });
                  }
                });
                if (result == 1) {
                  return Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/audio_file.gif")),
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15),
                    height: MediaQuery.of(context).size.height * 0.20,
                    child: VideoPlayer(_controller),
                  );
                } else {
                  return Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, right: 15),
                    height: MediaQuery.of(context).size.height * 0.22,
                  );
                }
              } else {
                return Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: MediaQuery.of(context).size.height * 0.22,
                );
              }
            },
          );
        } else if (globals.question_media['video']
            .contains(preMedia.substring(preMedia.lastIndexOf(".")))) {
          _controller = VideoPlayerController.network(preMedia);

          return FutureBuilder(
            future: _controller.initialize(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                Duration time;
                if (_controller.value != null &&
                    _controller.value.duration != null) {
                  time = _controller.value.duration;
                } else {
                  time = Duration(seconds: 10);
                }
                if (questionTime > 4) {
                  time = Duration(seconds: questionTime);
                }
                Future.delayed(
                    time,
                    (gameExited || finish) || replayMedia
                        ? () {}
                        : () {
                            if (!_isReload) {
                              isReload = true;
                              if (!finish) {
                                setState(() {
                                  showReplayBtn = true;
                                  preMediaLoad = false;
                                });
                                _controller.pause();
                              }
                            }
                          });

                _controller.play();
                return Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: MediaQuery.of(context).size.height * 0.22,
                  child: AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: VideoPlayer(_controller),
                  ),
                );
              } else {
                return Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: MediaQuery.of(context).size.height * 0.22,
                );
              }
            },
          );
        } else {
          return Center();
        }
      },
    );
  }

  headerTimer(Question question) {
    int time = 0;
    double progressPercentage = 0.0;
    if (question.questionTime >= 0 && question.optionTime > 0) {
      optionTime = question.optionTime;
      questionTime = question.questionTime;
      time = optionTime;
      optionTimeSet = false;
      progressPercentage = (optionTime / time);
      selection = "";
      timeOver = false;
      showAnswer = false;
    }
    return StatefulBuilder(
      builder: (headerContext, headerState) {
        if (!preMediaLoad &&
            !optionTimeSet &&
            loadOptions &&
            question.optionTime > 0) {
          _timer = Timer.periodic(Duration(seconds: 1), (timer) {
            if (timer.tick < time) {
              optionTimeSet = true;
              headerState(() {
                optionTime--;
              });
            } else {
              if (!gameExited) {
                headerState(() {
                  this.timeOver = true;
                  timer.cancel();
                  optionTime--;
                  showAnswer = true;
                });
              }
            }
            progressValue = (optionTime / time) * 100;
            progressPercentage = progressValue / 100;
          });
        }
        return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 90,
              height: 70,
              margin: EdgeInsets.only(left: 20),
              padding: EdgeInsets.only(bottom: 10),
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/question/score.webp"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: Text(
                question.positivePoints.round().toString(),
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Titillium",
                ),
              ),
            ),
            question.questionTime >= 0
                ? question.optionTime <= 0
                    ? Center()
                    : Container(
                        margin: EdgeInsets.only(right: 30),
                        alignment: FractionalOffset.center,
                        child: Stack(
                          alignment: FractionalOffset.center,
                          children: [
                            SizedBox(
                              height: 55,
                              width: 55,
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                                strokeWidth: 10,
                                value: progressPercentage,
                              ),
                            ),
                            Container(
                              width: 55,
                              height: 55,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.white),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30)),
                                color: Color(0xff4a0d9d),
                              ),
                              child: Text(
                                '$optionTime',
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Titillium"),
                              ),
                            ),
                          ],
                        ),
                      )
                : Center(),
          ],
        );
      },
    );
  }

  questionHeader(Question question) {
    if (!questionPlayed) {
      _playQuestionAppearSound();
      questionPlayed = true;
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.1,
          height: MediaQuery.of(context).size.width * 0.1,
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.005,
            right: 2,
          ),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/question/question_no.webp")),
          ),
          child: Text("${globals.questionNumber}",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontFamily: "Titillium",
              )),
        ),
        SizedBox(width: 7),
        Container(
          alignment: FractionalOffset.center,
          width: MediaQuery.of(context).size.width * 0.84,
          child: AutoSizeText(
            "${question.question}",
            minFontSize: 14,
            maxFontSize: 50,
            textAlign: TextAlign.start,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: "Titillium",
            ),
          ),
        ),
      ],
    );
  }

  optionButton(String option, optionState, {height = 70.0, width}) {
    var optionButton = DigiButton(
      height: height,
      width: width,
      child: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          option,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontFamily: "Titillium",
          ),
          textAlign: TextAlign.left,
        ),
      ),
      image: showAnswer
          ? answer == option
              ? AssetImage("assets/question/option_right.webp")
              : selection == option || selectionB == option
                  ? AssetImage("assets/question/option_wrong.webp")
                  : AssetImage("assets/question/option.webp")
          : (selection.isNotEmpty && selection == option) ||
                  (selectionB.isNotEmpty && selectionB == option)
              ? AssetImage("assets/question/option_select.webp")
              : AssetImage("assets/question/option.webp"),
      onPressed: !timeOver
          ? () {
              if (selection.isEmpty) {
                _playSound();
                print("Option Selected:" + option.toString());
                optionState(() {
                  endSelection = DateTime.now();
                  selection = option;
                });
                points = option == answer ? positivePoints : negativePoints;
                var timeTaken = endSelection.difference(timeSelection);
                sendAnalytics(
                    endSelection != null
                        ? timeTaken.inMilliseconds
                        : optionTime * 1000,
                    points,
                    selection);
              }
              /*if (doubleXSelected && selection != option) {
                if (selection.isNotEmpty && selectionB.isEmpty) {
                  _playSound();
                  print("OptionB Selected:" + option.toString());
                  optionState(() {
                    endSelection = DateTime.now();
                    selectionB = option;
                  });
                  points = option == answer ? positivePoints : negativePoints;
                  var timeTaken = endSelection.difference(timeSelection);
                  sendAnalytics(
                      endSelection != null
                          ? timeTaken.inMilliseconds
                          : optionTime * 1000,
                      points,
                      selectionB);
                }
              }*/
            }
          : null,
    );
    var optionCount;
    if (optionArray != null) {
      int i = optionArray
          .indexWhere((element) => element["label"].toString() == option);
      // this is overflowing. handle it. i is increasing more than optionArray
      // length in case the app goes in background. OK I'll do it
      if (i >= 0) optionCount = Text(optionArray[i]["count"]);
    }
    return Column(
      children: [
        showAnswer && answer == option
            ? FadeTransition(opacity: animation, child: optionButton)
            : optionButton,
        optionCount ?? Center(),
      ],
    );
    /*return showAnswer && answer == option
        ? FadeTransition(opacity: animation, child: optionButton)
        : optionButton;*/
  }

  options(Question question) {
    timeSelection = DateTime.now();
    return StatefulBuilder(builder: (optionContext, optionState) {
      if (loadOptions) {
        if (selection.isEmpty && !timeOver && question.optionTime > 0) {
          if (!gameExited) {
            Future.delayed(Duration(seconds: optionTime), () {
              if (!gameExited) {
//                getAnswerRatio();
                Future.delayed(
                    Duration(
                        seconds: question.analyticsTime > 0
                            ? question.analyticsTime
                            : 4),
                    !gameExited
                        ? () {
                            if (imageCtxParent != null) {
                              Navigator.of(imageCtxParent).pop();
                            }
                            if (!gameExited) {
                              finish = true;
                              changeQuestionItself(question);
                            }
                          }
                        : () {
                            if (imageCtxParent != null) {
                              Navigator.of(imageCtxParent).pop();
                            }
                          });
                optionState(() {
                  showAnswer = true;
                });
              }
            });
          }
        } else if (selection.isNotEmpty &&
            (question.optionTime <= 0 || question.questionTime <= 0)) {
          showAnswer = true;
          Future.delayed(
              Duration(
                seconds:
                    question.analyticsTime > 0 && question.questionTime == 0
                        ? question.analyticsTime
                        : 4,
              ), () {
            if ((selection.isNotEmpty &&
                (question.answer == selection ||
                    question.answer == selectionB))) {
              changeQuestionItself(question);
            } else {
              changeQuestionItself(question);
            }
          });
        }
        if (!optionPlayed) {
          _playOptionSound();
          optionPlayed = true;
          if (!globals.practice) PlayAudio(question.optionTime);
        }
        if (optionPlayed && showAnswer) {
          stopAudio();
          if (selection == answer || selectionB == answer) {
            _playRightSound();
          } else {
            _playWrongSound();
          }
        }
        return Center(
          child: question.questionMedia != null &&
                  question.questionMedia.isNotEmpty
              ? Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 0.2,
                    mainAxisSpacing: 0.0,
                    childAspectRatio: 2.2,
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children: [
                      question.optionA != null && question.optionA.isNotEmpty
                          ? optionButton(
                              question.optionA.trim(),
                              optionState,
                              height: 60.0,
                              width: (MediaQuery.of(context).size.width / 2) -
                                  45.0,
                            )
                          : Center(),
                      question.optionB != null && question.optionB.isNotEmpty
                          ? optionButton(
                              question.optionB.trim(),
                              optionState,
                              height: 60.0,
                              width: (MediaQuery.of(context).size.width / 2) -
                                  45.0,
                            )
                          : Center(),
                      question.optionC != null && question.optionC.isNotEmpty
                          ? optionButton(
                              question.optionC.trim(),
                              optionState,
                              height: 60.0,
                              width: (MediaQuery.of(context).size.width / 2) -
                                  45.0,
                            )
                          : Center(),
                      question.optionD != null && question.optionD.isNotEmpty
                          ? optionButton(
                              question.optionD.trim(),
                              optionState,
                              height: 60.0,
                              width: (MediaQuery.of(context).size.width / 2) -
                                  45.0,
                            )
                          : Center(),
                    ],
                  ),
                )
              : Column(
                  children: [
                    question.optionA != null && question.optionA.isNotEmpty
                        ? optionButton(
                            question.optionA.trim(),
                            optionState,
                            width: MediaQuery.of(context).size.width - 65.0,
                          )
                        : Center(),
                    question.optionB != null && question.optionB.isNotEmpty
                        ? optionButton(
                            question.optionB.trim(),
                            optionState,
                            width: MediaQuery.of(context).size.width - 65.0,
                          )
                        : Center(),
                    question.optionC != null && question.optionC.isNotEmpty
                        ? optionButton(question.optionC.trim(), optionState,
                            width: MediaQuery.of(context).size.width - 65.0)
                        : Center(),
                    question.optionD != null && question.optionD.isNotEmpty
                        ? optionButton(question.optionD.trim(), optionState,
                            width: MediaQuery.of(context).size.width - 65.0)
                        : Center(),
                  ],
                ),
        );
      } else {
        int defaultTime = 0;
        if (globals.practice) {
          defaultTime = 4;
        }
        Future.delayed(
            Duration(
                seconds: question.questionTime > 0
                    ? question.questionTime
                    : defaultTime), () {
          setState(() {
            showReplayBtn = true;
            loadOptions = true;
          });
        });
        return Center();
      }
    });
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    animation.dispose();
    _stopSound();
    if (_controller != null) _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          body: Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.transparent,
            child: Stack(
              children: [
                Column(
                  children: [
                    //region Number of Users Online and Header Logo
                    Container(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).padding.top),
                        child: Container(
                          margin: EdgeInsets.only(left: 20),
                          padding: EdgeInsets.fromLTRB(10, 25, 10, 0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  globals.profilePicture.isNotEmpty
                                      ? ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(75.0),
                                          child: FadeInImage(
                                            width: 70,
                                            placeholder: AssetImage(
                                                "assets/question/player.webp"),
                                            image: NetworkImage(
                                                globals.profilePicture),
                                          ),
                                        )
                                      : Image.asset(
                                          "assets/question/player.webp",
                                          width: 70,
                                        ),
                                  Text(
                                    globals.userName,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "Titillium",
                                    ),
                                  ),
                                ],
                              ),
                              GestureDetector(
                                child: Image.asset(
                                  "assets/iql_logo.webp",
                                  width: 110,
                                  fit: BoxFit.fitWidth,
                                ),
                                onTap: () {
                                  _onBackPressed();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    //endregion

                    //region Builder to retrieving Question from DataBase
                    Expanded(
                      child: FutureBuilder(
                        future: getQuestion(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasData) {
                              var data = snapshot.data;
                              return SingleChildScrollView(
                                child: question(data),
                              );
                            } else {
                              var data = snapshot.error;
                              print(snapshot.error);
                              return Center(
                                  child: Text(data.toString(),
                                      style: TextStyle(color: Colors.white)));
                            }
                          } else if (snapshot.connectionState ==
                              ConnectionState.none) {
                            return Center(
                                child: Text("Unable to Create Connection",
                                    style: TextStyle(color: Colors.white)));
                          } else {
                            return Center();
                          }
                        },
                      ),
                    ), //endregion
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
//                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  height: 70,
                  child: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    leading: Material(
                      child: IconButton(
                        iconSize: 20,
                        icon: Image.asset("assets/icons/back_arrow.webp"),
                        onPressed: () {
                          _onBackPressed();
                        },
                        tooltip: "Back",
                      ),
                      color: Colors.transparent,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  changeQuestionItself(Question ques) {
    if (globals.last) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => GameOverScreen(),
      ));
    } else {
      globals.questionNumber++;
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => RequestQuestion(question: ques),
        ),
      );
    }
  }

  //region Sound Initializaion, Play and Stop(Release)

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    if (finish || gameExited) {
      return;
    }
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<int> _loadOptionSound() async {
    var asset = await rootBundle.load("sounds/daily_option_appear.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playOptionSound() async {
    if (finish || gameExited) {
      return;
    }
    var _optionAppearSound = await _optionSound;
    _ClickSoundStreamId = await _soundpool.play(_optionAppearSound);
  }

  Future<int> _loadQuestionAppearSound() async {
    var asset =
        await rootBundle.load("assets/sounds/alpha_question_appear.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playQuestionAppearSound() async {
    if (finish || gameExited) {
      return;
    }
    var _questionAppearSound = await _questionSound;
    _ClickSoundStreamId = await _soundpool.play(_questionAppearSound);
  }

  Future<int> _loadRightSound() async {
    var asset = await rootBundle.load("sounds/right_ans_new.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playRightSound() async {
    if (finish || gameExited) {
      return;
    }
    var _questionAppearSound = await _rightSound;
    _ClickSoundStreamId = await _soundpool.play(_questionAppearSound);
  }

  Future<int> _loadWrongSound() async {
    var asset = await rootBundle.load("sounds/wrong_ans_new.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playWrongSound() async {
    if (finish || gameExited) {
      return;
    }
    var _questionAppearSound = await _wrongSound;
    _ClickSoundStreamId = await _soundpool.play(_questionAppearSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
    stopAudio();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  // endregion

  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    questionAudio = new AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  Future<int> playQuestionAudio(url) async {
    int result = await questionAudio.play(url, volume: 1.0);
    return result;
  }

  void PlayAudio(int time) async {
//    audioCache.play();
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    if (gameExited) {
      return;
    }
    if (time <= 10) {
      audioCache.loop('question_time_10.mp3');
      advancedPlayer.seek(Duration(seconds: 10 - time));
    } else if (time <= 20) {
      audioCache.loop('question_time_20.mp3');
      advancedPlayer.seek(Duration(seconds: 20 - time));
    } else if (time <= 30) {
      audioCache.loop('question_time_30.mp3');
      advancedPlayer.seek(Duration(seconds: 30 - time));
    } else {
      audioCache.loop('question_time_30.mp3');
    }
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer?.stop();
    advancedPlayer?.release();
    questionAudio?.stop();
    questionAudio?.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  //region Sending Analytics and LifeLine
  sendAnalytics(timeInMilli, points, selection) async {
    var webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    Map<String, dynamic> action = {
      "action": "createGameLog",
      "data": {
        "stage": globals.STAGE,
        "authToken": globals.authToken,
        "UserId": globals.userId,
        "GameId": globals.gameId,
        "QuizNo": globals.quizNumber,
        "QuestionId": questionId,
        "choosenOption": selection,
        "timeTakenToChooseOption": timeInMilli,
        "pointsScored": points,
      }
    };
    webSocket.sink.add(jsonEncode(action).toString());
  }

  /*outputRatio(message) {
    if (globals.getBouncer) {
      getBouncerQuestion();
      globals.getBouncer = false;
    }
    var object = jsonDecode(message);
    print(object);
    if (object.containsKey("msg")) {
      if (object['msg'].contains("Options with number of users")) {
        optionArray = object['data'];
      }
      if (object['msg'].contains("bouncer")) {
        if (globals.isBouncer) {
          isBouncer = object['isBouncer'];
          if (isBouncer) {
            globals.isBouncer = false;
          }
        }
      }
    } else {
      // print(object['message']);
    }
  }*/

  output(message) async {
    var object = jsonDecode(message);

    if (object.containsKey("msg")) {
    } else {
      print(object['message']);
    }
  }

//endregion

  /*getBouncerQuestion() async {
    Map<String, dynamic> action = {
      "action": "getBuncerQuestion",
      "data": {
        "stage": globals.STAGE,
        "GameId": globals.gameId,
        "QuestionId": questionId,
        "authToken": globals.authToken
      }
    };
    bouncerSocket.sink.add(jsonEncode(action).toString());
  }*/

  /*getAnswerRatio() {
    bouncerSocket = WebSocketChannel.connect(globals.uri);
    bouncerSocket.stream.listen(outputRatio);
    Map<String, dynamic> action = {
      "action": "optionCount",
      "data": {
        "stage": globals.STAGE,
        "GameId": globals.gameId,
        "QuestionId": questionId,
        "authToken": globals.authToken,
      }
    };
    bouncerSocket.sink.add(jsonEncode(action).toString());
  }*/

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Do you want to exit the App ?"),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.pop(context, false),
                    child: Text("No")),
                FlatButton(
                    onPressed: () {
                      gameExited = true;
                      if (_timer != null) _timer.cancel();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => GameList(),
                        ),
                      );
                      _stopSound();
                      stopAudio();
                    },
                    child: Text("Yes"))
              ],
            ));
  }

  imageDialog(src) {
    showDialog(
      context: context,
      builder: (imageCxt) {
        imageCtxParent = imageCxt;
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              color: Colors.transparent,
              height: MediaQuery.of(imageCxt).size.height - 100,
              child: PhotoView(
                backgroundDecoration: BoxDecoration(
                  color: Colors.transparent,
                ),
                imageProvider: NetworkImage(src),
              ),
            ),
            FlatButton(
              child: Icon(
                Icons.cancel,
                size: 50,
              ),
              onPressed: () {
                imageCtxParent = null;
                Navigator.of(imageCxt).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
