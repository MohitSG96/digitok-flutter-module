import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/games_sub_pages/alpha/previous_quizzes.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:soundpool/soundpool.dart';

class Stage2Quiz extends StatefulWidget {
  @override
  _Stage2QuizState createState() => _Stage2QuizState();
}

class _Stage2QuizState extends State<Stage2Quiz> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int _current = 0;
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }

  @override
  Widget build(BuildContext context) {
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/navigation_back.webp"),
          fit: BoxFit.cover,
        )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DrawerHeader(
//              padding: EdgeInsets.zero,
//              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/bg_bubble.webp"),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/iql_logo.webp",
                    width: double.maxFinite,
                    height: 100,
                  ),
                  Text(
                    "IQL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  NavItem(
                    image: Icon(Icons.home),
                    text: Text(
                      "Home",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => GameList(),
                          ),
                          (route) => false);
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/pin.png",
                      height: 25,
                      width: 25,
                      color: navTextColor,
                    ),
                    text: Text(
                      "Notice Board",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.showToast("No Notice at moment");
                      if (_scaffoldKey.currentState.isDrawerOpen)
                        _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  NavItem(
                    icon: Icons.account_circle,
                    text: Text(
                      "Profile",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ));
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/check_list.png",
                      color: navTextColor,
                      height: 25,
                      width: 25,
                    ),
                    text: Text(
                      "Rules and Format",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => GameRulesScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.settings,
                    text: Text(
                      "Settings",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: navTextColor,
                      ),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xFF2F3F7e),
                              width: 0.8,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Color(0xFF2F3F7e),
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Scorecard",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: navTextColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      expandedAlignment: Alignment.centerLeft,
                      childrenPadding: EdgeInsets.only(left: 40),
                      children: <Widget>[
                        NavItem(
                          text: Text(
                            "Your Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals
                                .showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        NavItem(
                          text: Text(
                            "Your School’s Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals
                                .showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    ),
                  ),
                  NavItem(
                    icon: Icons.info,
                    text: Text(
                      "About",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ReportAbuseScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.share,
                    text: Text(
                      "Share App",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {},
                  ),
                  NavItem(
                    icon: Icons.power_settings_new,
                    text: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      UserData().logout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    var height = MediaQuery.of(context).size.height;

    return WillPopScope(
        onWillPop: () {
          return _onBackPressed(context);
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          body: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(top: 80),
                        height: (height) - 80,
                        width: 250,
                        alignment: Alignment.center,
                        child: listOptions(context),
                      ),
                    ),
                    AppBar(
                      backgroundColor: Color(0x00000000),
                      elevation: 0,
                      leading: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          tooltip: "Back",
                          onPressed: () {
                            _onBackPressed(context);
                          }),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).padding.top),
                        child: Image.asset(
                          "assets/iql_logo.webp",
//                        width: 160,
                          height: 90,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          drawer: drawer,
        ));
  }

  listOptions(var context) {
    return Column(
      /*shrinkWrap: true,
      padding: EdgeInsets.all(0),*/
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          height: 180,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CarouselSlider(
                  items: [
                    Container(
                      width: MediaQuery.of(context).size.width - 20,
                      alignment: Alignment.center,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        border: Border.all(color: Colors.black87),
                        color: Colors.deepPurple.shade900,
                      ),
                      child: Text(
                        "Stage 1 results coming up in 2 days",
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 20,
                      alignment: Alignment.center,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        border: Border.all(color: Colors.black87),
                        color: Colors.deepPurple.shade900,
                      ),
                      child: Text(
                        "Stage 2 results coming up in 2 days",
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 20,
                      alignment: Alignment.center,
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        border: Border.all(color: Colors.black87),
                        color: Colors.deepPurple.shade900,
                      ),
                      child: Text(
                        "Check new Rules for Stage 1 Game",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                  options: CarouselOptions(
                    height: 150,
                    enlargeCenterPage: true,
                    aspectRatio: 2.0,
                    autoPlay: true,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    },
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [0, 1, 2].map((e) {
                  int index = e;
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => PreviousQuizzes(),
                ));
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent, width: 1.2),
                color: Colors.deepPurple.shade800),
            child: AutoSizeText(
              "Play",
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30, color: Colors.white),
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => GameRulesScreen(),
            ));
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent, width: 1.2),
                color: Colors.deepPurple.shade800),
            child: AutoSizeText(
              "Rules",
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30, color: Colors.white),
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {},
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent, width: 1.2),
                color: Colors.deepPurple.shade800),
            child: AutoSizeText(
              "Results",
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30, color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  Future<bool> _onBackPressed(var context) {
    return Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => GameList(),
        ));
  }
}
