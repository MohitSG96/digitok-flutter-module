import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class LeaderBoard extends StatefulWidget {
  @override
  _LeaderBoardState createState() => _LeaderBoardState();
}

class _LeaderBoardState extends State<LeaderBoard> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int leaderBoardTab = 1, //1: School Tab, 2: Open Tab, 3: Mixed Tab
      quizType =
          1; //1: Daily Quiz, 2: Weekly Quiz, 3: OnGoing Quiz listView Page
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  @override
  void initState() {
    super.initState();
    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();
  }
  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    _stopSound();
  }
  @override
  Widget build(BuildContext context) {
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/navigation_back.webp"),
              fit: BoxFit.cover,
            )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DrawerHeader(
//              padding: EdgeInsets.zero,
//              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/bg_bubble.webp"),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/iql_logo.webp",
                    width: double.maxFinite,
                    height: 100,
                  ),
                  Text(
                    "IQL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  NavItem(
                    image: Icon(Icons.home),
                    text: Text(
                      "Home",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => GameList(),
                          ),
                              (route) => false);
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/pin.png",
                      height: 25,
                      width: 25,
                      color: navTextColor,
                    ),
                    text: Text(
                      "Notice Board",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.showToast("No Notice at moment");
                      if (_scaffoldKey.currentState.isDrawerOpen)
                        _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  NavItem(
                    icon: Icons.account_circle,
                    text: Text(
                      "Profile",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ));
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/check_list.png",
                      color: navTextColor,
                      height: 25,
                      width: 25,
                    ),
                    text: Text(
                      "Rules and Format",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => GameRulesScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.settings,
                    text: Text(
                      "Settings",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: navTextColor,
                      ),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xFF2F3F7e),
                              width: 0.8,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Color(0xFF2F3F7e),
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Scorecard",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: navTextColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      expandedAlignment: Alignment.centerLeft,
                      childrenPadding: EdgeInsets.only(left: 40),
                      children: <Widget>[
                        NavItem(
                          text: Text(
                            "Your Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        NavItem(
                          text: Text(
                            "Your School’s Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    ),
                  ),
                  NavItem(
                    icon: Icons.info,
                    text: Text(
                      "About",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ReportAbuseScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.share,
                    text: Text(
                      "Share App",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {},
                  ),
                  NavItem(
                    icon: Icons.power_settings_new,
                    text: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      UserData().logout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: EdgeInsets.only(top: 80),
                    height: (height) - 80,
                    alignment: Alignment.center,
                    child: showLeaderBoard(),
                  ),
                ),
                AppBar(
                  backgroundColor: Color(0x00000000),
                      elevation: 0,
                  leading: IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      tooltip: "Back",
                      onPressed: () {
                        _onBackPressed();
                      }),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                    child: Image.asset(
                      "assets/iql_logo.webp",
//                        width: 160,
                      height: 90,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      drawer: drawer,
    );
  }

  showLeaderBoard() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          width: MediaQuery.of(context).size.width,
          child: Image.asset("assets/leaderboard_ribbon.png"),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.center,
          margin: EdgeInsets.fromLTRB(15, 2, 15, 2),
          child: showTabs(),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(10, 2, 10, 2),
          child: showPage(),
        ),
      ],
    );
  }

  showTabs() {
    return Row(
      mainAxisSize: MainAxisSize.max,
//      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(width: 1.2, color: Colors.grey.shade400),
              color: leaderBoardTab != 1 ? null : Colors.transparent,
              image: leaderBoardTab == 1
                  ? DecorationImage(
                      image: AssetImage("assets/base_copy.webp"),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  : null,
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15)),
            ),
            child: Text(
              "Open",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: leaderBoardTab == 1
                    ? Colors.blueAccent.shade700
                    : Colors.grey.shade400,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          onTap: () {
            setState(() {
              leaderBoardTab = 1;
            });
          },
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
            decoration: BoxDecoration(
              border: Border.all(width: 1.2, color: Colors.grey.shade400),
              color: leaderBoardTab != 2 ? null : Colors.transparent,
              image: leaderBoardTab == 2
                  ? DecorationImage(
                      image: AssetImage("assets/base_copy.webp"),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  : null,
            ),
            child: Text(
              "Mixed",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: leaderBoardTab == 2
                    ? Colors.blueAccent.shade700
                    : Colors.grey.shade400,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          onTap: () {
            setState(() {
              leaderBoardTab = 2;
            });
          },
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
            decoration: BoxDecoration(
              border: Border.all(width: 1.2, color: Colors.grey.shade400),
              color: leaderBoardTab != 3 ? null : Colors.transparent,
              image: leaderBoardTab == 3
                  ? DecorationImage(
                      image: AssetImage("assets/base_copy.webp"),
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                    )
                  : null,
              borderRadius: BorderRadius.only(topRight: Radius.circular(15)),
            ),
            child: Text(
              "Schools",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: leaderBoardTab == 3
                    ? Colors.blueAccent.shade700
                    : Colors.grey.shade400,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          onTap: () {
            setState(() {
              leaderBoardTab = 3;
            });
          },
        ),
      ],
    );
  }

  showPage() {
    var decoration = BoxDecoration(
        border: Border.all(
          color: Colors.blue.shade800,
          width: 1.2,
        ),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          bottomLeft: Radius.circular(20),
        ),
        color: Colors.white);
    switch (leaderBoardTab) {
      case 2:
        return Container(
          decoration: decoration,
          alignment: Alignment.center,
          child: mixedTab(),
        );
        break;
      case 3:
        return Container(
          decoration: decoration,
          alignment: Alignment.center,
          child: schoolTab(),
        );
        break;
      case 1:
      default:
        return Container(
          decoration: decoration,
          alignment: Alignment.center,
          child: openCandidateTab(),
        );
    }
  }

  schoolTab() {
    return Column(
      children: [
        showHeaderButtons(),
        showTopWinners(),
      ],
    );
  }

  openCandidateTab() {}

  mixedTab() {}

  showHeaderButtons() {
    var buttonWidth = (MediaQuery.of(context).size.width - 50) / 3;
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
          onTap: () {
            if (quizType != 1)
              setState(() {
                quizType = 1;
              });
          },
          child: Container(
            alignment: Alignment.center,
            width: buttonWidth,
            decoration: BoxDecoration(
              color: quizType == 1 ? Colors.white : null,
              gradient: quizType == 1
                  ? null
                  : RadialGradient(
                      radius: 2,
                      colors: [Colors.white, Colors.grey.shade400],
                    ),
            ),
            child: Text(
              "Daily",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: quizType == 1 ? Colors.blue.shade600 : Colors.grey,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            if (quizType != 2)
              setState(() {
                quizType = 2;
              });
          },
          child: Container(
            alignment: Alignment.center,
            width: buttonWidth,
            decoration: BoxDecoration(
              color: quizType == 2 ? Colors.white : null,
              gradient: quizType == 2
                  ? null
                  : RadialGradient(
                      radius: 2,
                      colors: [Colors.white, Colors.grey.shade400],
                    ),
            ),
            child: Text(
              "Weekly",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: quizType == 2 ? Colors.blue.shade600 : Colors.grey,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            if (quizType != 3)
              setState(() {
                quizType = 3;
              });
          },
          child: Container(
            alignment: Alignment.center,
            width: buttonWidth,
            decoration: BoxDecoration(
                color: quizType == 3 ? Colors.white : null,
                gradient: quizType == 3
                    ? null
                    : RadialGradient(
                        radius: 2,
                        colors: [Colors.white, Colors.grey.shade400],
                      ),
                borderRadius: BorderRadius.only(topRight: Radius.circular(11))),
            child: Text(
              "Ongoing",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: quizType == 3 ? Colors.blue.shade600 : Colors.grey,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ],
    );
  }

  showTopWinners() {
    return Container(
      margin: EdgeInsets.all(3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(3),
                bottomLeft: Radius.circular(3),
              ),
            ),
            child: Column(
              children: [
                Image.asset(
                  "assets/icons/avtar.webp",
                  width: 40,
                  fit: BoxFit.fitWidth,
                ),
                Text(
                  "Gopal Sharma",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Colors.white, Colors.grey.shade300],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.blue.shade900,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(3),
                bottomLeft: Radius.circular(3),
              ),
            ),
            child: Column(
              children: [
                Image.asset(
                  "assets/icons/avtar.webp",
                  width: 40,
                  fit: BoxFit.fitWidth,
                ),
                Text(
                  "Gopal Sharma",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(0xFF), Colors.grey.shade300],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(3),
                bottomLeft: Radius.circular(3),
              ),
            ),
            child: Column(
              children: [
                Image.asset(
                  "assets/icons/avtar.webp",
                  width: 40,
                  fit: BoxFit.fitWidth,
                ),
                Text(
                  "Gopal Sharma",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Colors.white, Colors.grey.shade300],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => GameList(),
        ));
  }
}
