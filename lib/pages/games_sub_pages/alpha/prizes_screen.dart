import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';

class PrizesScreen extends StatefulWidget {
  @override
  _PrizesScreenState createState() => _PrizesScreenState();
}

class _PrizesScreenState extends State<PrizesScreen>
    with WidgetsBindingObserver {
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  List<String> prizes;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    Screen.keepOn(true);
    prizes = [
      "A lot of daily and weekly prizes are up for grabs from Indiannica!",
      "We will contact the daily and weekly winners via their mobile number or email id. So, please make sure your contact numbers and email IDs are updated in the Profile page"
    ];
/*    initPlayer();
    PlayAudio();*/

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/navigation_back.webp"),
              fit: BoxFit.cover,
            )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DrawerHeader(
//              padding: EdgeInsets.zero,
//              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/bg_bubble.webp"),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/iql_logo.webp",
                    width: double.maxFinite,
                    height: 100,
                  ),
                  Text(
                    "IQL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  NavItem(
                    image: Icon(Icons.home),
                    text: Text(
                      "Home",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => GameList(),
                          ),
                              (route) => false);
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/pin.png",
                      height: 25,
                      width: 25,
                      color: navTextColor,
                    ),
                    text: Text(
                      "Notice Board",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.showToast("No Notice at moment");
                      if (_scaffoldKey.currentState.isDrawerOpen)
                        _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  NavItem(
                    icon: Icons.account_circle,
                    text: Text(
                      "Profile",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ));
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/check_list.png",
                      color: navTextColor,
                      height: 25,
                      width: 25,
                    ),
                    text: Text(
                      "Rules and Format",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => GameRulesScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.settings,
                    text: Text(
                      "Settings",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: navTextColor,
                      ),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xFF2F3F7e),
                              width: 0.8,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Color(0xFF2F3F7e),
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Scorecard",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: navTextColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      expandedAlignment: Alignment.centerLeft,
                      childrenPadding: EdgeInsets.only(left: 40),
                      children: <Widget>[
                        NavItem(
                          text: Text(
                            "Your Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        NavItem(
                          text: Text(
                            "Your School’s Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    ),
                  ),
                  NavItem(
                    icon: Icons.info,
                    text: Text(
                      "About",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ReportAbuseScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.share,
                    text: Text(
                      "Share App",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {},
                  ),
                  NavItem(
                    icon: Icons.power_settings_new,
                    text: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      UserData().logout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            tooltip: "Back",
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => GameList(),
                ),
              );
            },
          ),
          backgroundColor: Colors.transparent,
        ),
        body: Center(
          child: Container(
            width: width - 30,
            height: height - 130,
            decoration: BoxDecoration(
              color: Colors.deepPurple.shade800,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 15.0),
                  child: AutoSizeText(
                    "Prizes",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 32.0),
                  ),
                ),
                Container(
                  width: width,
                  height: height - 200,
                  padding: EdgeInsets.only(right: 15.0, left: 15.0, top: 10.0),
                  child: ListView.builder(
//                  shrinkWrap: true,
                    itemCount: prizes.length,
                    itemBuilder: (context, index) {
                      String _rules = prizes[index];
                      return GestureDetector(
                        child: rules_item(_rules),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        drawer: drawer,
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
  }

  rules_item(title) {
    var width = MediaQuery.of(context).size.width;

    return Align(
        alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.only(top: 7),
          child: Container(
              width: width,
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(right: 10),
                          child: Image.asset(
                            "assets/icons/bullet.jpg",
                            color: Colors.white,
                          )),
                      Flexible(
                        child: AutoSizeText(
                          title.toString(),
                          softWrap: true,
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Container(
                          height: 0.5, width: width, color: Colors.grey)),
                ],
              )),
        ));
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  //endregion

  @override
  void dispose() {
    super.dispose();
    stopAudio();
    WidgetsBinding.instance.removeObserver(this);
  }
}
