import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:digitok/models/game.dart';
import 'package:digitok/models/game_feature.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/countdown_screen.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/games_sub_pages/alpha/previous_quizzes.dart';
import 'package:digitok/pages/games_sub_pages/alpha/prizes_screen.dart';
import 'package:digitok/pages/games_sub_pages/game_rules.dart';
import 'package:digitok/pages/games_sub_pages/new_leaderboard.dart';
import 'package:digitok/pages/profile_screen.dart';
import 'package:digitok/pages/report_abuse.dart';
import 'package:digitok/pages/settings_screen.dart';
import 'package:digitok/ui_widgets/nav_item.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:soundpool/soundpool.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class DailyQuiz extends StatefulWidget {
  final Game game;

  const DailyQuiz({Key key, @required this.game}) : super(key: key);

  @override
  _DailyQuizState createState() => _DailyQuizState();
}

class _DailyQuizState extends State<DailyQuiz> with WidgetsBindingObserver {
  bool _isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var webSocket;
  Game game;
  String _authToken;
  GameFeatures _gameFeature;
  var questionList;
  bool goPreviousQuizzes = false;
  var quizNumber;

  Soundpool _soundpool;
  Future<int> _soundId;
  int _ClickSoundStreamId;

  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();

    initPlayer();
    PlayAudio();

    _soundpool = Soundpool();
    _soundId = _loadSound();

    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    game = widget.game;
    getStack();
  }

  void getStack() async {
    _authToken = await UserData().getAuthToken();
    User _user = await UserData().getUser();
    Map<String, dynamic> action = {
      "action": "stacksList",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
        "condition": {"id": game.stackId}
      }
    };
//    print(jsonEncode(action));
    webSocket.sink.add(jsonEncode(action).toString());
    globals.authToken = _authToken;
    globals.userId = _user.id;
    globals.userName = _user.username;
  }

  void getDailyQuiz() async {
    quizNumber = 1;
    globals.quizNumber = quizNumber;
    _authToken = await UserData().getAuthToken();
    User _user = await UserData().getUser();
    Map<String, dynamic> action = {
      "action": "FetchQuiz",
      "data": {
        "stage": globals.STAGE,
        "authToken": _authToken,
        "QuizNo": quizNumber,
        "StackId": 90
      }
    };
    print(jsonEncode(action));
    webSocket.sink.add(jsonEncode(action).toString());
    globals.authToken = _authToken;
    globals.userId = _user.id;
    globals.userName = _user.username;
  }

  output(message) {
    Map<String, dynamic> object = jsonDecode(message);

    if (object.containsKey("msg")) {
      if (object['msg'].toString().contains("played the quiz")) {
        if (object['isPlayed'] != null && !object['isPlayed']) {
          var data = object['quiz'];
          globals.extraLife = 0;
          globals.doubleX = false;
          globals.bankItQuestion = -1;
          globals.animtype = "";
          questionList = data['questionIds'] ?? [];
          print(questionList);
          setState(() {
            _isLoading = false;
          });
        } else {
          print(message);
//          globals.showToast("Questions not found");
          setState(() {
            _isLoading = false;
          });
        }
      } else if (object['msg'].toString().contains("gamelog")) {
      } else {
        if (object.containsKey("data") &&
            object['data'] != null &&
            object['data'].length > 0) {
          getDailyQuiz();
          var data = object['data'][0];
          _gameFeature = GameFeatures.fromJson(data);
          globals.gameFeatures = _gameFeature;
          /*globals.extraLife = _gameFeature.game.heartCount;
            globals.doubleX = _gameFeature.game.is2x;
          globals.bankItQuestion = _gameFeature.bankOn;
          globals.animtype = _gameFeature.game.animationType;
          globals.doubleX = false;
          globals.extraLife = -2;*/

          /*setState(() {
            _isLoading = false;
          });*/
          if (data['Game'].containsKey('countdownTimer'))
            globals.countdown = data['Game']['countdownTimer'] ?? 10;
          else
            globals.countdown = 10;
        } else {
          globals.showToast(
              "Game not Found or taking long time to load\n Please try again");
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => GameList(),
          ));
        }
      }
    } else {
      globals.showToast("Something went wrong\nPlease come again in sometime");
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => GameList(),
      ));
    }
  }

  cacheRewardImage() async {
    for (String url in _gameFeature.game.rewardImages) {
      CachedNetworkImage(imageUrl: url);
    }
  }

  @override
  Widget build(BuildContext context) {
    var navTextColor = Color(0xFF2F3F7e);
    var drawer = Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/navigation_back.webp"),
              fit: BoxFit.cover,
            )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DrawerHeader(
//              padding: EdgeInsets.zero,
//              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/bg_bubble.webp"),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    "assets/iql_logo.webp",
                    width: double.maxFinite,
                    height: 100,
                  ),
                  Text(
                    "IQL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  NavItem(
                    image: Icon(Icons.home),
                    text: Text(
                      "Home",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => GameList(),
                          ),
                              (route) => false);
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/pin.png",
                      height: 25,
                      width: 25,
                      color: navTextColor,
                    ),
                    text: Text(
                      "Notice Board",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.showToast("No Notice at moment");
                      if (_scaffoldKey.currentState.isDrawerOpen)
                        _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  NavItem(
                    icon: Icons.account_circle,
                    text: Text(
                      "Profile",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ProfileScreen(),
                      ));
                    },
                  ),
                  NavItem(
                    image: Image.asset(
                      "assets/icons/check_list.png",
                      color: navTextColor,
                      height: 25,
                      width: 25,
                    ),
                    text: Text(
                      "Rules and Format",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => GameRulesScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.settings,
                    text: Text(
                      "Settings",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: navTextColor,
                      ),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Color(0xFF2F3F7e),
                              width: 0.8,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        padding: EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Color(0xFF2F3F7e),
                            ),
                            SizedBox(width: 15),
                            Text(
                              "Scorecard",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: navTextColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      expandedAlignment: Alignment.centerLeft,
                      childrenPadding: EdgeInsets.only(left: 40),
                      children: <Widget>[
                        NavItem(
                          text: Text(
                            "Your Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                        NavItem(
                          text: Text(
                            "Your School’s Scorecard",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: navTextColor,
                            ),
                          ),
                          onTap: () {
                            globals.showToast("This Week's Score is Processing");
                            if (_scaffoldKey.currentState.isDrawerOpen)
                              _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      ],
                    ),
                  ),
                  NavItem(
                    icon: Icons.info,
                    text: Text(
                      "About",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      globals.Screen_name = "GameList";
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ReportAbuseScreen()));
                    },
                  ),
                  NavItem(
                    icon: Icons.share,
                    text: Text(
                      "Share App",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {},
                  ),
                  NavItem(
                    icon: Icons.power_settings_new,
                    text: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: navTextColor),
                    ),
                    onTap: () {
                      UserData().logout(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    var height = MediaQuery.of(context).size.height;

    return WillPopScope(
        onWillPop: () {
          return _onBackPressed(context);
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          key: _scaffoldKey,
          body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(top: 90, bottom: 10),
                        padding: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 15,
                        ),
                        height: (height) - 150,
                        width: MediaQuery.of(context).size.width - 55,
                        alignment: Alignment.center,
                        child: _isLoading ? globals.lottie : listOptions(),
                        decoration: _isLoading ? null :BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25),
                            bottomLeft: Radius.circular(25),
                          ),
                          border: Border.all(
                            color: Colors.lightBlueAccent,
                            width: 1.5,
                          ),
                          image: DecorationImage(
                            image: AssetImage("assets/base_copy.webp"),
                            colorFilter: new ColorFilter.mode(
                              Colors.black.withOpacity(0.9),
                              BlendMode.dstATop,
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    AppBar(
                      backgroundColor: Color(0x00000000),
                      elevation: 0,
                      leading: IconButton(
                          icon: Image.asset("assets/btn/hamburger_button.webp"),
                          tooltip: "Menu",
                          onPressed: () {
                            _scaffoldKey.currentState.openDrawer();
                          }),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                        child: Image.asset(
                          "assets/iql_logo.webp",
//                        width: 160,
                          height: 90,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 15),
                    width: 60,
                    child: Image.asset(
                      "assets/btn/back_button.webp",
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    _onBackPressed(context);
                  },
                ),
              ),
            ],
          ),
          drawer: drawer,
        ));
  }

  listOptions() {
    return Column(
      /*shrinkWrap: true,
      padding: EdgeInsets.all(0),*/
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: [
        GestureDetector(
          onTap: () {
            globals.language = "english";
            if (questionList != null && questionList.length > 0) {
              _playSound();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) =>
                    CountDownScreen(questionList: questionList),
              ));
            } else {
              globals.showToast("No Questions Added in today's game!");
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width - 90,
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/btn/band_button.webp"),
                fit: BoxFit.fill,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RotatedBox(
                  quarterTurns: 0,
                  child: Image.asset(
                    "assets/btn/play_button.webp",
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: AutoSizeText(
                    "Today's Quiz".toUpperCase(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {
            if(goPreviousQuizzes){
              _playSound();
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PreviousQuizzes(),
                  ));
            } else {
              globals.showToast("No previous quizzes for you");
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width - 90,
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
//                border: Border.all(color: Colors.blueAccent, width: 1.2),
//                color: Colors.deepPurple.shade800,
              image: DecorationImage(
                image: AssetImage("assets/btn/band_button.webp"),
                fit: BoxFit.fill,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RotatedBox(
                  quarterTurns: 2,
                  child: Image.asset(
                    "assets/btn/play_button.webp",
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
//                  flex: 2,
                  child: AutoSizeText(
                    "Previous Quizzes".toUpperCase(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {
            _playSound();
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => GameRulesScreen(feature: _gameFeature),
              ),
            );
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
//                border: Border.all(color: Colors.blueAccent, width: 1.2),
//                color: Colors.deepPurple.shade800,
              image: DecorationImage(
                image: AssetImage("assets/btn/band_button.webp"),
                fit: BoxFit.fill,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RotatedBox(
                  quarterTurns: 0,
                  child: Image.asset(
                    "assets/btn/rules_button.webp",
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: AutoSizeText(
                    "Rules".toUpperCase(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {
            _playSound();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => PrizesScreen(),
            ));
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
//                border: Border.all(color: Colors.blueAccent, width: 1.2),
//                color: Colors.deepPurple.shade800,
              image: DecorationImage(
                image: AssetImage("assets/btn/band_button.webp"),
                fit: BoxFit.fill,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RotatedBox(
                  quarterTurns: 0,
                  child: Image.asset(
                    "assets/btn/play_button.webp",
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: AutoSizeText(
                    "Prizes".toUpperCase(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        GestureDetector(
          onTap: () {
            _playSound();
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => LeaderBoard(),
            ));
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
//                border: Border.all(color: Colors.blueAccent, width: 1.2),
//                color: Colors.deepPurple.shade800,
              image: DecorationImage(
                image: AssetImage("assets/btn/band_button.webp"),
                fit: BoxFit.fill,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RotatedBox(
                  quarterTurns: 0,
                  child: Image.asset(
                    "assets/btn/play_button.webp",
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: AutoSizeText(
                    "LeaderBoard".toUpperCase(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //region background music
  void initPlayer() {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  }

  void PlayAudio() {
    if (Platform.isIOS) {
      advancedPlayer
          .monitorNotificationStateChanges(globals.audioPlayerHandler);
    }
    audioCache.loop('opening_music.mp3');
    advancedPlayer.setVolume(1.0);
  }

  void stopAudio() {
    advancedPlayer.stop();
    advancedPlayer.release();
  }

  void pauseAudio() {
    advancedPlayer.pause();
  }

  void resumeAudio() {
    advancedPlayer.resume();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      pauseAudio();
    } else if (state == AppLifecycleState.resumed) {
      resumeAudio();
    }
  }

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("sounds/click_sound.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _ClickSound = await _soundId;
    _ClickSoundStreamId = await _soundpool.play(_ClickSound);
  }

  Future<void> _stopSound() async {
    if (_ClickSoundStreamId != null) {
      await _soundpool.stop(_ClickSoundStreamId);
      _soundpool.release();
    }
  }

  //endregion

  @override
  void dispose() {
    stopAudio();
    _stopSound();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<bool> _onBackPressed(var context) {
    return Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => GameList(),
        ));
  }
}
