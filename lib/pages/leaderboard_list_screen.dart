import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:digitok/pages/giftsvoucher_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:intl/intl.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class LeaderBoardUsers extends StatefulWidget {
  final userList, index;

  const LeaderBoardUsers({Key key, this.userList, this.index})
      : super(key: key);

  @override
  _LeaderBoardUsersState createState() => _LeaderBoardUsersState();
}

class _LeaderBoardUsersState extends State<LeaderBoardUsers> {
  int userIndex = 0;
  int totalUsers = 0;
  var usersList;
  var webSocket1;
  var _authToken;

  @override
  void initState() {
    super.initState();
    Screen.keepOn(true);
    if (widget.userList != null) usersList = widget.userList;
    if (widget.index != null) userIndex = widget.index;
    webSocket1 = WebSocketChannel.connect(globals.uri);
    webSocket1.stream.listen(OnlineUserOutput);
  }

  void CheckUserOnline() async {
    _authToken = await UserData().getAuthToken();

    Map<String, dynamic> data = {
      "stage": globals.STAGE,
      "authToken": _authToken,
    };

    Map<String, dynamic> action = {
      "action": "totalUsers",
      "data": data,
    };
    webSocket1.sink.add(jsonEncode(action).toString());
  }

  OnlineUserOutput(message) {
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg']
          .toString()
          .contains("Game list with number of users found")) {
        print(object['data']);
        if (object['data'].length > 0) {
          var data = object['data'][0];
          totalUsers = data['totalUsers'];
        }
        globals.totalUsersOnline = totalUsers;
      }
    }
  }

  personProfile(context,Map<String, dynamic> user) {
    var width = MediaQuery.of(context).size.width;
    print(user);
    return Container(
      height: 235,
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: FadeInImage(
                  width: 60,
                  height: 60,
                  placeholder: AssetImage("assets/icons/avtar.webp"),
                  image: user['profilePicture'] != null &&
                          user['profilePicture'].toString().isNotEmpty &&
                          !user['profilePicture']
                              .toString()
                              .contains("no image")
                      ? NetworkImage(user['profilePicture'])
                      : AssetImage("assets/icons/avtar.webp"),
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AutoSizeText(
                      user["username"],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Rank: ${user["rank"]}",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/history_background.jpg"),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              children: [
                user.containsKey("totalGamesPlayed") && user["totalGamesPlayed"] != null? Padding(
                  padding: EdgeInsets.only(
                      top: 15, left: 5.0, right: 5.0, bottom: 5.0),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/gameplayedicon.png",
                        width: 45,
                        height: 45,
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: AutoSizeText("Total Games Played",
                            style: TextStyle(
                                fontSize: 18.0, color: HexColor("#3e1b5f"))),
                      ),
                      AutoSizeText("$user['totalGamesPlayed']",
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: HexColor("#3e1b5f"))),
                      SizedBox(width: 10),
                    ],
                  ),
                ):Center(),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: Container(
                        height: 2.0, width: width, color: HexColor("#E3DDDD"))),
                Padding(
                  padding: EdgeInsets.only(
                      top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/totalpointsicon.png",
                        width: 45,
                        height: 45,
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: AutoSizeText("Total Points Won",
                            style: TextStyle(
                                fontSize: 18.0, color: HexColor("#3e1b5f"))),
                      ),
                      AutoSizeText("${user["points"]}",
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: HexColor("#3e1b5f"))),
                      SizedBox(width: 10),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  userGrid() {
    return GridView.builder(
      itemCount: usersList.length,
      gridDelegate:
          new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (context, index) {
        var user = usersList[index];
        return GestureDetector(
          onTap: () {
            showDialog(
              context: this.context,
              barrierDismissible: true,
              builder: (context) {
                return AlertDialog(
                  content: personProfile(context, user),
                  backgroundColor: Color(0xFF64114E),
                  actions: [
                    CupertinoButton(
                        child: Text(
                          "Close",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })
                  ],
                );
              },
            );
          },
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: FadeInImage(
                  width: 45,
                  height: 45,
                  placeholder: AssetImage("assets/icons/avtar.webp"),
                  image: user['profilePicture'] != null &&
                          user['profilePicture'].toString().isNotEmpty &&
                          !user['profilePicture']
                              .toString()
                              .contains("no image")
                      ? NetworkImage(user['profilePicture'])
                      : AssetImage("assets/icons/avtar.webp"),
                ),
              ),
              Text(
                "${user['username']}",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                "${user['points']} pts",
                textAlign: TextAlign.center,
                style: TextStyle(color: HexColor("#FFCA00")),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: true,
          body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: globals.gameBackGround.isEmpty
                    ? AssetImage("assets/bg_bubble.webp")
                    : NetworkImage(globals.gameBackGround),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    //region Number of Users Online and Header Logo
                    Container(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).padding.top),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 25, 10, 0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(
                                    "assets/followers_icon.webp",
                                    width: 60,
                                  ),
                                  Text(
                                    NumberFormat.compact()
                                        .format(globals.totalUsersOnline),
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              Image.asset(
                                "assets/WinnerScreen/winners_img.webp",
                                width: 100,
                                height: 100,
                              ),
                              GestureDetector(
                                child: Image.asset(
                                  "assets/round_logo.gif",
                                  width: 80,
                                  height: 80,
                                ),
                                onTap: () {
                                  _onBackPressed();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    //endregion

                    //region List of Users Played Game
                    Expanded(
                      child: Align(
                        alignment: FractionalOffset.center,
                        child: Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          decoration:
                              BoxDecoration(color: HexColor("#94CBCBCB")),
                          child: userGrid(),
                        ),
                      ),
                    ),
                    //endregion
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
//                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  height: 70,
                  child: AppBar(
                    backgroundColor: Colors.transparent,
                    leading: Material(
                      child: IconButton(
                        iconSize: 20,
                        icon: Image.asset("assets/icons/back_arrow.webp"),
                        onPressed: () {
                          _onBackPressed();
                        },
                        tooltip: "Back",
                      ),
                      color: Colors.transparent,
                    ),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: new BottomAppBar(
            child: Container(
              height: 50,
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              color: HexColor("#cbcbcb"),
              child: Row(
                children: [
                  //region Rank TEXT and Rank
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          "YOUR RANK",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.center,
                        padding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.black,
                        ),
                        child: Text(
                          "${usersList[userIndex]['rank']}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: HexColor("#FFCA00"),
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  //endregion
                  VerticalDivider(
                    color: Colors.white,
                    thickness: 0.8,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: FadeInImage(
                      width: 45,
                      height: 45,
                      placeholder: AssetImage("assets/icons/avtar.webp"),
                      image: usersList[userIndex]['profilePicture'] != null &&
                              usersList[userIndex]['profilePicture']
                                  .toString()
                                  .isNotEmpty &&
                              !usersList[userIndex]['profilePicture']
                                  .toString()
                                  .contains("no image")
                          ? NetworkImage(usersList[userIndex]['profilePicture'])
                          : AssetImage("assets/icons/avtar.webp"),
                    ),
                  ),
                  SizedBox(width: 5),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("${usersList[userIndex]['username']}"),
                      Text(
                        "${usersList[userIndex]['points']} pts",
                      )
                    ],
                  ),
                ],
              ),
            ),
            color: Colors.transparent,
          ),
        ),
        onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed() {
    return Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GiftsVoucherScreen(),
      ),
    );
  }
}
