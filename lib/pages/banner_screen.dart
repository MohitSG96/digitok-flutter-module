import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:digitok/pages/login_signup/main_home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class BannerScreen extends StatefulWidget {
  @override
  _BannerScreenState createState() => _BannerScreenState();
}

class _BannerScreenState extends State<BannerScreen> {
  List<Widget> bannerImages = [
    Image.asset("assets/banners/frame1.webp"),
    Image.asset("assets/banners/frame3.webp"),
    Image.asset("assets/banners/frame4.webp"),
    Image.asset("assets/banners/frame8.webp"),
  ];

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg_bubble.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg_bubble_white.webp"),
              fit: BoxFit.cover,
              alignment: Alignment.center,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 90,
                height: 90,
                child: Image.asset("assets/digitok_logo.webp"),
              ),
              Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 5, right: 5),
                child: AutoSizeText(
                  "FROM THE TEAM BEHIND \nINDIA’S BEST KNOWLEDGE SHOWS"
                      .toUpperCase(),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xFF4F1546),
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ),
              Container(
                child: CarouselSlider(
                  items: bannerImages,
                  options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 1.0,
                    viewportFraction: 0.9,
                  ),
                ),
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      child: Image.asset(
                        "assets/home.webp",
                        alignment: Alignment.center,
                        height: 65,
                        width: MediaQuery.of(context).size.width,
                      ),
                      onTap: () {
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) => MainHomeScreen(),
                          ),
                        );
                      },
                    ),
                    AutoSizeText(
                      "Tap Home to continue",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF4F1546),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
