import 'dart:convert';

import 'package:digitok/models/game.dart';
import 'package:digitok/models/user.dart';
import 'package:digitok/pages/attachment_model_sheet.dart';
import 'package:digitok/pages/game_screen.dart';
import 'package:digitok/pages/games_list.dart';
import 'package:digitok/pages/history_screen.dart';
import 'package:digitok/utils/globals.dart' as globals;
import 'package:digitok/utils/hex_color.dart';
import 'package:digitok/utils/user_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:screen/screen.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ProfileScreen extends StatefulWidget {
  final Game game;

  const ProfileScreen({Key key, this.game}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  User _user;
  bool _isEditable = false, _isStatLoading = true;
  User newUserData = User();
  GlobalKey _userKey, _userNameKey, _schoolKey, _cityKey, _emailKey, _phoneKey;
  var _name = TextEditingController();
  var _userName = TextEditingController();
  var _school = TextEditingController();
  var _city = TextEditingController();
  var _email = TextEditingController();
  var _phone = TextEditingController();
  var webSocket;
  var _isUserData = false;
  String authToken;
  Map<String, dynamic> userStatsData = {
    "totalGamesPlayed": 5,
    "totalLifeUsed": {"Heart": 1, "BankIt": 0, "PlayOn": 1},
    "totalPoints": 1405,
    "totalGameWon": 2,
    "overAllRank": 3
  };

  @override
  void initState() {
    super.initState();
    webSocket = WebSocketChannel.connect(globals.uri);
    webSocket.stream.listen(output);
    Screen.keepOn(true);
    getUserData();
  }

  getUserData() async {
    _user = await UserData().getUser();
    authToken = await UserData().getAuthToken();
    setState(() {
      _isUserData = true;
    });
  }

  profileInformation() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: _isEditable
                ? () {
                    var sheet =
                        AddAttachmentModalSheet(MediaQuery.of(context).size);
                    showModalBottomSheet(
                      context: context,
                      builder: (context) => sheet,
                      isScrollControlled: true,
                    ).then((value) {
                      if (globals.imageURL != null &&
                          globals.imageURL.isNotEmpty) {
                        setState(() {
                          _user.profilePicture = globals.imageURL;
                          newUserData.profilePicture = globals.imageURL;
                        });
                      }
                    });
                  }
                : () {},
            child: Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(70.0),
                  child: FadeInImage(
                    placeholder: AssetImage("assets/icons/avtar.webp"),
                    image: _user.profilePicture != null &&
                            _user.profilePicture.isNotEmpty
                        ? NetworkImage(_user.profilePicture)
                        : AssetImage("assets/icons/avtar.webp"),
                    height: 100,
                    width: 100,
                  ),
                ),
                Positioned.fill(
                  bottom: 7,
                  right: 5,
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: _isEditable
                        ? Container(
                            alignment: Alignment.center,
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black,
                            ),
                            child: Image.asset(
                              "assets/icons/camera_icon.webp",
                              color: Colors.white,
                              width: 20,
                              height: 20,
                            ),
                          )
                        : Center(),
                  ),
                ),
              ],
            ),
          ),
          name(),
          userName(),
          school(),
          city(),
          email(),
          phone(),
          Divider(
            thickness: 2,
            indent: 15,
            endIndent: 15,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  setState(() {
                    if (_isEditable) {
                      globals.showToast("Saving Profile");
                      save();
                    } else {
                      _name.clear();
                      _userName.clear();
                      _school.clear();
                      _city.clear();
                      _email.clear();
                      _phone.clear();
                      setState(() {
                        _isEditable = true;
                      });
                    }
                  });
                },
                color: Colors.green,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18)),
                child: Text(_isEditable ? "Save Profile" : "Edit Profile"),
              ),
              FlatButton(
                onPressed: () {
                  if (_isEditable) {
                    setState(() {
                      _isEditable = false;
                    });
                  } else {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => GameList(),
                    ));
                  }
                },
                color: Colors.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18)),
                child: Text(_isEditable ? "Cancel" : "Close"),
              ),
            ],
          ),
        ],
      ),
    );
  }

  name() {
    var screenWidth = MediaQuery.of(context).size.width;
    _name.text = _user.name;
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
              left: (screenWidth - 40) * 0.02,
              right: (screenWidth - 40) * 0.02),
          width: 40,
          child: Image.asset("assets/icons/icon_profile.webp"),
        ),
        Container(
          width: (screenWidth - 40) * 0.35,
          child: Text(
            "Name",
            style: TextStyle(color: Colors.white),
          ),
        ),
        Container(
          width: (screenWidth - 40) * 0.54,
          child: TextField(
            key: _userKey,
            keyboardType: TextInputType.text,
            controller: _name,
            enabled: _isEditable,
            style: TextStyle(color: Colors.white),
            onChanged: (val) {
              newUserData.name = val.trim();
            },
          ),
        ),
      ],
    );
  }

  userName() {
    var screenWidth = MediaQuery.of(context).size.width;
    _userName.text = _user.username;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: (screenWidth - 40) * 0.02,
                right: (screenWidth - 40) * 0.02),
            width: 40,
            child: Image.asset("assets/icons/icon_profile.webp"),
          ),
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "UserName",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _userNameKey,
              keyboardType: TextInputType.text,
              controller: _userName,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                newUserData.username = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  school() {
    var screenWidth = MediaQuery.of(context).size.width;
    _school.text = _user.school;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: (screenWidth - 40) * 0.02,
                right: (screenWidth - 40) * 0.02),
            width: 40,
            child: Image.asset("assets/icons/icon_school.webp"),
          ),
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "School/ \nOrganization",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _schoolKey,
              keyboardType: TextInputType.text,
              controller: _school,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                newUserData.school = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  city() {
    _city.text = _user.city;
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: (screenWidth - 40) * 0.02,
                right: (screenWidth - 40) * 0.02),
            width: 40,
            child: Image.asset("assets/icons/icon_city.webp"),
          ),
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "City",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _cityKey,
              keyboardType: TextInputType.text,
              controller: _city,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                newUserData.city = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  email() {
    var screenWidth = MediaQuery.of(context).size.width;
    _email.text = _user.email;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: (screenWidth - 40) * 0.02,
                right: (screenWidth - 40) * 0.02),
            width: 40,
            child: Image.asset("assets/icons/emailicon.webp"),
          ),
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Email",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _emailKey,
              keyboardType: TextInputType.emailAddress,
              controller: _email,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                newUserData.email = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  phone() {
    var screenWidth = MediaQuery.of(context).size.width;
    _phone.text = _user.phone;
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: (screenWidth - 40) * 0.02,
                right: (screenWidth - 40) * 0.02),
            width: 40,
            child: Image.asset("assets/icons/icon_phone.webp"),
          ),
          Container(
            width: (screenWidth - 40) * 0.35,
            child: Text(
              "Phone",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            width: (screenWidth - 40) * 0.54,
            child: TextField(
              key: _phoneKey,
              keyboardType: TextInputType.phone,
              maxLength: 10,
              controller: _phone,
              enabled: _isEditable,
              style: TextStyle(color: Colors.white),
              onChanged: (val) {
                newUserData.phone = val.trim();
              },
            ),
          ),
        ],
      ),
    );
  }

  userStatistics() {
    if (_isStatLoading) {
      return Center(
        child: globals.lottie,
      );
    }
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/statisticsimg.webp"),
            )),
            margin: EdgeInsets.all(5),
            child: Container(
              margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              height: 320,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/new_stats_icons.webp")),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 125, left: 45, right: 30),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${userStatsData["totalGamesPlayed"]}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: HexColor("#81B5AA"),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: Text(
                            "${userStatsData["overAllRank"]}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                              color: HexColor("#9AD4DC"),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 45, right: 30),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "${userStatsData["totalGameWon"]}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: HexColor("#A85F59"),
                          ),
                        ),
                        Text(
                          "${userStatsData["totalPoints"]}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: HexColor("#DAA505"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                "latest achievements".toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
              FlatButton(
                onPressed: () {
                  globals.Screen_name = "";
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => History_Screen(),
                  ));
                },
                child: Text("SEE ALL"),
                color: HexColor("#3F6FA4"),
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18)),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: StatefulBuilder(
        builder: (context, setState) {
          if (_isUserData) {
            if (_isStatLoading) getUserStats();
            return Stack(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  margin:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  height: 50,
                  child: Material(
                    child: IconButton(
                      iconSize: 50,
                      icon: Image.asset("assets/icons/back_arrow.webp"),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => GameList()));
                      },
                      tooltip: "Back",
                    ),
                    color: Colors.transparent,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  height: 120,
                  child: SafeArea(child: Image.asset("assets/round_logo.gif")),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 150),
                  child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      backgroundColor: Colors.transparent,
                      appBar: PreferredSize(
                        child: Container(
                          child: SafeArea(
                              child: TabBar(
                            indicator: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30),
                              ),
                              color: HexColor("#3e1b5f"),
                            ),
                            tabs: [
                              Tab(text: "Information"),
                              Tab(text: "Statistics"),
                            ],
                          )),
                        ),
                        preferredSize: Size.fromHeight(kToolbarHeight),
                      ),
                      body: TabBarView(
                        children: [
                          Container(
                            color: HexColor("#3e1b5f"),
                            child: profileInformation(),
                          ),
                          Container(
                            color: HexColor("#3e1b5f"),
                            child: userStatistics(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: globals.lottie,
            );
          }
        },
      ),
      onWillPop: _onBackPressed,
    );
  }

  save() async {
    Map<String, dynamic> feilds = {};
    if (newUserData.name != null) {
      feilds.addAll({"name": newUserData.name});
    }
    if (newUserData.username != null) {
      feilds.addAll({"username": newUserData.username});
    }
    if (newUserData.phone != null) {
      if (newUserData.phone.length == 10) {
        feilds.addAll({"phone": newUserData.phone});
      } else {
        globals.showToast(" Please Enter 10 digit Mobile Number ");
        return;
      }
    }
    if (newUserData.school != null) {
      feilds.addAll({"school": newUserData.school});
    }
    if (newUserData.city != null) {
      feilds.addAll({"city": newUserData.city});
    }
    if (newUserData.email != null) {
      if (!validateEmail(newUserData.email)) {
        feilds.addAll({"email": newUserData.email});
      } else {
        globals.showToast(" Please enter valid Email ID ");
        return;
      }
    }
    if (newUserData.profilePicture != null &&
        newUserData.profilePicture.isNotEmpty) {
      feilds.addAll({"profilePicture": newUserData.profilePicture});
    }

    if (feilds.length > 0) {
      Map<String, dynamic> action = {
        "action": "userUpdate",
        "data": {
          "id": _user.id,
          "authToken": authToken,
          "stage": globals.STAGE,
          "fields": feilds,
        },
      };

      var webSocket1 = WebSocketChannel.connect(globals.uri);
      webSocket1.stream.listen(output);
      print(jsonEncode(action).toString());
      webSocket1.sink.add(jsonEncode(action).toString());
    }
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value);
  }

  getUserStats() async {
    authToken = await UserData().getAuthToken();
    Map<String, dynamic> action = {
      "action": "userHistory",
      "data": {
        "stage": globals.STAGE,
        "authToken": authToken,
        "condition": {
          "UserId": _user.id,
        }
      }
    };
    print(jsonEncode(action).toString());
    webSocket.sink.add(jsonEncode(action).toString());
  }

  output(message) async {
    print(message);
    Map<String, dynamic> object = jsonDecode(message);
    if (object.containsKey("msg")) {
      if (object['msg'] != null &&
          object['msg'].contains("Usergamesummarylist successfully found")) {
        userStatsData = object['data'];
        setState(() {
          _isStatLoading = false;
        });
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] != null) {
        User userRes = User.fromJson(object['user']);
        await UserData().saveUser(userRes, authToken);
        globals.showToast(" Profile Updated Successfully ");
        setState(() {
          _isEditable = false;
          _isUserData = false;
          getUserData();
        });
//      try {} on Exception catch (_) {}
      } else if (object['msg'] != null &&
          object['msg'].contains("User Updated") &&
          object['user'] == null) {
        globals.showToast(" User Not Updated !");
        setState(() {
          _isEditable = false;
        });
      } else {
        setState(() {
          _isStatLoading = false;
        });
        globals.showToast(object['msg'].toString());
      }
    } else {
      setState(() {
        _isStatLoading = false;
      });
      globals.showToast("Internal server error to show Stats");
    }
  }

  Future<bool> _onBackPressed() {
    if (globals.Screen_name == "GameList") {
      return Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => GameList()));
    } else if (globals.Screen_name == "GameScreen") {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => GameScreen(game: widget.game)));
    } else {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => GameList(),
      ));
    }
  }
}
