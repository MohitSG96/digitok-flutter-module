import 'dart:async';
import 'dart:io' as io;

import 'package:digitok/models/question.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static Database _db;

  //region DataBase and their Fields
  static const String TABLE_PRACTICE = 'PracticeQuestion';
  static const String TABLE_HINDI = 'HindiQuestion';
  static const String TABLE_ENGLISH = 'EnglishQuestion';
  static const String DB_NAME = 'DigiTokManager.db';
  static const String PK = 'pk';
  static const String ID = 'id';
  static const String KEY_QUESTION = "question";
  static const String KEY_OPTION_A = "option_a";
  static const String KEY_OPTION_B = "option_b";
  static const String KEY_OPTION_C = "option_c";
  static const String KEY_OPTION_D = "option_d";
  static const String KEY_DESCRIPTION = "description";
  static const String KEY_ANSWER = "answer";
  static const String KEY_MEDIA = "media";
  static const String KEY_BOUNCER = "bouncer";
  static const String KEY_QUESTION_MEDIA = "question_media";
  static const String KEY_PRE_MEDIA = "pre_media";
  static const String KEY_TAG = "tags";
  static const String KEY_QUESTION_TIME = "question_time";
  static const String KEY_ANALYTICS_TIME = "analytics_time";
  static const String KEY_OPTION_TIME = "option_time";
  static const String KEY_POSITIVE_POINTS = "positive_points";
  static const String KEY_NEGATIVE_POINTS = "negative_points";
  static const String KEY_DESCRIPTION_TIME = "description_time";

  //endregion
  static const int DATABASE_VERSION = 1;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: DATABASE_VERSION);
    return db;
  }

  create() async {
    var dbClient = await db;
    try {
      await dbClient.execute(
          "CREATE TABLE $TABLE_PRACTICE ($PK INTEGER PRIMARY KEY, $ID INTEGER, " +
              " $KEY_QUESTION  TEXT, $KEY_ANSWER  TEXT, $KEY_OPTION_A TEXT, " +
              " $KEY_OPTION_B TEXT, $KEY_OPTION_C TEXT, $KEY_OPTION_D TEXT, " +
              " $KEY_MEDIA TEXT, $KEY_DESCRIPTION TEXT, $KEY_BOUNCER TEXT," +
              " $KEY_PRE_MEDIA TEXT, $KEY_QUESTION_MEDIA TEXT, $KEY_TAG TEXT, " +
              " $KEY_QUESTION_TIME INTEGER, $KEY_ANALYTICS_TIME INTEGER, " +
              " $KEY_OPTION_TIME INTEGER, $KEY_POSITIVE_POINTS REAL, " +
              " $KEY_NEGATIVE_POINTS REAL $KEY_QUESTION_TIME INTEGER," +
              " $KEY_DESCRIPTION_TIME INTEGER)");

      await dbClient.execute(
          "CREATE TABLE $TABLE_ENGLISH ($PK INTEGER PRIMARY KEY, $ID INTEGER, " +
              " $KEY_QUESTION  TEXT, $KEY_ANSWER  TEXT, $KEY_OPTION_A TEXT, " +
              " $KEY_OPTION_B TEXT, $KEY_OPTION_C TEXT, $KEY_OPTION_D TEXT, " +
              " $KEY_MEDIA TEXT, $KEY_DESCRIPTION TEXT, $KEY_BOUNCER TEXT," +
              " $KEY_PRE_MEDIA TEXT, $KEY_QUESTION_MEDIA TEXT, $KEY_TAG TEXT, " +
              " $KEY_QUESTION_TIME INTEGER, $KEY_ANALYTICS_TIME INTEGER, " +
              " $KEY_OPTION_TIME INTEGER, $KEY_POSITIVE_POINTS REAL, " +
              " $KEY_NEGATIVE_POINTS REAL $KEY_QUESTION_TIME INTEGER," +
              " $KEY_DESCRIPTION_TIME INTEGER)");

      await dbClient.execute(
          "CREATE TABLE $TABLE_HINDI ($PK INTEGER PRIMARY KEY, $ID INTEGER, " +
              " $KEY_QUESTION  TEXT, $KEY_ANSWER  TEXT, $KEY_OPTION_A TEXT, " +
              " $KEY_OPTION_B TEXT, $KEY_OPTION_C TEXT, $KEY_OPTION_D TEXT, " +
              " $KEY_MEDIA TEXT, $KEY_DESCRIPTION TEXT, $KEY_BOUNCER TEXT," +
              " $KEY_PRE_MEDIA TEXT, $KEY_QUESTION_MEDIA TEXT, $KEY_TAG TEXT, " +
              " $KEY_QUESTION_TIME INTEGER, $KEY_ANALYTICS_TIME INTEGER, " +
              " $KEY_OPTION_TIME INTEGER, $KEY_POSITIVE_POINTS REAL, " +
              " $KEY_NEGATIVE_POINTS REAL $KEY_QUESTION_TIME INTEGER," +
              " $KEY_DESCRIPTION_TIME INTEGER)");
    } catch (ex) {
      print(ex);
    }
  }

  savePractice(Question question) async {
    var dbClient = await db;
    await dbClient.insert(TABLE_PRACTICE, question.toMap());
  }

  saveEnglish(Question question) async {
    var dbClient = await db;
    await dbClient.insert(TABLE_ENGLISH, question.toMap());
  }

  saveHindi(HindiQuestion question) async {
    var dbClient = await db;
    await dbClient.insert(TABLE_HINDI, question.toMap());
  }

  Future<List<Question>> getPractice() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_PRACTICE);

    List<Question> questions = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        questions.add(Question.fromDBMap(maps[i]));
      }
    }
    return questions;
  }

  Future<List<Question>> getEnglish() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_ENGLISH);

    List<Question> questions = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        questions.add(Question.fromDBMap(maps[i]));
      }
    }
    return questions;
  }

  Future<List<Question>> getHindi() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_HINDI);

    List<Question> questions = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        questions.add(Question.fromDBMap(maps[i]));
      }
    }
    return questions;
  }

  drop() async {
    var dbClient = await db;
    await dbClient.execute("DROP TABLE IF EXISTS $TABLE_PRACTICE");
    await dbClient.execute("DROP TABLE IF EXISTS $TABLE_ENGLISH");
    await dbClient.execute("DROP TABLE IF EXISTS $TABLE_HINDI");
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
