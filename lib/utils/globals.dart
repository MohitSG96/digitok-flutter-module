import 'package:audioplayers/audioplayers.dart';
import 'package:digitok/models/game_feature.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';

//const STAGE = "DEVELOPMENT";
const STAGE = "PRODUCTION";
const BUCKET_ID = "foobucketlambdanew";
const POOL_ID = "us-east-2:c160f658-44cf-4c28-a309-faad8ff24024";

String language = "english";

const image_media = [".png", ".webp", ".jpg", ".jpeg", ".gif", ".jpe"];
const question_media = {
  "image": image_media,
  "video": [".mp4", ".3gp"],
  "audio": [".mp3", ".wav"],
};
int totalQuestions = 1;
var uri = Uri.parse("wss://kpue6mvso2.execute-api.us-east-2.amazonaws.com/ss");
String authToken = "";
String userName = "";
int userId = -1;
String profilePicture = "";

GameFeatures gameFeatures;
int gameId;
String animtype;
int extraLife = 0;
bool doubleX = false;
bool practice = false;
bool last = false;
bool isBouncer = true;
bool getBouncer = true;
bool description = true;
int countdown = 30;
int quizNumber = 1;

int totalUsersOnline = 0;
String Screen_name = "";

var gameBackGround = "";
var questionNumber = 1;
var bankItQuestion = -1;
var imageURL = "";

showToast(msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    backgroundColor: Colors.deepPurpleAccent,
    textColor: Colors.white,
  );
}

void audioPlayerHandler(AudioPlayerState value) => print('state => $value');

var lottie = Lottie.asset(
  'assets/loader/loader.json',
  width: 200,
  height: 200,
);

blueGlowTextStyle(double fontSize) {
  return TextStyle(
    fontSize: fontSize,
    color: Colors.white,
//    fontWeight: FontWeight.bold,
    fontFamily: "Digital",
    shadows: [
      Shadow(
        blurRadius: 5.0,
        color: Color(0xFF2F55DD),
        offset: Offset(5.0, 5.0),
      ),
    ],
  );
}
