import 'package:digitok/models/user.dart';
import 'package:digitok/pages/login_signup/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserData {
  final _storage = new FlutterSecureStorage();
  static const KEY_NAME = "NAME",
      KEY_USER_ID = "USER_ID",
      KEY_USER = "USERNAME",
      KEY_PHONE = "PHONE",
      KEY_EMAIL = "EMAIL",
      KEY_GENDER = "GENDER",
      KEY_PROFILE = "PROFILE_PIC",
      KEY_DOB = "DOB",
      KEY_CITY = "CITY",
      KEY_SCHOOL = "SCHOOL",
      KEY_AUTH = "AUTH",
      KEY_GAME_MUSIC = "GAME_MUSIC",
      KEY_APP_MUSIC = "APP_MUSIC",
      KEY_NOTIFICATION = "PUSH_NOTIFICATION";

  saveUser(User user, String authToken) async {
    await _storage.write(key: KEY_NAME, value: user.name ?? "");
    await _storage.write(key: KEY_USER_ID, value: user.id.toString());
    await _storage.write(key: KEY_USER, value: user.username);
    await _storage.write(key: KEY_PHONE, value: user.phone);
    await _storage.write(key: KEY_EMAIL, value: user.email ?? "");
    await _storage.write(key: KEY_GENDER, value: user.gender ?? "");
    await _storage.write(key: KEY_PROFILE, value: user.profilePicture ?? "");
    await _storage.write(key: KEY_DOB, value: user.dob ?? "");
    await _storage.write(key: KEY_SCHOOL, value: user.school ?? "");
    await _storage.write(key: KEY_CITY, value: user.city ?? "");
    await _storage.write(key: KEY_AUTH, value: authToken);
  }

  setUserProfilePic(String imageUrl) async {
    await _storage.write(key: KEY_PROFILE, value: imageUrl);
  }

  Future<User> getUser() async {
    String name = await _storage.read(key: KEY_NAME);
    String id = await _storage.read(key: KEY_USER_ID);
    String userName = await _storage.read(key: KEY_USER);
    String phone = await _storage.read(key: KEY_PHONE);
    String email = await _storage.read(key: KEY_EMAIL);
    String gender = await _storage.read(key: KEY_GENDER);
    String profilePic = await _storage.read(key: KEY_PROFILE);
    String dob = await _storage.read(key: KEY_DOB);
    String school = await _storage.read(key: KEY_SCHOOL);
    String city = await _storage.read(key: KEY_CITY);

    User user = User(
      name: name,
      id: int.tryParse(id),
      username: userName,
      phone: phone,
      dob: dob,
      email: email,
      gender: gender,
      profilePicture: profilePic,
      city: city,
      school: school,
    );

    return user;
  }

  Future<String> getAuthToken() async {
    String authToken = await _storage.read(key: KEY_AUTH);
    return authToken;
  }

  logout(context) async {
    String welcome = await _storage.read(key: "FIRST_USE");
    await _storage.deleteAll();
    await _storage.write(key: "FIRST_USE", value: welcome ?? "YES");
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  saveGameSetting(bool appSound, bool gameSound, bool notification) async {
    await _storage.write(key: KEY_APP_MUSIC, value: appSound ? "yes" : "no");
    await _storage.write(key: KEY_GAME_MUSIC, value: gameSound ? "yes" : "no");
    await _storage.write(
        key: KEY_NOTIFICATION, value: notification ? "yes" : "no");
  }

  Future<List<bool>> getGameSetting() async {
    String appSound, gameSound, notification;
    appSound = await _storage.read(key: KEY_APP_MUSIC);
    gameSound = await _storage.read(key: KEY_GAME_MUSIC);
    notification = await _storage.read(key: KEY_NOTIFICATION);

    List<bool> settings = [
      appSound != null && appSound.isNotEmpty ? appSound != "no" : true,
      gameSound != null && gameSound.isNotEmpty ? gameSound != "no" : true,
      notification != null && notification.isNotEmpty
          ? notification != "no"
          : true
    ];
    return settings;
  }

  setWelcome()async{
    await _storage.write(key: "FIRST_USE", value: "NO");
  }
  Future<String> getWelcome()async{
    return await _storage.read(key: "FIRST_USE");
  }

}
