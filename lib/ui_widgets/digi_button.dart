import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DigiButton extends StatelessWidget {
  final onPressed, child, color, image, width, height;

  DigiButton(
      {this.onPressed,
      @required this.child,
      this.color,
      this.image,
      this.width,
      this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? MediaQuery.of(context).size.width,
      height: height ?? 90,
      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.fill,
            image: image ?? AssetImage("assets/base_blank.webp")),
      ),
      child: Padding(
        padding: EdgeInsets.fromLTRB(width != null ? width * 0.11 : 55, 15,
            width != null ? width * 0.11 : 55, 15),
        child: FlatButton(
          onPressed: onPressed ?? () {},
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(45)),
          child: child,
          color: Colors.transparent,
          hoverColor: Colors.transparent,
          highlightColor: Colors.transparent,
          focusColor: Colors.transparent,
          textColor: Colors.white,
//          splashColor: Colors.transparent,
        ),
      ),
    );
  }
}
