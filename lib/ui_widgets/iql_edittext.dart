import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IQLEditText extends StatelessWidget {
  final Widget child;
  final double height;

  IQLEditText({@required this.child, this.height});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(7),
                bottomLeft: Radius.circular(7),
              ),
              border: Border.all(
                color: Color(0xFFC81273),
                width: 5,
              )),
          child: height != null && height != 0.0
              ? SizedBox(
                  height: height,
                  width: MediaQuery.of(context).size.width,
                )
              : TextFormField(
                  decoration: InputDecoration(
                    hintText: null,
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.zero,
                  ),
                  key: null,
                ),
        ),
        Container(
          margin: EdgeInsetsDirectional.only(start: 5, end: 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(7),
                bottomLeft: Radius.circular(7),
              ),
              border: Border.all(
                color: Colors.white,
                width: 5,
              )),
          child: child,
        )
      ],
    );
  }
}
