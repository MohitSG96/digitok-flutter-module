import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class NavItem extends StatelessWidget {
  final IconData icon;
  final text;
  final onTap;
  final image;

  const NavItem({Key key, this.icon, this.image, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.black,
      highlightColor: Colors.grey,
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Color(0xFF2F3F7e),
              width: 0.8,
            ),
          ),
        ),
        margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
        padding: EdgeInsets.only(bottom: 5),
        width: double.maxFinite,
        child: Row(
          children: <Widget>[
            icon != null
                ? Icon(
                    icon,
                    color: Color(0xFF2F3F7e),
                  )
                : image != null
                    ? image
                    : Center(),
            SizedBox(width: 15),
            text ?? Text(""),
          ],
        ),
      ),
    );
  }
}
