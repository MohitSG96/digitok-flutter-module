import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DigiEditText extends StatelessWidget {
  final Widget child;
  final double height;

  DigiEditText({@required this.child, this.height});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(55),
              border: Border.all(
                color: Colors.teal,
                width: 5,
              )),
          child: height != null && height != 0.0
              ? SizedBox(
                  height: height,
                  width: MediaQuery.of(context).size.width,
                )
              : TextFormField(
                  decoration: InputDecoration(
                    hintText: null,
                    border: InputBorder.none,
                  ),
                  key: null,
                ),
        ),
        Container(
          margin: EdgeInsetsDirectional.only(start: 5, end: 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(55),
              border: Border.all(
                color: Colors.white,
                width: 5,
              )),
          child: child,
        )
      ],
    );
  }
}
