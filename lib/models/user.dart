class User {
  String name,
      username,
      phone,
      email,
      countryCode,
      gender,
      profilePicture,
      dob,
      city,
      school,
      grade;
  int id;
  bool flowCompleted;

  User(
      {this.name,
      this.id,
      this.phone,
      this.email,
      this.countryCode,
      this.gender,
      this.profilePicture,
      this.username,
      this.city,
      this.school,
      this.dob,
      this.grade,
      this.flowCompleted});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    String phone = "", country = "91";
    if (parsedJson.containsKey("phone") && parsedJson['phone'] != null) {
      if (parsedJson['phone'].toString().length == 13) {
        country = parsedJson['phone'].toString().substring(1, 3);
        phone = parsedJson['phone'].toString().substring(3);
      } else if (parsedJson['phone'].toString().length == 12) {
        country = parsedJson['phone'].toString().substring(0, 2);
        phone = parsedJson['phone'].toString().substring(2);
      } else {
        phone = parsedJson['phone'].toString();
      }
    } else if (parsedJson.containsKey("mobile") &&
        parsedJson['mobile'] != null) {
      if (parsedJson['mobile'].toString().length == 13) {
        country = parsedJson['mobile'].toString().substring(1, 3);
        phone = parsedJson['mobile'].toString().substring(3);
      } else if (parsedJson['mobile'].toString().length == 12) {
        country = parsedJson['mobile'].toString().substring(0, 2);
        phone = parsedJson['mobile'].toString().substring(2);
      } else {
        phone = parsedJson['mobile'].toString();
      }
    }
    return User(
      name: parsedJson['name'] ?? "",
      id: int.tryParse(parsedJson['id'].toString()),
      username:
          parsedJson.containsKey('username') && parsedJson['username'] != null
              ? parsedJson['username']
              : "",
      countryCode: country,
      phone: phone,
      email: parsedJson['email'] ?? "",
      gender: parsedJson['gender'] ?? "",
      profilePicture: parsedJson.containsKey("profilePicture") &&
              parsedJson['profilePicture'] != null
          ? parsedJson['profilePicture']
          : "",
      school: parsedJson['school'] ?? "",
      city: parsedJson['city'] ?? "",
      grade: parsedJson['grade'] ?? "",
      dob: parsedJson['dob'] ?? "",
      flowCompleted: parsedJson.containsKey('isFlowCompleted')
          ? parsedJson['isFlowCompleted'] ?? false
          : false,
    );
  }
}
