class Avatar {
  String avatarImage;
  bool isSelected;

  Avatar({this.avatarImage, this.isSelected});

  factory Avatar.fromJson(Map<String, dynamic> parsedJson) {
    return Avatar(avatarImage: parsedJson["avatarImageUrl"]);
  }
}
