class Game {
  String gameName, tagLine, imageSrc, gameBackground;
  int stackId, id;
  bool isAppointment;

  Game(
      {this.gameName,
      this.tagLine,
      this.imageSrc,
      this.gameBackground,
      this.stackId,
      this.id,
      this.isAppointment});

  factory Game.fromJson(Map<String, dynamic> parsedJson) {
    return Game(
      id: int.tryParse(parsedJson['id'].toString()),
      gameName: parsedJson['name'],
      tagLine: parsedJson['tagline'],
      imageSrc: parsedJson['logo'] ?? "",
      gameBackground: parsedJson['colorCode'] ?? "#9C27B0",
      stackId: parsedJson['activeStack'],
      isAppointment: parsedJson['isAppointment'] ?? false,
    );
  }
}
