class GameFeatures {
  List questionIds;
  int gameId;
  String stackName;
  int bankOn;
  bool isHindi;
  GameDetail game;
  Season season;

  GameFeatures(
      {this.questionIds,
      this.gameId,
      this.stackName,
      this.bankOn,
      this.isHindi,
      this.game,
      this.season});

  factory GameFeatures.fromJson(Map<String, dynamic> parsedJson) {
    GameDetail detail = GameDetail.fromJson(parsedJson['Game']);
    Season season = Season.fromJson(parsedJson['Season']);

    return GameFeatures(
      gameId: parsedJson['id'],
      stackName: parsedJson['name'],
      bankOn: parsedJson.containsKey("bankOn") && parsedJson['bankOn'] != null
          ? int.tryParse(parsedJson['bankOn'].toString())
          : -1,
      isHindi:
          parsedJson.containsKey("isHindi") && parsedJson['isHindi'] != null
              ? parsedJson['isHindi']
              : false,
      questionIds: parsedJson['questionIds'] ?? [],
      game: detail,
      season: season,
    );
  }
}

class GameDetail {
  List<String> rewardImages;
  List<String> hindiRewards;
  List<String> englishRewards;
  List<String> hindiCaptions;
  List<String> englishCaptions;
  List<String> hindiCountText;
  List<String> englishCountText;
  String name;
  String logo;
  String tagLine;
  int bankPoints;
  int totalPoints;
  int positivePoints;
  int heartCount;
  bool is2x;
  bool isHeart;
  int negativePoints;
  String animationType;
  String nextGame;

  GameDetail(
      {this.rewardImages,
      this.hindiRewards,
      this.englishRewards,
      this.hindiCaptions,
      this.englishCaptions,
      this.hindiCountText,
      this.englishCountText,
      this.name,
      this.logo,
      this.tagLine,
      this.bankPoints,
      this.totalPoints,
      this.positivePoints,
      this.heartCount,
      this.is2x,
      this.isHeart,
      this.negativePoints,
      this.animationType,
      this.nextGame});

  factory GameDetail.fromJson(Map<String, dynamic> parsedJson) {
    return GameDetail(
      rewardImages: parsedJson['rewards_image'] != null
          ? parsedJson['rewards_image'].cast<String>()
          : [],
      englishRewards: parsedJson['rewards_english'] != null
          ? parsedJson['rewards_english'].cast<String>()
          : null,
      hindiRewards: parsedJson['rewards_hindi'] != null
          ? parsedJson['rewards_hindi'].cast<String>()
          : null,
      englishCaptions: parsedJson['captions_english'] != null
          ? parsedJson['captions_english'].cast<String>()
          : null,
      hindiCaptions: parsedJson['captions_hindi'] != null
          ? parsedJson['captions_hindi'].cast<String>()
          : null,
      hindiCountText: parsedJson['countdowntexts_hindi'] != null
          ? parsedJson['countdowntexts_hindi'].cast<String>()
          : null,
      englishCountText: parsedJson['countdowntexts_english'] != null
          ? parsedJson['countdowntexts_english'].cast<String>()
          : null,
      name: parsedJson['name'],
      logo: parsedJson['logo'],
      tagLine: parsedJson['tagline'] ?? null,
      bankPoints: parsedJson['bankPoints'] ?? 0,
      totalPoints: parsedJson['totalPoints'] ?? 0,
      positivePoints: parsedJson['positivePoints'] ?? 0,
      is2x: parsedJson['is2x'] ?? false,
      isHeart: parsedJson['isHeart'] ?? false,
      negativePoints: parsedJson['negativePoints'] ?? 0,
      animationType: parsedJson['animationType'] ?? null,
      nextGame: parsedJson['nextGame'] ?? null,
      heartCount: parsedJson['heartCount'] ?? 0,
    );
  }
}

class Season {
  List<String> rules;
  List<String> features;
  String name;
  String backgroundUrl;
  String optionImageUrl;

  Season(
      {this.rules,
      this.features,
      this.name,
      this.backgroundUrl,
      this.optionImageUrl});

  factory Season.fromJson(Map<String, dynamic> parsedJson) {
    return Season(
      rules: parsedJson['rules'] != null
          ? parsedJson['rules'].cast<String>()
          : null,
      features: parsedJson['features'] != null
          ? parsedJson['features'].cast<String>()
          : null,
      name: parsedJson['name'] ?? null,
      backgroundUrl: parsedJson['background'] ?? null,
      optionImageUrl: parsedJson['optionImage'] ?? null,
    );
  }
}
