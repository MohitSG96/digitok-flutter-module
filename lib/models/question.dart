class Question {
  int id;
  String question;
  String answer;
  String media;
  String description;
  String optionA;
  String optionB;
  String optionC;
  String optionD;
  String preMedia;
  String questionMedia;
  int questionTime = -1;
  int analyticsTime = -1;
  int optionTime = -1;
  double positivePoints = -1.0;
  double negativePoints = -1.0;
  int descriptionTime = -1;
  List tags = [];

  Question(
      {this.id,
      this.question,
      this.answer,
      this.media,
      this.description,
      this.optionA,
      this.optionB,
      this.optionC,
      this.optionD,
      this.preMedia,
      this.questionMedia,
      this.questionTime,
      this.analyticsTime,
      this.optionTime,
      this.positivePoints,
      this.negativePoints,
      this.descriptionTime,
      this.tags});

  toMap() {
    return {
      'id': id,
      'question': question,
      'option_a': optionA,
      'option_b': optionB,
      'option_c': optionC,
      'option_d': optionD,
      'description': description,
      'answer': answer,
      'media': media,
      'bouncer': 'NO',
      'question_media': questionMedia,
      'pre_media': preMedia,
      'tags': tags.join(", "),
      'question_time': questionTime,
      'analytics_time': analyticsTime,
      'option_time': optionTime,
      'positive_points': positivePoints,
      'negative_points': negativePoints,
      'description_time': descriptionTime,
    };
  }

  factory Question.fromDBMap(Map<String, dynamic> map) {
    return Question(
      id: map['id'],
      question: map['question'],
      optionA: map['option_a'],
      optionB: map['option_b'],
      optionC: map['option_c'],
      optionD: map['option_d'],
      tags: map['tags'] != null && map['tags'].length != 0
          ? map['tags'].split(',')
          : [],
      answer: map['answer'],
      description: map['description'],
      media: map['media'],
      questionTime: map['question_time'] ?? -1,
      analyticsTime: map['analyticsTime'] ?? -1,
      optionTime: map['option_time'] ?? -1,
      descriptionTime: map['description_time'] ?? -1,
      preMedia: map['pre_media'],
      questionMedia: map['question_media'],
      positivePoints: double.tryParse(map['positive_points'].toString()),
      negativePoints: double.tryParse(map['negative_points'].toString()),
    );
  }

  factory Question.fromMap(Map<String, dynamic> map) {
    return Question(
      id: map['id'] ?? 0,
      question: map['question'] ?? "",
      optionA: map['options'][0]!= null ? map['options'][0].trim() : "",
      optionB: map['options'][1]!= null ? map['options'][1].trim() : "",
      optionC: map['options'][2]!= null ? map['options'][2].trim() : "",
      optionD: map['options'][3]!= null ? map['options'][3].trim() : "",
      tags: map['tags'] ?? [],
      answer: map['answer']!= null ? map['answer'].trim() : "",
      description: map['description'] ?? "",
      media: map['media'] ?? "",
      questionTime: map['questionTime'] ?? 0,
      analyticsTime: map['analyticsTime'] ?? 0,
      optionTime: map['optionTime'] ?? 0,
      descriptionTime: int.tryParse(map['descriptionTime'].toString()) ?? 0,
      preMedia: map['preQuestionsMedia'] ?? "",
      questionMedia: map['preOptionsMedia'] ?? "",
      positivePoints: map['positivePoints'] != null
          ? map['positivePoints'].toDouble()
          : 0.0,
      negativePoints: map['negativePoints'] != null
          ? map['negativePoints'].toDouble()
          : 0.0,
    );
  }
}

class HindiQuestion {
  int id;
  String question;
  String answer;
  String media;
  String description;
  String optionA;
  String optionB;
  String optionC;
  String optionD;
  String preMedia;
  String questionMedia;
  int questionTime = -1;
  int analyticsTime = -1;
  int optionTime = -1;
  double positivePoints = -1.0;
  double negativePoints = -1.0;
  int descriptionTime = -1;
  List tags = [];

  HindiQuestion(
      {this.id,
      this.question,
      this.answer,
      this.media,
      this.description,
      this.optionA,
      this.optionB,
      this.optionC,
      this.optionD,
      this.preMedia,
      this.questionMedia,
      this.questionTime,
      this.analyticsTime,
      this.optionTime,
      this.positivePoints,
      this.negativePoints,
      this.descriptionTime,
      this.tags});

  toMap() {
    return {
      'id': id,
      'question': question,
      'option_a': optionA,
      'option_b': optionB,
      'option_c': optionC,
      'option_d': optionD,
      'description': description,
      'answer': answer,
      'media': media,
      'bouncer': 'NO',
      'question_media': questionMedia,
      'pre_media': preMedia,
      'tags': tags.join(", "),
      'question_time': questionTime,
      'analytics_time': analyticsTime,
      'option_time': optionTime,
      'positive_points': positivePoints,
      'negative_points': negativePoints,
      'description_time': descriptionTime,
    };
  }

  factory HindiQuestion.fromDBMap(Map<String, dynamic> map) {
    return HindiQuestion(
      id: map['id'],
      question: map['question'],
      optionA: map['option_a'],
      optionB: map['option_b'],
      optionC: map['option_c'],
      optionD: map['option_d'],
      tags: map['tags'] != null && map['tags'].length != 0
          ? map['tags'].split(',')
          : [],
      answer: map['answer'],
      description: map['description'],
      media: map['media'],
      questionTime: map['question_time'],
      analyticsTime: map['analyticsTime'],
      optionTime: map['option_time'],
      descriptionTime: map['description_time'],
      preMedia: map['pre_media'],
      questionMedia: map['question_media'],
      positivePoints: double.tryParse(map['positive_points'].toString()),
      negativePoints: double.tryParse(map['negative_points'].toString()),
    );
  }

  factory HindiQuestion.fromMap(Map<String, dynamic> map) {
    return HindiQuestion(
      id: map['id'] ?? 0,
      question: map['question_hi'] ?? "",
      optionA: map['options_hi'][0] ?? "",
      optionB: map['options_hi'][1] ?? "",
      optionC: map['options_hi'][2] ?? "",
      optionD: map['options_hi'][3] ?? "",
      tags: map['tags_hi'] ?? [],
      answer: map['answer_hi'] ?? "",
      description: map['description_hi'] ?? "",
      media: map['media'] ?? "",
      questionTime: map['questionTime'] ?? "",
      analyticsTime: map['analyticsTime'] ?? 0,
      optionTime: map['optionTime'] ?? 0,
      descriptionTime: int.tryParse(map['descriptionTime'].toString()) ?? 0,
      preMedia: map['preQuestionsMedia'] ?? "",
      questionMedia: map['preOptionsMedia'] ?? "",
      positivePoints: map['positivePoints'] != null
          ? map['positivePoints'].toDouble()
          : 0.0,
      negativePoints: map['negativePoints'] != null
          ? map['negativePoints'].toDouble()
          : 0.0,
    );
  }
}
